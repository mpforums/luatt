/*

class LuaScriptManager //this is a completly new coding style for me
{	
	class LuaScript : public ScriptImpClass
	{
		void Created(GameObject *obj);
		void Destroyed(GameObject *obj);
		void Killed(GameObject *obj,GameObject *shooter);
		void Damaged(GameObject *obj,GameObject *damager,float damage);
		void Custom(GameObject *obj,int message,int param,GameObject *sender);
		void Enemy_Seen(GameObject *obj,GameObject *seen);
		void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
		void Timer_Expired(GameObject *obj,int number);
		void Animation_Complete(GameObject *obj,const char *anim);
		void Poked(GameObject *obj,GameObject *poker);
		void Entered(GameObject *obj,GameObject *enter);
		void Exited(GameObject *obj,GameObject *exit);
	};
	struct Script
	{
		char *script;
		lua_State *lua;
		ScriptRegistrant<LuaScript> *scriptreg;
	};
	static Script *Scripts[256];
	static ScriptImpClass *Get_Script(GameObject *obj, int scriptID);
	static bool Report_Errors(lua_State *L, int status);
	static void RegisterScriptFunctions(lua_State *L);

	static int Lua_DestroyScript_Wrap(lua_State *L);
	static int Lua_Get_Float_Parameter_Wrap(lua_State *L);
	static int Lua_Get_Int_Parameter_Wrap(lua_State *L);
	static int Lua_Get_String_Parameter_Wrap(lua_State *L);
	static int Lua_Start_Timer_Wrap(lua_State *L);
public:
	static void Load();
	static int Register_Script(lua_State *L);
	//static void AddScript(const char *LuaFile);
	static void Lua_DestroyScript(GameObject *obj, int scriptID);
	static float Lua_Get_Float_Parameter(GameObject *obj, int scriptID, const char *Name);
	static int Lua_Get_Int_Parameter(GameObject *obj, int scriptID, const char *Name);
	static const char *Lua_Get_String_Parameter(GameObject *obj, int scriptID, const char *Name);
	static void Lua_Start_Timer(GameObject *obj, int scriptID, float Time, int Number);
	static void Cleanup();
};
*/


void KeyHookBaseHook(void *data);
struct ConnectionRequest;

class LuaScriptManager //this is a completly new coding style for me
{
public:	
    

	class LuaScript : public JFW_Key_Hook_Base
	{	
		void Created(GameObject *obj);
		void Destroyed(GameObject *obj);
		void Killed(GameObject *obj,GameObject *shooter);
		void Damaged(GameObject *obj,GameObject *damager,float damage);
		void Custom(GameObject *obj,int message,int param,GameObject *sender);
		void Enemy_Seen(GameObject *obj,GameObject *seen);
		void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
		void Timer_Expired(GameObject *obj,int number);
		void Animation_Complete(GameObject *obj,const char *anim);
		void Poked(GameObject *obj,GameObject *poker);
		void Entered(GameObject *obj,GameObject *enter);
		void Exited(GameObject *obj,GameObject *exit);
		void Detach(GameObject *obj);
		void KeyHook();
	};
	struct Script
	{
		ScriptRegistrant<LuaScript> *scriptreg;
		lua_State *lua;
		int uRef;
		char Name[256];
		char Param[256];
	};
	// I guess that the best thing to do is to not bind a key hook to a script, but have a separate Add_Key_Hook kind of call
	// hmmm.. by having a separated script should work?
	// Script? YENota a script, just another object, hmm..

	
	static Script *Scripts[256];
	static ScriptImpClass *Get_Script(GameObject *obj, int scriptID);
	static void RegisterScriptFunctions(lua_State *L);

	static int Lua_DestroyScript_Wrap(lua_State *L);
	static int Lua_Get_Float_Parameter_Wrap(lua_State *L);
	static int Lua_Get_Int_Parameter_Wrap(lua_State *L);
	static int Lua_Get_String_Parameter_Wrap(lua_State *L);
	static int Lua_Start_Timer_Wrap(lua_State *L);
	static int Lua_Install_Hook_Warp(lua_State *L);
	

	static void Load();
	static int Register_Script(lua_State *L);
	static void Lua_DestroyScript(GameObject *obj, int scriptID);
	static float Lua_Get_Float_Parameter(GameObject *obj, int scriptID, const char *Name);
	static int Lua_Get_Int_Parameter(GameObject *obj, int scriptID, const char *Name);
	static const char *Lua_Get_String_Parameter(GameObject *obj, int scriptID, const char *Name);
	static void Lua_Start_Timer(GameObject *obj, int scriptID, float Time, int Number);
	static void Cleanup();
	
};



class LuaManager
{	
	
public:
	static lua_State *Lua[256];
	//static const char *LuaNames[256];
	static void Load();
	static void ShowBanner();
	static void LoadLua(const char *LuaFile);
	static void Cleanup();
	static char LuaFiles[256];
	static bool Call_Chat_Hook(int ID, int Type, const wchar_t *Msg, int Target);
	static const char *ConAc(const char *ip, const char *nick, const char *serial, const char *version);
	static bool Call_Host_Hook(int PlayerID,TextMessageEnum Type,const char *Message);
	static void Call_Player_Join_Hook(int ID, const char *Nick);
	static bool Refill_Hook(GameObject *purchaser);
	static void Call_Player_Leave_Hook(int ID);
	static void Call_Level_Loaded_Hook();
	static void Call_GameOver_Hook();
	static void Call_Console_Output_Hook(const char *Output);
	//static void Call_DDE_Hook(const char *DDEStr);
	static bool Report_Errors(lua_State *L, int status);
	static void Purchase_Hook(BaseControllerClass *base, GameObject *purchaser, unsigned int cost, unsigned int preset,unsigned int purchaseret,const char *data);
	static void Call_Object_Hook(void *data, GameObject *obj);
	static void Call_Think_Hook();
	static int Purchase_Hook2(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data);
	static void CallInvoke(const char *Function, const char *Arg);
	//static void Call_KeyHook(const char *key, int pID);
	static bool RadioHook(int PlayerType, int PlayerID, int AnnouncementID, int IconID, AnnouncementEnum AnnouncementType);
	static void UnloadingLua();
	static void Reload_All();
	//static void InputConsole2(const char *Message);
	static void KilledHook(int obj,int shooter);
	static void DestroyedHook(int obj);
	static void CustomHook(int obj,int message,int param,int sender);
	static void DamageHook(int obj,int damager,float damage);
	static void PokedHook(int obj,int damager);
	static void KeyHook(int obj, const char *key, const char *callback);
	static bool TT_Damage_Hook(PhysicalGameObj* damager, PhysicalGameObj* target, const AmmoDefinitionClass* ammo, const char* bone);
   // static void SSGM_Log(const char *Message);
    static void Adress_IP(const char *addr);
	static bool Reload_Flag;
	static class CommandLua;
};

void LoadConsoleLua();

class LuaSocketManager
{
	static TCPSocket *Sockets[64];
public:
	static void Load();
	static void Cleanup();

	static int Connect(const char *IP, int port);
	static int Server(const char *IP, int port);
	static int Accept(int Socket);
	static int Recv(int Socket, char *Data, int Length);
	static int Send(int Socket, const char *Data);
	static int Is_DataAvaliable(int Socket);
	static int Is_ClientWaiting(int Socket);
	static void Disconnect(int Socket);	
	static int Lua_Connect_Wrap(lua_State *L);
	static int Lua_Server_Wrap(lua_State *L);
	static int Lua_Accept_Wrap(lua_State *L);
	static int Lua_Send_Wrap(lua_State *L);
	static int Lua_Recv_Wrap(lua_State *L);
	static int Lua_DataAvaliable_Wrap(lua_State *L);
	static int Lua_ClientWaiting_Wrap(lua_State *L);
	static int Lua_Disconnect_Wrap(lua_State *L);
	static void Register(lua_State *L);
	
};


#define lua_boxpointer(L,u) \
	(*(void **)(lua_newuserdata(L, sizeof(void *))) = (u))

#define lua_unboxpointer(L,i)	(*(void **)(lua_touserdata(L, i)))


class Lua_Script_Hook : public ScriptImpClass {
 void Killed(GameObject *obj, GameObject *shooter);
 void Destroyed(GameObject *obj);
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 void Damaged(GameObject *obj,GameObject *damager,float damage);
 void Poked(GameObject *obj,GameObject *poker);
};

class Lua_Key_Hook : public JFW_Key_Hook_Base {
	void Created(GameObject *obj);
	void KeyHook();
	int myobj;
};

/*
class LuaConsoleReg
{

}

*/