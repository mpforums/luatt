#pragma once

#include "gmplugin.h"
#include "ConnectionAcceptanceFilter.h"

class cGameData;
class StringClass;


class luatt :
	public Plugin,
	public ConnectionAcceptanceFilter
{
	void evict(int clientId, const WideStringClass& reason);
public:
	
	luatt();
	~luatt();
	virtual void OnLoadGlobalINISettings(INIClass *SSGMIni);
	virtual void OnFreeData();
	virtual void OnLoadMapINISettings(INIClass *SSGMIni);
	virtual void OnFreeMapData();
	virtual bool OnChat(int PlayerID,TextMessageEnum Type,const wchar_t *Message,int recieverID);
	virtual void OnObjectCreate(void *data,GameObject *obj);
	virtual void OnLoadLevel();
	virtual void OnGameOver();
	virtual void OnPlayerJoin(int PlayerID,const char *PlayerName);
	virtual void OnPlayerLeave(int PlayerID);
	virtual bool OnRefill(GameObject *purchaser);
	virtual int OnPowerupPurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data);
	virtual int OnVehiclePurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data);
	virtual int OnCharacterPurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data);
	virtual void OnThink();
	virtual void Log_Output(const char *message);

	
	virtual void handleInitiation(const ConnectionRequest& connectionRequest);
	virtual void handleTermination(const ConnectionRequest& connectionRequest);
	virtual void handleCancellation(const ConnectionRequest& connectionRequest);
	STATUS getStatus(const ConnectionRequest& connectionRequest, WideStringClass& refusalMessage);
};


//extern luatt Luatt;