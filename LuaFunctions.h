
void GetFiles(const char *Path, char **Data, int DataLength);

int Lua_Console_Input(lua_State *L); 
int Lua_Console_Output(lua_State *L);
int Lua_Get_Definition_Name(lua_State *L); 
int Lua_Get_Definition_ID(lua_State *L);
int Lua_Get_Definition_Class_ID(lua_State *L); 
int Lua_Is_Valid_Preset_ID(lua_State *L); 
int Lua_Is_Valid_Preset(lua_State *L); 
int Lua_Set_Max_Health(lua_State *L);
int Lua_Set_Max_Shield_Strength(lua_State *L);
int Lua_Get_Shield_Type(lua_State *L);
int Lua_Get_Skin(lua_State *L);
int Lua_Set_Skin(lua_State *L);
int Lua_Power_Base(lua_State *L); 
int Lua_Set_Can_Generate_Soldiers(lua_State *L);
int Lua_Set_Can_Generate_Vehicles(lua_State *L);
int Lua_HideTexture(lua_State *L);
int Lua_Destroy_Base(lua_State *L);
int Lua_Get_All_Vehicles(lua_State *L);
int Lua_Get_All_Buildings2(lua_State *L);
int Lua_Get_All_Players(lua_State *L);
int Lua_Get_Current_Map_Index(lua_State *L);
int Lua_Set_Map(lua_State *L);
int Lua_Set_Is_Visible(lua_State *L);
int Lua_Clear_Weapons(lua_State *L);
int Lua_Get_Client_Revision(lua_State *L);
int Lua_Is_WOL_User(lua_State *L);
int Lua_Get_Distance(lua_State *L);
int Lua_OColision(lua_State *L);
int Lua_Beacon_Destroyed_Base(lua_State *L);
int Can_Build_Here(Vector3 &Size, Vector3 &Pos, GameObject *Player);
int Lua_Enable_Base_Radar(lua_State *L);
int Lua_Is_Harvester(lua_State *L);
int Lua_Memory_Write(lua_State *L);
int Lua_Memory_Read(lua_State *L);
//int Lua_Set_cPlayer(lua_State *L);
int Lua_Is_Radar_Enabled(lua_State *L); 
int Lua_ShowTexture(lua_State *L);
int Lua_Is_Night_Map(lua_State *L);
int Lua_Revive_Building(lua_State *L);
int Lua_Set_Animation(lua_State *L);
int Lua_Restore_Building(lua_State *L);
int Lua_Create_Building(lua_State *L);
int Lua_Set_Game_Title(lua_State *L);
//int Lua_Set_cPlayer(lua_State *L);
int Lua_Damage_Occupants(lua_State *L);
int Lua_Add_TTDamageHook(lua_State *L);
int Lua_Building_Type(lua_State *L); 
int Lua_Is_Building_Dead(lua_State *L); 
int Lua_Find_Building(lua_State *L);
int Lua_Find_Base_Defense(lua_State *L);
int Lua_Is_Map_Flying(lua_State *L); 
int Lua_Find_Harvester(lua_State *L);
int Lua_Is_Base_Powered(lua_State *L);
int Lua_Can_Generate_Vehicles(lua_State *L);
int Lua_Can_Generate_Soliders(lua_State *L);
int Lua_The_Skirmish_Game(lua_State *L);
int Lua_The_Cnc_Game(lua_State *L);
int Lua_The_Single_Player_Game(lua_State *L);
int Lua_Is_A_Building(lua_State *L);
int Lua_Get_Building_Count_Team(lua_State *L);
int Lua_Find_Building_By_Team(lua_State *L);
int Lua_Find_Building_By_Name(lua_State *L);
int Lua_Find_Power_Plant(lua_State *L);
int Lua_Find_Refinery(lua_State *L);
int Lua_Find_Repair_Bay(lua_State *L);
int Lua_Find_Soldier_Factory(lua_State *L);
int Lua_Find_Airstrip(lua_State *L);
int Lua_Find_War_Factory(lua_State *L);
int Lua_Find_Vehicle_Factory(lua_State *L);
int Lua_Find_Com_Center(lua_State *L);
int Lua_Is_Gameplay_Permitted(lua_State *L);
int Lua_Is_Dedicated(lua_State *L); 
int Lua_Is_Linux(lua_State *L);
int Lua_Get_Current_Game_Mode(lua_State *L);
int Lua_Get_Harvester_Preset_ID(lua_State *L);
int Lua_Is_Harvester_Preset(lua_State *L);
int Lua_Destroy_Connection(lua_State *L);
int Lua_Get_IP_Address(lua_State *L);
int Lua_Get_IP_Port(lua_State *L);
int Lua_Get_Bandwidth(lua_State *L);
int Lua_Get_Ping(lua_State *L);
int Lua_Get_Kbits(lua_State *L);
int Lua_Get_Object_Type(lua_State *L);
int Lua_Set_Object_Type(lua_State *L);
int Lua_Is_Building(lua_State *L);
int Lua_Is_Soldier(lua_State *L);
int Lua_Is_Vehicle(lua_State *L);
int Lua_Is_Cinematic(lua_State *L);
int Lua_Is_ScriptZone(lua_State *L);
int Lua_Is_Powerup(lua_State *L);
int Lua_Is_C4(lua_State *L);
int Lua_Is_Beacon(lua_State *L);
int Lua_Is_Armed(lua_State *L);
int Lua_Is_Simple(lua_State *L);
int Lua_Is_PowerPlant(lua_State *L);
int Lua_Is_SoldierFactory(lua_State *L);
int Lua_Is_VehicleFactory(lua_State *L);
int Lua_Is_Airstrip(lua_State *L);
int Lua_Is_WarFactory(lua_State *L);
int Lua_Is_Refinery(lua_State *L);
int Lua_Is_ComCenter(lua_State *L);
int Lua_Is_Vehicle_Owner(lua_State *L);
int Lua_Is_RepairBay(lua_State *L);
int Lua_Is_Scriptable(lua_State *L);
int Lua_Get_Building_Type(lua_State *L);
int Lua_Get_Object_Count(lua_State *L);
int Lua_Find_Nearest_Preset(lua_State *L);
int Lua_Find_Random_Preset(lua_State *L);
int Lua_Send_Custom_To_Team_Buildings(lua_State *L);
int Lua_Send_Custom_To_Team_Preset(lua_State *L);
int Lua_Send_Custom_All_Objects_Area(lua_State *L);
int Lua_Send_Custom_All_Objects(lua_State *L);
int Lua_Send_Custom_Event_To_Object(lua_State *L);
int Lua_Is_Unit_In_Range(lua_State *L);
int Lua_Get_Is_Powerup_Persistant(lua_State *L);
int Lua_Get_Powerup_Always_Allow_Grant(lua_State *L);
int Lua_Set_Powerup_Always_Allow_Grant(lua_State *L);
int Lua_Get_Powerup_Grant_Sound(lua_State *L);
int Lua_Grant_Powerup(lua_State *L);
int Lua_Get_Vehicle(lua_State *L);
int Lua_Grant_Refill(lua_State *L);
int Lua_Change_Character(lua_State *L);
int Lua_Create_Vehicle(lua_State *L);
int Lua_Toggle_Fly_Mode(lua_State *L);
int Lua_Get_Vehicle_Occupant_Count(lua_State *L);
int Lua_Get_Vehicle_Occupant(lua_State *L);
int Lua_Get_Vehicle_Driver(lua_State *L);
int Lua_Get_Vehicle_Gunner(lua_State *L);
int Lua_Force_Occupant_Exit(lua_State *L);
int Lua_Force_Occupants_Exit(lua_State *L);
int Lua_Get_Vehicle_Return(lua_State *L);
int Lua_Is_Stealth(lua_State *L);
int Lua_Get_Fly_Mode(lua_State *L);
int Lua_Reload_Lua(lua_State *L);
int Lua_Get_Vehicle_Seat_Count(lua_State *L);
int Lua_Soldier_Transition_Vehicle(lua_State *L);
int Lua_Get_File_List(lua_State *L);
int Lua_Get_Vehicle_Mode(lua_State *L);
int Lua_Get_Team_Vehicle_Count(lua_State *L);
int Lua_Get_Vehicle_Owner(lua_State *L);
int Lua_DNS_IP_To_Host(lua_State *L);
int Lua_Force_Occupants_Exit_Team(lua_State *L);
int Lua_Get_Vehicle_Definition_Mode(lua_State *L);
int Lua_Find_Closest_Zone(lua_State *L);
int Lua_IsInsideZone(lua_State *L);
int Lua_Get_Vehicle_Definition_Mode_By_ID(lua_State *L);
int Lua_Get_Zone_Type(lua_State *L);
int Lua_Get_Zone_Box(lua_State *L);
int Lua_Set_Zone_Box(lua_State *L);
int Lua_Create_Zone(lua_State *L);
int Lua_PointInZone(lua_State *L);
int Lua_Overlap_Test(lua_State *L);
int Lua_IsAvailableForPurchase(lua_State *L);
int Lua_Get_Vehicle_Gunner_Pos(lua_State *L);
//int Lua_Set_Vehicle_Is_Visible(lua_State *L);
int Lua_Set_Vehicle_Gunner(lua_State *L);
int Lua_Get_Model(lua_State *L);
int Lua_Get_Animation_Frame(lua_State *L);
int Lua_Is_TrackedVehicle(lua_State *L);
int Lua_Is_VTOLVehicle(lua_State *L);
int Lua_Is_WheeledVehicle(lua_State *L);
int Lua_Is_Motorcycle(lua_State *L);
int Lua_Is_Door(lua_State *L);
int Lua_Is_Elevator(lua_State *L);
int Lua_Is_DamageableStaticPhys(lua_State *L);
int Lua_Is_AccessablePhys(lua_State *L);
int Lua_Is_DecorationPhys(lua_State *L);
int Lua_Is_HumanPhys(lua_State *L);
int Lua_Is_MotorVehicle(lua_State *L);
int Lua_Is_Phys3(lua_State *L);
int Lua_Is_RigidBody(lua_State *L);
int Lua_Is_ShakeableStatricPhys(lua_State *L);
int Lua_Is_StaticAnimPhys(lua_State *L);
int Lua_Is_StaticPhys(lua_State *L);
int Lua_Is_TimedDecorationPhys(lua_State *L);
int Lua_Is_VehiclePhys(lua_State *L);
int Lua_Is_DynamicAnimPhys(lua_State *L);
int Lua_Is_BuildingAggregate(lua_State *L);
int Lua_Is_Projectile(lua_State *L);
int Lua_Get_Physics(lua_State *L);
int Lua_Copy_Transform(lua_State *L);
int Lua_Get_Mass(lua_State *L);
int Lua_Get_Htree_Name(lua_State *L);
int Lua_Get_Sex(lua_State *L);
int Lua_Create_Effect_All_Stealthed_Objects_Area(lua_State *L);
int Lua_Create_Effect_All_Of_Preset(lua_State *L);
int Lua_Get_GameObj(lua_State *L);
int Lua_Get_Player_ID(lua_State *L);
int Lua_Get_Player_Name(lua_State *L);
int Lua_Get_Player_Name_By_ID(lua_State *L);
int Lua_Change_Team(lua_State *L);
int Lua_Change_Team_By_ID(lua_State *L);
int Lua_Get_Player_Count(lua_State *L);
int Lua_Get_Team_Player_Count(lua_State *L);
int Lua_Get_Team(lua_State *L);
int Lua_Get_Rank(lua_State *L);
int Lua_Get_Kills(lua_State *L);
int Lua_Get_Deaths(lua_State *L);
int Lua_Get_Score(lua_State *L);
int Lua_Get_Money(lua_State *L);
int Lua_Get_Kill_To_Death_Ratio(lua_State *L);
int Lua_Get_Part_Name(lua_State *L);
int Lua_Get_Part_Names(lua_State *L);
int Lua_Get_Team_Color(lua_State *L);
int Lua_Get_Player_Color(lua_State *L);
int Lua_Get_GameObj_By_Player_Name(lua_State *L);
int Lua_Purchase_Item(lua_State *L);
int Lua_Set_Ladder_Points(lua_State *L);
int Lua_Set_Rung(lua_State *L);
int Lua_Set_Money(lua_State *L);
int Lua_Set_Score(lua_State *L);
int Lua_Find_First_Player(lua_State *L);
int Lua_Change_Player_Team(lua_State *L);
int Lua_Tally_Team_Size(lua_State *L);
int Lua_Get_Team_Score(lua_State *L);
int Lua_Send_Custom_All_Players(lua_State *L);
int Lua_Steal_Team_Credits(lua_State *L);
int Lua_Get_Team_Credits(lua_State *L);
int Lua_Change_Team_2(lua_State *L);
int Lua_Get_Player_Type(lua_State *L);
int Lua_Get_Team_Cost(lua_State *L);
int Lua_Get_Cost(lua_State *L);
int Lua_Get_Team_Icon(lua_State *L);
int Lua_Get_Icon(lua_State *L);
int Lua_Remove_Script(lua_State *L);
int Lua_Remove_All_Scripts(lua_State *L);
int Lua_Get_Player_Version(lua_State *L);
int Lua_Kick_Player(lua_State *L);
int Lua_Attach_Script_Preset(lua_State *L);
int Lua_Attach_Script_Type(lua_State *L);
int Lua_Remove_Script_Preset(lua_State *L);
int Lua_Remove_Script_Type(lua_State *L);
int Lua_Add_Console_Hook(lua_State *L);
int Lua_Remove_Weapon(lua_State *L);
int Lua_Set_Fog_Range(lua_State *L);
int Lua_Play_Building_Announcement(lua_State *L);
int Lua_Shake_Camera(lua_State *L);
int Lua_Set_War_Blitz(lua_State *L);
int Lua_Get_Build_Time_Multiplier(lua_State *L);
int Lua_Stop_Background_Music_Player(lua_State *L);
int Lua_Change_Time_Remaining(lua_State *L);
int Lua_Change_Time_Limit(lua_State *L);
int Lua_Create_3D_WAV_Sound_At_Bone(lua_State *L);
int Lua_Send_Message(lua_State *L);
int Lua_Select_Weapon(lua_State *L);
int Lua_Create_2D_Sound(lua_State *L);
int Lua_Create_2D_Sound_Player(lua_State *L);
int Lua_Remove_All_Weapons(lua_State *L);
int Lua_Force_Camera_Look_Player(lua_State *L);
int Lua_Set_Screen_Fade_Opacity_Player(lua_State *L);
int Lua_Set_Screen_Fade_Color_Player(lua_State *L);
int Lua_Enable_Radar_Player(lua_State *L);
int Lua_Set_Background_Music(lua_State *L);
int Lua_Create_Explosion_At_Bone(lua_State *L);
int Lua_Create_Explosion(lua_State *L);
int Lua_Add_RadioHook(lua_State *L);
int Lua_Set_Air_Vehicle_Limit(lua_State *L);
int Lua_Set_Vehicle_Limit(lua_State *L);
int Lua_Get_Vehicle_Limit(lua_State *L);
int Lua_Get_Air_Vehicle_Limit(lua_State *L);
int Lua_Set_Fog_Enable(lua_State *L);
int Lua_Get_Client_Serial_Hash(lua_State *L);
int Lua_Get_Preset_Name_By_Preset_ID(lua_State *L);
int Lua_Is_Crate(lua_State *L);
int Lua_Is_Script_Attached(lua_State *L);
int Lua_Attach_Script_Once(lua_State *L);
int Lua_Attach_Script_Preset_Once(lua_State *L);
int Lua_Attach_Script_Type_Once(lua_State *L);
int Lua_Attach_Script_Building(lua_State *L);
int Lua_Attach_Script_Is_Preset(lua_State *L);
int Lua_Attach_Script_Is_Type(lua_State *L);
int Lua_Attach_Script_Player_Once(lua_State *L);
int Lua_Remove_Duplicate_Script(lua_State *L);
int Lua_Attach_Script_All_Buildings_Team(lua_State *L);
int Lua_Attach_Script_All_Turrets_Team(lua_State *L);
int Lua_Find_Building_With_Script(lua_State *L);
int Lua_Find_Object_With_Script(lua_State *L);
int Lua_Get_Translated_String(lua_State *L);
int Lua_Get_Translated_Preset_Name(lua_State *L);
int Lua_Get_Translated_Weapon(lua_State *L);
int Lua_Get_Current_Translated_Weapon(lua_State *L);
int Lua_Get_Team_Name(lua_State *L);
int Lua_Get_Vehicle_Name(lua_State *L);
int Lua_Get_Translated_Definition_Name(lua_State *L);
int Lua_Get_Current_Bullets(lua_State *L);
int Lua_Get_Current_Clip_Bullets(lua_State *L);
int Lua_Get_Current_Total_Bullets(lua_State *L);
int Lua_Get_Total_Bullets(lua_State *L);
int Lua_Get_Clip_Bullets(lua_State *L);
int Lua_Get_Bullets(lua_State *L);
int Lua_Get_Current_Max_Bullets(lua_State *L);
int Lua_Get_Current_Clip_Max_Bullets(lua_State *L);
int Lua_Get_Current_Total_Max_Bullets(lua_State *L);
int Lua_Get_Max_Total_Bullets(lua_State *L);
int Lua_Get_Max_Clip_Bullets(lua_State *L);
int Lua_Get_Max_Bullets(lua_State *L);
int Lua_Get_Position_Total_Bullets(lua_State *L);
int Lua_Get_Position_Bullets(lua_State *L);
int Lua_Get_Position_Clip_Bullets(lua_State *L);
int Lua_Get_Position_Total_Max_Bullets(lua_State *L);
int Lua_Get_Position_Max_Bullets(lua_State *L);
int Lua_Get_Position_Clip_Max_Bullets(lua_State *L);
int Lua_Set_Current_Bullets(lua_State *L);
int Lua_Set_Current_Clip_Bullets(lua_State *L);
int Lua_Set_Position_Bullets(lua_State *L);
int Lua_Set_Position_Clip_Bullets(lua_State *L);
int Lua_Set_Bullets(lua_State *L);
int Lua_Set_Clip_Bullets(lua_State *L);
int Lua_Get_Powerup_Weapon(lua_State *L);
int Lua_Get_Weapon_Ammo_Definition(lua_State *L);
int Lua_Get_Current_Weapon_Ammo_Definition(lua_State *L);
int Lua_Get_Position_Weapon_Ammo_Definition(lua_State *L);
int Lua_Get_Weapon_Definition(lua_State *L);
int Lua_Get_Current_Weapon_Definition(lua_State *L);
int Lua_Get_Position_Weapon_Definition(lua_State *L);
int Lua_Get_Explosion(lua_State *L);
int Lua_Get_Powerup_Weapon_By_Obj(lua_State *L);
int Lua_Get_Current_Weapon_Style(lua_State *L);
int Get_Position_Weapon_Style(lua_State *L);
int Lua_Get_Weapon_Style(lua_State *L);
int Lua_Disarm_Beacon(lua_State *L);
int Lua_Disarm_Beacons(lua_State *L);
int Lua_Disarm_C4(lua_State *L);
int Lua_Get_Current_Weapon(lua_State *L);
int Lua_Get_Weapon_Count(lua_State *L);
int Lua_Get_Weapon(lua_State *L);
int Lua_Has_Weapon(lua_State *L);
int Lua_Find_Beacon(lua_State *L);
int Lua_Get_Vehicle_Lock_Owner(lua_State *L);
int Lua_Get_C4_Count(lua_State *L);
int Lua_Get_Beacon_Count(lua_State *L);
int Lua_Get_Mine_Limit(lua_State *L);
int Lua_Get_C4_Mode(lua_State *L);
int Lua_Get_C4_Planter(lua_State *L);
int Lua_Get_C4_Attached(lua_State *L);
int Lua_Get_Beacon_Planter(lua_State *L);
int Lua_Create_Object(lua_State *L);
int Lua_Destroy_Object(lua_State *L);
int Lua_Get_Preset_Name(lua_State *L);
int Lua_Get_Position(lua_State *L);
int Lua_Enable_Vehicle_Transitions(lua_State *L);
int Lua_Set_Model(lua_State *L);
int Lua_Goto_Location(lua_State *L);
int Lua_Goto_Object(lua_State *L);
int Lua_Disable_Physical_Collisions(lua_State *L);
int Lua_Enable_Collisions(lua_State *L);
int Lua_Random_Building(lua_State *L);
int Lua_Get_Current_Map(lua_State *L);
int Lua_Get_Next_Map(lua_State *L);
int Lua_Set_Position(lua_State *L);
int Lua_Apply_Damage(lua_State *L);
int Lua_Get_Facing(lua_State *L);
int Lua_Set_Facing(lua_State *L);
int Lua_Set_Clouds(lua_State *L);
int Lua_Set_Ash(lua_State *L);
int Lua_Set_Rain(lua_State *L);
int Lua_Set_Snow(lua_State *L);
int Lua_Set_Wind(lua_State *L);
int Lua_Attach_Script(lua_State *L);
int Lua_Create_Object_At_Bone(lua_State *L);
int Lua_Attach_To_Object_Bone(lua_State *L);
int Lua_Display_Health_Bar(lua_State *L);

/*  ------------------------------------------------------------------------------------
                              Sla Company Functions
    ------------------------------------------------------------------------------------
*/

int Lua_SBHCostume(lua_State *L);
int Lua_Get_All_Buildings(lua_State *L);
int Lua_Set_Shield_Type(lua_State *L);
int Lua_FDSMessage(lua_State *L);
int Lua_Get_Rotation(lua_State *L);
int Lua_Set_Health(lua_State *L);
int Lua_Set_Shield_Strength(lua_State *L);
int Lua_The_Game(lua_State *L);
//int Lua_cPlayer(lua_State *L);
int Lua_LongToIP(lua_State*L);
int Lua_IPToLong(lua_State*L);
int Lua_GetTimeRemaining(lua_State *L);
int Lua_Get_Health(lua_State *L);
int Lua_Get_Max_Health(lua_State *L);
int Lua_Get_Shield_Strength(lua_State *L);
int Lua_Set_Max_Speed(lua_State *L);
int Lua_Get_Max_Speed(lua_State *L);
int Lua_Warp_Player(lua_State *L);
int Lua_Get_Max_Shield_Strength(lua_State *L);
int Lua_Hide_Object_Player(lua_State *L);
int Lua_Kill_Player(lua_State *L);
int Lua_Set_Is_Powerup_Persistant(lua_State *L);
int Lua_Freeze_Player(lua_State *L);
int Lua_Get_BW_Player(lua_State *L);
int Lua_Get_Vehicles_Limit(lua_State *L);
int Lua_Display_GDI_Player_Terminal_Player(lua_State *L);
int Lua_Display_Nod_Player_Terminal_Player(lua_State *L);
int Lua_Get_IDobj(lua_State *L);
int Lua_Specate(lua_State *L);
int Lua_HUD_Pokable(lua_State *L);
int Lua_Set_Background_Music(lua_State *L);
int Lua_Get_Background_Music(lua_State *L);
int Lua_Get_Map_By_Number(lua_State *L);

int Lua_Set_Damage_Points(lua_State *L);
int Lua_Set_Death_Points(lua_State *L);
int Lua_Damage_Objects_Half(lua_State *L);
int Lua_Get_Death_Points(lua_State *L);
int Lua_Get_Damage_Points(lua_State *L);
int Lua_Kill_Occupants(lua_State *L);
int Lua_Damage_All_Objects_Area(lua_State *L);
int Lua_Damage_All_Vehicles_Area(lua_State *L);
int Lua_Damage_All_Buildings_By_Team(lua_State *L);
int Lua_Set_Info_Texture(lua_State *L);
int Lua_Clear_Info_Texture(lua_State *L);
int Lua_Set_Naval_Vehicle_Limit(lua_State *L);
int Lua_Get_Naval_Vehicle_Limit(lua_State *L);
int Lua_Send_Message_Player(lua_State *L);
//int Lua_Set_Wireframe_Mode(lua_State *L);
int Lua_Load_New_HUD_INI(lua_State *L);
int Lua_Change_Radar_Map(lua_State *L);
int Lua_Set_Currently_Building(lua_State *L);
int Lua_Is_Currently_Building(lua_State *L);
int Lua_Set_Fog_Color(lua_State *L);
int Lua_Display_Security_Dialog(lua_State *L);
int Lua_Do_Objectives_Dlg(lua_State *L);
int Lua_Set_Player_Limit(lua_State *L);
int Lua_Get_Player_Limit(lua_State *L);
int Lua_Set_GDI_Soldier_Name(lua_State *L);
int Lua_Set_Nod_Soldier_Name(lua_State *L);
int Lua_Set_Moon_Is_Earth(lua_State *L);
int Lua_Get_Revision(lua_State *L);
int Lua_Can_Team_Build_Vehicle(lua_State *L);
int Lua_Find_Naval_Factory(lua_State *L);
int Lua_Vehicle_Preset_Is_Air(lua_State *L);
int Lua_Vehicle_Preset_Is_Naval(lua_State *L);
int Lua_Busy_Preset_By_Name(lua_State *L);
int Lua_Hide_Preset_By_Name(lua_State *L);
int Lua_Attach_Script_Occupants(lua_State *L);
int Lua_Destroy_Lua_Thread(lua_State *L);
int Lua_Create_Lua_Thread(lua_State *L);
int Lua_Restart_Lua(lua_State *L);
int Lua_Get_Lua_Thread_By_Name(lua_State *L);
int Lua_tClock(lua_State *L); //I would expect this to be already implimented in Lua :(
int Lua_Set_Kills(lua_State *L);
int Lua_Set_Is_Rendered(lua_State *L);

void AddFunctions(lua_State *L);
/*
Style_C4 0 //note that this list reflects the list in leveledit
Style_None 1
Style_Shoulder 2
Style_Hip 3
Style_Launcher 4
Style_Handgun 5
Style_Beacon 6
CHARACTERS 0
VEHICLES 1
EQUIPMENT 2
SECRETCHARS 3
SECRETVEHICLES 4
DEFAULT 0 
CTF 1
VEHICLE_CONSTRUCTION 2
VEHICLE_REPAIR 3
TIBERIUM 4
BEACON 5
GDITIBERIUM 6
NODTIBERIUM 7
CAR 0 
TANK 1
BIKE 2
FLYING 3
TURRET 4
NONE -1
POWER_PLANT 0
SOLDIER_FACTORY 1
VEHICLE_FACTORY 2
REFINERY 3
COM_CENTER 4
REPAIR_BAY 5
SHRINE 6
HELIPAD 7
CONYARD 8
BASE_DEFENSE 9
*/







