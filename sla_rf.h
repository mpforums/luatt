/*	Renegade Scripts.dll
	Scripts by Stan "sla.ro" Laurentiu Alexandru 
	Copyright 2010-2011 Sla Company (http://sla-co.webs.com)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it 
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code 
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#pragma once

// My Little Mutant Stuff

class sla_mlm_teleportportal : public ScriptImpClass {
	void Entered(GameObject *obj, GameObject *p);
};

class sla_mlm_selectpony : public ScriptImpClass {
	void Entered(GameObject *obj, GameObject *p);
};

class sla_mlm_teleport_controller : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	int objective;
	int teleports[5];
};

class sla_mlm_teleport : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	int teleports[4];
};

class sla_mlm_portal_controller : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Rise(GameObject *obj);

	bool c[6];
};

class sla_mlm_portal_button : public ScriptImpClass {
	void Poked(GameObject *obj,GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
	void Created(GameObject *obj);
	bool active;
}; 

class sla_mlm_boss_heal : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
	bool healing;
	bool hit;
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Killed(GameObject *obj,GameObject *killer);
};

class sla_rfe_aitrain : public ScriptImpClass {
	void Timer_Expired(GameObject *obj, int number);
	void CreateBots(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class sla_rfe_controller : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
	bool critical_message;
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	int bots;
	int wave;
	bool game_end;
};

class rf_intel_start : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_intel_firestorm : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_intel_pp : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_intel_facility : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_intel_documents : public ScriptImpClass {
	bool blockme;
	void Poked(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_station_wait : public ScriptImpClass {
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
	void Exited(GameObject *obj, GameObject *p);
	void Created(GameObject *obj);
	int arrival;
	int onplatform;
	bool trainstarted;
	bool announced;
};

class rf_station_near : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_station_start : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};

class rf_express_start : public ScriptImpClass {
	bool blockme;
	void Entered(GameObject *obj, GameObject *p);
	void Timer_Expired(GameObject *obj, int number);
};


