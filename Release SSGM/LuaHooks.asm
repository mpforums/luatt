; Listing generated by Microsoft (R) Optimizing Compiler Version 18.00.21005.1 

	TITLE	C:\Users\Administrator\Desktop\source\LuaTT\LuaHooks.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB OLDNAMES

PUBLIC	?main_loop_glue@@YAXXZ				; main_loop_glue
	ALIGN	4

_allocator_arg DB 01H DUP (?)
	ALIGN	4

_piecewise_construct DB 01H DUP (?)
_BSS	ENDS
; Function compile flags: /Ogtpy
; File c:\users\administrator\desktop\source\luatt\luahooks.cpp
;	COMDAT ?main_loop_glue@@YAXXZ
_TEXT	SEGMENT
?main_loop_glue@@YAXXZ PROC				; main_loop_glue, COMDAT

; 18   : 	__asm
; 19   : 	{
; 20   : 		call LuaManager::Call_Think_Hook;

	call	?Call_Think_Hook@LuaManager@@SAXXZ	; LuaManager::Call_Think_Hook

; 21   : 
; 22   : 		pop edi;

	pop	edi

; 23   : 		pop esi;

	pop	esi

; 24   : 		add esp, 0Ch;

	add	esp, 12					; 0000000cH

; 25   : 		ret;

	ret	0
?main_loop_glue@@YAXXZ ENDP				; main_loop_glue
_TEXT	ENDS
END
