#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "AABoxClass.h"
#include "plugin.h"


extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}

#include "Misc.h"
#include "LuaManager.h"

#include "CommandLineParser.h"
#include "ConnectionRequest.h"


luatt::luatt()
{
	

	RegisterEvent(EVENT_GLOBAL_INI,this);
	RegisterEvent(EVENT_MAP_INI,this);
	RegisterEvent(EVENT_CHAT_HOOK,this);
	RegisterEvent(EVENT_OBJECT_CREATE_HOOK,this);
	RegisterEvent(EVENT_LOAD_LEVEL_HOOK,this);
	RegisterEvent(EVENT_GAME_OVER_HOOK,this);
	RegisterEvent(EVENT_PLAYER_JOIN_HOOK,this);
	RegisterEvent(EVENT_PLAYER_LEAVE_HOOK,this);
	RegisterEvent(EVENT_REFILL_HOOK,this);
	RegisterEvent(EVENT_POWERUP_PURCHASE_HOOK,this);
	RegisterEvent(EVENT_VEHICLE_PURCHASE_HOOK,this);
	RegisterEvent(EVENT_CHARACTER_PURCHASE_HOOK,this);
	RegisterEvent(EVENT_THINK_HOOK,this);
	// hooks
	AddHostHook(LuaManager::Call_Host_Hook);
	addConnectionAcceptanceFilter(this);

	LuaLoad();
}


void luatt::handleInitiation(const ConnectionRequest& connectionRequest)
{
}
	
void luatt::handleTermination(const ConnectionRequest& connectionRequest)
{
}
	
void luatt::handleCancellation(const ConnectionRequest& connectionRequest)
{
}
	
ConnectionAcceptanceFilter::STATUS luatt::getStatus(const ConnectionRequest& connectionRequest, WideStringClass& refusalMessage)
{
	STATUS status;
	const char *refused;
	if (connectionRequest.clientSerialHash.Is_Empty()) // Called when old clients have not yet sent their serial numbers.
		status = STATUS_INDETERMINATE;
	else
	{
		// code goes here

		
			// get ip
	    char ip[40];
		char serialp[55];
		char version[55];
		char nick[55];

        int ip1,ip2,ip3,ip4;
                ip1 = connectionRequest.clientAddress.sin_addr.s_addr&0x000000FF;
                ip2 = (connectionRequest.clientAddress.sin_addr.s_addr&0x0000FF00)>>8;
                ip3 = (connectionRequest.clientAddress.sin_addr.s_addr&0x00FF0000)>>16;
                ip4 = (connectionRequest.clientAddress.sin_addr.s_addr&0xFF000000)>>24;

       sprintf(serialp, "%s" ,connectionRequest.clientSerialHash);
       sprintf(ip,"%d.%d.%d.%d",ip1,ip2,ip3,ip4);
	   sprintf(nick, "%ls", connectionRequest.clientName);
	   sprintf(version, "%f", connectionRequest.clientVersion);

		refused = LuaManager::ConAc((const char*)ip, (const char*)nick, (const char*)serialp, (const char*)version);
		refusalMessage.Format((const wchar_t *)refused);

     if (strcmp(refused, "CONTINUE") == 0) 
		 status = STATUS_ACCEPTING;
	 else {
		 refusalMessage.Format(L"%S", (StringClass)refused);
		 status=STATUS_REFUSING;
	 }
	 
	}
	return status;
}

//luatt Luatt;


luatt::~luatt()
{
	LuaManager::UnloadingLua();
	UnregisterEvent(EVENT_GLOBAL_INI,this);
	UnregisterEvent(EVENT_MAP_INI,this);
	UnregisterEvent(EVENT_CHAT_HOOK,this);
	UnregisterEvent(EVENT_OBJECT_CREATE_HOOK,this);
	UnregisterEvent(EVENT_LOAD_LEVEL_HOOK,this);
	UnregisterEvent(EVENT_GAME_OVER_HOOK,this);
	UnregisterEvent(EVENT_PLAYER_JOIN_HOOK,this);
	UnregisterEvent(EVENT_PLAYER_LEAVE_HOOK,this);
	UnregisterEvent(EVENT_REFILL_HOOK,this);
	UnregisterEvent(EVENT_POWERUP_PURCHASE_HOOK,this);
	UnregisterEvent(EVENT_VEHICLE_PURCHASE_HOOK,this);
	UnregisterEvent(EVENT_CHARACTER_PURCHASE_HOOK,this);
	UnregisterEvent(EVENT_THINK_HOOK,this);
	LuaUnload();

//	removeConnectionAcceptanceFilter(this);
}

void luatt::OnLoadGlobalINISettings(INIClass *SSGMIni)
{

}

void luatt::OnFreeData()
{

}






/*
// Code by iRANian
// http://www.renegadeforums.com/index.php?t=msg&th=40864&start=0

int TTChatHookAddress;
int TTChatHookJMPAddress;
int TTPrologueFuncCallAddress;

bool ReadMemory(int Address, void* Buffer, int Size)
{
	bool ret = 1;
	DWORD OldProtect;
	HANDLE Process = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, false, GetCurrentProcessId());
	VirtualProtectEx(Process, (LPVOID)Address, Size, PAGE_EXECUTE_READWRITE, &OldProtect);
	if (!ReadProcessMemory(Process, (LPVOID)Address, Buffer, Size, NULL))
	{
		Console_Output("Reading process memory adress 0x%x failed\n", Address);
		Console_Output("GetLastError() = %d\n", GetLastError());
		ret = 0;
	}
	VirtualProtectEx(Process, (LPVOID)Address, Size, OldProtect, NULL);
	CloseHandle(Process);
	return ret;
}

bool WriteMemory(int Address, const void* Buffer, int Size)
{
	bool ret = 1;
	DWORD OldProtect;
	HANDLE Process = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, false, GetCurrentProcessId());
	VirtualProtectEx(Process, (LPVOID)Address, Size, PAGE_EXECUTE_READWRITE, &OldProtect);
	if (!WriteProcessMemory(Process, (LPVOID)Address, Buffer, Size, NULL))
	{
		Console_Output("Hooking address 0x%x failed\n", Address);
		Console_Output("GetLastError() = %d\n", GetLastError());
		ret = 0;
	}
	VirtualProtectEx(Process, (LPVOID)Address, Size, OldProtect, NULL);
	CloseHandle(Process);
	return ret;
}

void Install_Hook(char OpCode, int Addr, int Glue, char *Padding)
{
	int HookAddress = Addr;
	int Offset = Glue - HookAddress - 5;
	WriteMemory(HookAddress, &OpCode, 1);
	WriteMemory(HookAddress + 1, &Offset, 4);
};

int Calculate_Address_From_Displacement(int JMPStartAddress)
{
	char OpCodes[5];
	int Displacement, Address;

	ReadMemory(JMPStartAddress, OpCodes, 5);
	Console_Output("BYTES READ: 0x%x 0x%x 0x%x 0x%x 0x%x \n", OpCodes[0], OpCodes[1], OpCodes[2], OpCodes[3], OpCodes[4]);

	memcpy(&Displacement, OpCodes + 1, sizeof(char)* 4); // OpCodeBuffer+1 or we'll also read the JMP opcode

	Address = JMPStartAddress + 5 + Displacement;
	return Address;
}

bool _cdecl Private_Chat_Hook(int PlayerID, int Type, wchar_t *Message, int TargetID)
{
	//if (Type == 2) { } // Only trigger on valid private chat

	//Console_Output("PlayerID = %d, TargetID = %d, type = %d, Message = %S\n", PlayerID, TargetID, Type, Message);
	
	//return false;
	return LuaManager::Call_Chat_Hook(PlayerID, Type, Message, TargetID);
}

void _declspec(naked) PrivateChatHook_Glue()
{
	_asm
	{
		push esi
			mov eax, esi
			mov esi, ecx // save ecx

			push[ecx + 06C0h] // arg 4, TargetID
			push[ecx + 06BCh] // arg 3, Message
			push[ecx + 06B8h] // arg 2, Type
			push[ecx + 06B4h] // arg 1, PlayerID
			call Private_Chat_Hook
			add esp, 16;

		mov ecx, esi // restore ecx

			test al, al
			jz Block_Private_Chat

			//call TTPrologueFuncCallAddress
			jmp TTChatHookJMPAddress

		Block_Private_Chat :
		pop esi
			retn
	}
}*/

void luatt::OnLoadMapINISettings(INIClass *SSGMIni)
{
	/*TTChatHookAddress = Calculate_Address_From_Displacement(0x004B5C10); // Hook from  cCsTextObj::Act(void)
	TTChatHookJMPAddress = TTChatHookAddress + 8;
	TTPrologueFuncCallAddress = Calculate_Address_From_Displacement(TTChatHookAddress + 3);


	Install_Hook('\xE9', TTChatHookAddress, (int)&PrivateChatHook_Glue, "");*/
}

void luatt::OnFreeMapData()
{

}

bool luatt::OnChat(int PlayerID,TextMessageEnum Type,const wchar_t *Message,int recieverID)
{
	int ret = LuaManager::Call_Chat_Hook(PlayerID, Type, Message, recieverID);
	return ret == 0 ? false : true;
}

void luatt::OnObjectCreate(void *data,GameObject *obj)
{
//	LuaManager::Call_Object_Hook(data, obj); //called already by Lua's hooks
}


void luatt::OnLoadLevel()
{
	LuaManager::Call_Level_Loaded_Hook();
	
}

void luatt::OnGameOver()
{
	LuaManager::Call_GameOver_Hook();
}

void luatt::OnPlayerJoin(int PlayerID,const char *PlayerName)
{
	if (!PlayerID) { return; }
	if (!PlayerName) { return; }
	LuaManager::Call_Player_Join_Hook(PlayerID, PlayerName);
}

void luatt::OnPlayerLeave(int PlayerID)
{
	if (!PlayerID) { return; }
	LuaManager::Call_Player_Leave_Hook(PlayerID);
}

bool luatt::OnRefill(GameObject *purchaser)
{ 
 if (!purchaser) { return true; }
 int ret = LuaManager::Refill_Hook(purchaser);
 return ret == 0 ? false : true;
  //return true;
}

int luatt::OnPowerupPurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data)
{
	//LuaManager::Purchase_Hook(base,purchaser,cost,preset,data);
	return -1;
}

int luatt::OnVehiclePurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data)
{
	int i = LuaManager::Purchase_Hook2(base,purchaser,cost,preset,data);
	if (!i) { return -1; }
	return i;
}

int luatt::OnCharacterPurchase(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data)
{
    //LuaManager::Purchase_Hook(base,purchaser,cost,preset,data);
	return -1;
}

void luatt::OnThink()
{
	LuaManager::Call_Think_Hook();
}

void luatt::Log_Output(const char *message)
{
	if (!message) { return; }
//	LuaManager::SSGM_Log(message); // not work
}

luatt luaplugin;

extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &luaplugin;
}


