/*	Sla's Scripts
	Scripts by Stan "sla.ro" Laurentiu Alexandru 
	Copyright 2010-2011 Sla Company (http://sla-co.webs.com)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it 
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code 
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "AABoxClass.h"
#include "plugin.h"

#include "GameObjManager.h"
#include "HarvesterClass.h"
#include "PhysClass.h"
#include "MoveablePhysClass.h"
#include "PhysicsSceneClass.h"
#include "gmvehicle.h"

#include "sla.h"

void sla_tut_start::Created(GameObject *obj)
{
	used = false;
}

void sla_tut_start::Entered(GameObject *obj, GameObject *enter)
{
	if (!used) {
		used = true;
	    Commands->Start_Timer(obj,this,4.0f,1);	
	}
}

void sla_on_enter_message::Entered(GameObject *obj, GameObject *enter)
{
	Console_Input("snda com_ion_beep.wav");
	Send_Message(15,255,0,Get_Parameter("message"));
	Destroy_Script();
}

void sla_tut_start::Timer_Expired(GameObject *obj, int number) 
{
	if (number == 1) {
         Send_Message(255,204,0,"Welcome to Sla Mutant CO-OP by Sla Company (C) 2012-2014");
		 Console_Input("snda com_ion_beep.wav");
		 Commands->Start_Timer(obj,this,3.0f,2);
	}
	else if (number == 2) {
         Send_Message(255,204,0,"We found that GDI hides some secret documents around those bases.");
		Console_Input("snda com_ion_beep.wav");
		 Commands->Start_Timer(obj,this,3.0f,3);
	}
	else if (number == 3) {
         Send_Message(255,204,0,"Steal the intels from every building around this map.");
		 Console_Input("snda com_ion_beep.wav");
		 Commands->Start_Timer(obj,this,3.0f,4);
	}
    else if (number == 4) {
         Send_Message(255,204,0,"Good Luck! You need it.");
		 Console_Input("snda com_ion_beep.wav");
		 Commands->Start_Timer(obj,this,3.0f,5);
	}
	else if (number == 5) {
         Send_Message(255,204,0,"New Primary Objective: Get all intels and exit the base.");
		 Console_Input("snda com_ion_beep.wav");
		 Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("gateid")));
		 Commands->Start_Timer(obj,this,1.0f,6);
	}
	else if (number == 6) {
		 Destroy_Script();
	}
	
}

void sla_enable_poke::Created(GameObject *obj)
{
	Commands->Enable_HUD_Pokable_Indicator(obj, true);
}

void sla_enable_poke::Poked(GameObject *obj,GameObject *poker)
{
   Commands->Enable_HUD_Pokable_Indicator(obj, false);
}


void sla_custom_waypathfollow::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("msg")) {
	int waypathid;
			float speed;
			waypathid = Get_Int_Parameter("Waypathid");
			speed = 1.0f;
			ActionParamsStruct params;
			params.MoveArrivedDistance = 100;
			params.Set_Basic(this,100,777);
			params.Set_Movement(0,speed,10);
			params.WaypathID = waypathid;
			params.WaypathSplined = true;
			params.AttackActive = false;
			params.AttackCheckBlocked = false;
			Commands->Action_Goto(obj,params);
			Commands->Action_Goto(obj,params);
	}
}

void sla_enter_send_custom::Entered(GameObject *obj,GameObject *enter)
{
    GameObject *object;
	int message;
	int ID;
	int param;
    ID = Get_Int_Parameter("ID");
	message = Get_Int_Parameter("Message");
	object = Commands->Find_Object(ID);
	param = Commands->Get_ID(enter);
	Commands->Send_Custom_Event(obj,object,message,param,0);
	 Destroy_Script();
}


void sla_kill_distroyobject::Killed(GameObject *obj,GameObject *shooter)
{
	GameObject *object;
	object = Commands->Find_Object(Get_Int_Parameter("ID"));
	Commands->Destroy_Object(object);
	Destroy_Script();
}

void sla_poke_distroyobject::Poked(GameObject *obj,GameObject *poker)
{
	GameObject *object;
	object = Commands->Find_Object(Get_Int_Parameter("ID"));
	Commands->Destroy_Object(object);
	Destroy_Script();
}


void sla_poke_msg::Poked(GameObject *obj,GameObject *poker)
{
	    Commands->Create_2D_Sound("com_ion_beep.wav");
		Send_Message(15,255,0,Get_Parameter("message"));
		Destroy_Script();
}

void sla_tut_final::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 100) {
		final = true;
	}
}

void sla_enter_changeteam::Entered(GameObject *obj,GameObject *enter)
{
	char cht[200];
	if (Get_Int_Parameter("Team") == 0) {
	 sprintf(cht,"team2 %d 0",Get_Player_ID(enter));
	}
	else if (Get_Int_Parameter("Team") == 1) {
	 sprintf(cht,"team2 %d 1",Get_Player_ID(enter));
	}
	else if (Get_Int_Parameter("Team") == 2) {
	 sprintf(cht,"team2 %d 2",Get_Player_ID(enter));
	}
	Console_Input(cht);
}


void sla_tut_final::Entered(GameObject *obj,GameObject *enter)
{
	if (final == true) {
		final = false; // block flood
		Commands->Start_Timer(obj,this,1.0f,10);
		Commands->Start_Timer(obj,this,4.0f,20);
		Commands->Start_Timer(obj,this,7.0f,30);
	}
}

void sla_tut_final::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Console_Input("snda m00achk_kill0025i1nors_snd.wav");
	}

	if (number == 20) {
		Console_Input("snda m00_wins0001eval_snd.wav");
	}

	if (number == 30) {
		Console_Input("snda m00_wins0001evag_snd.wav");
	}
}


void sla_custom_playcinematic::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 100) {
	 GameObject *object = Commands->Create_Object("Invisible_Object",Commands->Get_Position(obj));
	 Commands->Attach_Script(object,"JFW_Cinematic", Get_Parameter("Script_Name"));
	}
}


void sla_tut_phase1::Poked(GameObject *obj,GameObject *poker)
{
	if (poked) {
		return;
	}
	poked = true; // player can't poke it again without sense like a retard :D
	Commands->Start_Timer(obj,this,20.0f,100);
	//Commands->Start_Timer(obj,this,40.0f,100);
	//Commands->Start_Timer(obj,this,80.0f,100);
}

void sla_enter_enable_spawn::Entered(GameObject *obj,GameObject *enter)
{
	if (Get_Int_Parameter("Enable") == 0) {
      Commands->Enable_Spawner(Get_Int_Parameter("ID"),true);
	}
	else {
	  Commands->Enable_Spawner(Get_Int_Parameter("ID"),false);
	}
}


void sla_tut_phase1::Timer_Expired(GameObject *obj, int number) 
{
	if (number == 100) {
    Commands->Enable_Spawner(Get_Int_Parameter("ID1"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID2"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID3"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID4"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID5"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID6"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID7"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID8"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID9"),true);
	Commands->Enable_Spawner(Get_Int_Parameter("ID10"),true);
	}
}


void sla_parachute::Created(GameObject *obj) {
  float Facing = Commands->Get_Facing(obj);
  Commands->Attach_Script(obj,"M00_No_Falling_Damage_DME","");
  parachute = Commands->Create_Object_At_Bone(obj,"Invisible_Object", "c CHEST");
  Commands->Set_Facing(parachute,Facing);
  char idx[100];
  sprintf(idx,"%d",Commands->Get_ID(obj));
  Commands->Attach_Script(parachute,"MDB_SSGM_Destroy_When_Object_Destroyed",idx);
  parachuteID = Commands->Get_ID(parachute);
  Commands->Set_Model(parachute, "X5D_Parachute");
  Commands->Attach_To_Object_Bone(parachute, obj, "c CHEST");
  Commands->Start_Timer(obj,this,10.0f,1);
}

void sla_parachute::Timer_Expired(GameObject *obj, int number) {
  Commands->Destroy_Object(parachute);
  Destroy_Script();
}

void sla_m01_prisoniercont::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
 if (message == 1) {
	if (killx == 5) {
		GameObject *object;
	    object = Commands->Find_Object(Get_Int_Parameter("distroyid1"));
	    Commands->Destroy_Object(object);
		object = Commands->Find_Object(Get_Int_Parameter("distroyid2"));
	    Commands->Destroy_Object(object);
		object = Commands->Find_Object(Get_Int_Parameter("distroyid3"));
	    Commands->Destroy_Object(object);
		object = Commands->Find_Object(Get_Int_Parameter("distroyid4"));
	    Commands->Destroy_Object(object);
	    Send_Message(255,204,0,"Thank you for saving us!");
	}
	else if (killx == 5) {
		killx = killx + 1;
	}
 }
}



void sla_m01_start::Entered(GameObject *obj,GameObject *enter)
{
		Commands->Start_Timer(obj,this,1.0f,10);
		Commands->Start_Timer(obj,this,3.0f,20);
}

void sla_m01_start::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Send_Message(255,204,0,"Briefing: Find the detention center and save prisoniers.");
		Commands->Create_2D_Sound("com_ion_beep.wav");
	}

	if (number == 20) {
		Send_Message(255,204,0,"GDI Base Detected!");
		Commands->Create_2D_Sound("com_ion_beep.wav");
	}
}



void sla_teleport::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		tele = true;
		Commands->Enable_HUD_Pokable_Indicator(obj,true);
	}
}



void sla_teleport::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 1) {
		spawn1 = true;
	}
	else if (message == 2) {
		spawn1 = false;
		spawn2 = true;
		spawn1 = false;
		spawn3 = false;
		spawn4 = false;
	}
	else if  (message == 3) {
		spawn1 = false;
		spawn2 = false;
		spawn3 = true;
		spawn4 = false;
	}
	else if  (message == 4) {
		spawn1 = false;
		spawn2 = false;
		spawn3 = false;
		spawn4 = true;
	}
}


void sla_teleport::Poked(GameObject *obj,GameObject *poker)
{
	char message[1000];
  if (tele == true) {
	  Commands->Enable_HUD_Pokable_Indicator(obj,false);
	  tele = false;
	  Commands->Start_Timer(obj,this,3.0f,10);
	if (spawn1 == true) {
		sprintf(message,"ppage %d Teleporting..",Get_Player_ID(poker));
	    Console_Input(message);
	    int x = Get_Int_Parameter("tel1");
		GameObject *gotoObject = Commands->Find_Object(x);
		Vector3 gotoLocation = Commands->Get_Position(gotoObject);
		Commands->Set_Position(poker,gotoLocation);
	}
	else if (spawn2 == true) {
		sprintf(message,"ppage %d Teleporting..",Get_Player_ID(poker));
	    Console_Input(message);
	 int x = Get_Int_Parameter("tel2");
		GameObject *gotoObject = Commands->Find_Object(x);
		Vector3 gotoLocation = Commands->Get_Position(gotoObject);
		Commands->Set_Position(poker,gotoLocation);
	}
	else if (spawn3 == true) {
		sprintf(message,"ppage %d Teleporting..",Get_Player_ID(poker));
	    Console_Input(message);
	int x = Get_Int_Parameter("tel3");
		GameObject *gotoObject = Commands->Find_Object(x);
		Vector3 gotoLocation = Commands->Get_Position(gotoObject);
		Commands->Set_Position(poker,gotoLocation);
	}
	else if (spawn4 == true) {
		sprintf(message,"ppage %d Teleporting..",Get_Player_ID(poker));
	    Console_Input(message);
	 int x = Get_Int_Parameter("tel4");
		GameObject *gotoObject = Commands->Find_Object(x);
		Vector3 gotoLocation = Commands->Get_Position(gotoObject);
		Commands->Set_Position(poker,gotoLocation);
	}
	else {
		sprintf(message,"ppage %d Teleport is not open !",Get_Player_ID(poker));
	    Console_Input(message);
	}

  }
  else {
	  sprintf(message,"ppage %d Teleporting is temporary offline..",Get_Player_ID(poker));
	  Console_Input(message);
  }
}


void sla_teleport::Created(GameObject *obj)
{
	tele = true;
	spawn1 = false;
	spawn2 = false;
	spawn3 = false;
	spawn4 = false;
}


void sla_m01_foodcart::Entered(GameObject *obj,GameObject *enter)
{
		Send_Message(255,204,0,"Very good havoc, i give you a gdi humvee.");
	    int ID = Get_Int_Parameter("ID");
	    GameObject *object = Commands->Find_Object(ID);
		Commands->Send_Custom_Event(enter,object,100,100,1.0f);
		  Destroy_Script();
}


void sla_custom_cinematic::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message") && can != false) {
	   //Remove_Script(obj,"JFW_Cinematic");
	   can = false;
       Commands->Attach_Script(obj,"JFW_Cinematic",Get_Parameter("Script_Name"));
	}
}


void sla_created_cinematic_attack::Created(GameObject *obj)
{
	ActionParamsStruct params;
	int priority = 100;
	float range = 1000.0f;
	float deviation = 0.0f;
	bool primary = true;
	//GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("Move_ID"));
	//params.Set_Movement(GotoObject,10.0f,50.0f);
	//params.MoveFollow = true;
	//Commands->Action_Goto(obj,params);
	params.Set_Basic(this,(float)priority,40016);
	params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("ID")),range,deviation,primary);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj,params);
	Commands->Start_Timer(obj,this,3.0f,250);
}

void sla_created_cinematic_attack::Timer_Expired(GameObject *obj, int number)
{
	if (number == 250)
	{
		ActionParamsStruct params;
	int priority = 100;
	float range = 1000.0f;
	float deviation = 0.0f;
	//GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("Move_ID"));
	//params.Set_Movement(GotoObject,10.0f,50.0f);
	//params.MoveFollow = true;
	//Commands->Action_Goto(obj,params);
	params.Set_Basic(this,(float)priority,40016);
	params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("ID")),range,deviation,true);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj,params);
	Commands->Start_Timer(obj,this,3.0f,250);
	}
}

void sla_created_cinematic_attack::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	Commands->Action_Reset(obj,100);
}


void sla_m01_finaldoor::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 300) {
		canbeopen = true;
	}
}


void sla_m01_finaldoor::Poked(GameObject *obj,GameObject *poker)
{
  if (pokeable != false) {
	  pokeable = false;
	  Commands->Enable_HUD_Pokable_Indicator(obj,false);
	if (canbeopen == true)  {
		int ID = Get_Int_Parameter("GateID");
	    GameObject *gate = Commands->Find_Object(ID);
		Commands->Destroy_Object(gate);
		Send_Message(15,255,0,"!~ ACCESS GARANTED ~!");
	}
	else {
		Send_Message(255,204,0,"!~ ACCESS DENIED ~!");
		Commands->Start_Timer(obj,this,3.0f,10);
	}
  }
}

void sla_m01_finaldoor::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
	   pokeable = true;
	   Commands->Enable_HUD_Pokable_Indicator(obj,true);
	}
}

void sla_m01_doorkey::Poked(GameObject *obj,GameObject *poker)
{
	if (poked != true) {
		poked = true;
		int ID = Get_Int_Parameter("SwitchID");
	    GameObject *sw = Commands->Find_Object(ID);
		Commands->Send_Custom_Event(poker,sw,300,300,1.0f);
	}
}


void sla_m01_final::Entered(GameObject *obj,GameObject *enter)
{
	// map created by sla.ro(master) (C) Sla Company
	Commands->Start_Timer(obj,this,1.0f,10);
	Commands->Start_Timer(obj,this,3.0f,20);
	Commands->Start_Timer(obj,this,5.0f,30);
	Commands->Start_Timer(obj,this,7.0f,40);
}

void sla_m01_final::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Send_Message(15,255,0,"Map created by SLA.RO(MASTER) (C) Sla Company.");
	}

	if (number == 20) {
		Send_Message(15,255,0,"Thank you for playing!");

	}

	if (number == 30) {
		Send_Message(15,255,0,"Mission Accomplished!");
	}

	if (number == 40) {
		Console_Input("win 1");
	}

}

void sla_powerup_parachute::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 1000000025) {
		Commands->Attach_Script(sender,"sla_parachute","");
	}
}

void sla_kill_sendcustom::Killed(GameObject *obj,GameObject *shooter)
{
	int param;
	if (Get_Int_Parameter("Param") == 0) {
		param = Commands->Get_ID(shooter);
	}
	else {
		param = Get_Int_Parameter("Param");	
	}
	Commands->Send_Custom_Event(obj,Commands->Find_Object(Get_Int_Parameter("ID")),Get_Int_Parameter("Message"),param,0);
	Destroy_Script();
}

void sla_kill_sendcustom::Destroyed(GameObject *obj)
{
	Commands->Send_Custom_Event(obj,Commands->Find_Object(Get_Int_Parameter("ID")),Get_Int_Parameter("Message"),Get_Int_Parameter("Param"),0);
}


void sla_poke_sendcustom::Poked(GameObject *obj,GameObject *poker)
{
	GameObject *object;
	int message;
	int ID;
	int param;
    ID = Get_Int_Parameter("ID");
	message = Get_Int_Parameter("Message");
	object = Commands->Find_Object(ID);
	param = Commands->Get_ID(poker);
	Commands->Send_Custom_Event(obj,object,message,param,0);
}



void sla_killed_dropkey::Killed(GameObject *obj, GameObject *shooter)
{
	int key = Get_Int_Parameter("KeyLevel");
	Vector3 pos = Commands->Get_Position(obj);
	GameObject *powerup = 0;
	pos.Z += 0.25f;

	if (key == 1)//green
	{
		powerup = Commands->Create_Object("Level_01_Keycard",pos);
	}
	else if (key == 2)//yellow
	{
		powerup = Commands->Create_Object("Level_02_Keycard",pos);
	}
	else if (key == 3)//red
	{
		powerup = Commands->Create_Object("Level_03_Keycard",pos);
	}
	Commands->Set_Facing(powerup,Commands->Get_Facing(obj));
}


void sla_picksound::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 1000000025)
	{
		if (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_MineRemote_02"))
		{
			Send_Message_Player(sender,104,234,40,"Remote C4 acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pac4_aqob0004i1evag_snd.wav");
		}
		else if ( (strstr(Commands->Get_Preset_Name(obj),"POW_Chaingun_Player")) || (strstr(Commands->Get_Preset_Name(obj),"POW_Chaingun_Player_Nod")) )
		{
			Send_Message_Player(sender,104,234,40,"Chaingun ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pacg_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"CnC_MineProximity_05"))
		{
			Send_Message_Player(sender,104,234,40,"Proximity C4 acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pacp_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"Level_01_Keycard"))
		{
			char pickupmsg[250];
			sprintf(pickupmsg,"%s acquired a Green Key Card.",Get_Player_Name(sender));
			Send_Message(104,234,40,pickupmsg);
			Commands->Create_2D_WAV_Sound("greenkeyacquired.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"Level_02_Keycard"))
		{
			char pickupmsg[250];
			sprintf(pickupmsg,"%s acquired a Yellow Security Card.",Get_Player_Name(sender));
			Send_Message(104,234,40,pickupmsg);
			Commands->Create_2D_WAV_Sound("yellowkeyacquired.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"Level_03_Keycard"))
		{
			char pickupmsg[250];
			sprintf(pickupmsg,"%s acquired a Red Key Card.",Get_Player_Name(sender));
			Send_Message(104,234,40,pickupmsg);
			Commands->Create_2D_WAV_Sound("redkeyacquired.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_ChemSprayer_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Chemsprayer fluid acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pacs_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_MineTimed_Player_02"))
		{
			Send_Message_Player(sender,104,234,40,"Timed C4 acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pact_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Flamethrower_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Flamethrower fuel acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00paft_aqob0001i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_GrenadeLauncher_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Grenades acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pagn_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_IonCannonBeacon_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Ion Cannon Beacon acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00paib_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_IonCannonBeacon_Player"))
		{
			Send_Message_Player(sender,104,234,40,"10 Second Superweapon Beacon acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00paib_aqob0004i1evag_snd.wav");
		}

		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Nuclear_Missle_Beacon"))
		{
			Send_Message_Player(sender,104,234,40,"10 Second Superweapon Beacon acquired.");
			Create_2D_WAV_Sound_Player(sender,"nukeavail.wav");
		}

		else if ((strstr(Commands->Get_Preset_Name(obj),"CnC_POW_IonCannonBeacon_Player")) || (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_Nuclear_Missle_Beacon")))
		{
			Send_Message_Player(sender,104,234,40,"Superweapon Beacon acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00paib_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_LaserChaingun_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Laser Chaingun battery acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00palc_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_LaserRifle_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Laser Rifle battery acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00palr_aqob0004i1evag_snd.wav");
		}
		else if ((strstr(Commands->Get_Preset_Name(obj),"POW_VoltAutoRifle_Player")) || (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_VoltAutoRifle_Player_Nod")))
		{
			Send_Message_Player(sender,104,234,40,"Volt Auto Rifle battery acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pavr_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Railgun_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Railgun ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00parg_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_RamjetRifle_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Ramjet Rifle ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00parj_aqob0004i1evag_snd.wav");
		}
		else if ((strstr(Commands->Get_Preset_Name(obj),"POW_RocketLauncher_Player")) || 
			     (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_RocketLauncher_Player")) || 
				 (strstr(Commands->Get_Preset_Name(obj),"M02_POW_Rocket_FullAmmo")) )
		{
			Send_Message_Player(sender,104,234,40,"Rockets acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00park_aqob0004i1evag_snd.wav");
		}
		else if ((strstr(Commands->Get_Preset_Name(obj),"POW_RepairGun_Player")) || (strstr(Commands->Get_Preset_Name(obj),"CnC_POW_RepairGun_Player")))
		{
			Send_Message_Player(sender,104,234,40,"Maintenance Tool Battery acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00parp_aqob0004i1evag_snd.wav");
		}
		else if ((strstr(Commands->Get_Preset_Name(obj),"POW_SniperRifle_Player")) || (strstr(Commands->Get_Preset_Name(obj),"POW_SniperRifle_Player_Nod")))
		{
			Send_Message_Player(sender,104,234,40,"Sniper Rifle ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pasr_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Shotgun_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Shotgun ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00pass_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_TiberiumFlechetteGun_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Tiberium Flechette ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00patf_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_TiberiumAutoRifle_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Tiberium Auto Rifle ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00patr_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_TiberiumAutoRifle_Player"))
		{
			Send_Message_Player(sender,104,234,40,"Tiberium Auto Rifle ammunition acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00patr_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Armor_100"))
		{
			Send_Message_Player(sender,104,234,40,"Full Body Armor acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00prba_aqob0004i1evag_snd.wav");
		}
		else if (strstr(Commands->Get_Preset_Name(obj),"POW_Armor_050"))
		{
			Send_Message_Player(sender,104,234,40,"Breast Plate Armor acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00prbp_aqob0004i1evag_snd.wav");
		}
		else if ( (strstr(Commands->Get_Preset_Name(obj),"POW_Health_025")) || (strstr(Commands->Get_Preset_Name(obj),"POW_Health_050")) || (strstr(Commands->Get_Preset_Name(obj),"POW_Health_100")) )
		{
			Send_Message_Player(sender,104,234,40,"Health Supplement acquired.");
			Create_2D_WAV_Sound_Player(sender,"m00ph25_aqob0004i1evag_snd.wav");
		}
	}
}


void sla_Building_GDI::Killed(GameObject *obj, GameObject *shooter)
{
	Commands->Start_Timer(obj,this,3.5f,500);
}

void sla_Building_GDI::Timer_Expired(GameObject *obj, int number)
{
	if (number == 500)
	{
		Commands->Create_2D_WAV_Sound("gdistructdestr.wav");
		Send_Message(15,255,0,"GDI Structure Destroyed.");
	}
}


void sla_Building_NOD::Killed(GameObject *obj, GameObject *shooter)
{
	Commands->Start_Timer(obj,this,3.5f,500);
}

void sla_Building_NOD::Timer_Expired(GameObject *obj, int number)
{
	if (number == 500)
	{
		Commands->Create_2D_WAV_Sound("nodstructdestr.wav");
		Send_Message(15,255,0,"Nod Structure Destroyed.");
	}
}



void sla_enter_sound::Entered(GameObject *obj,GameObject *enter)
{
	char msg[250];
	sprintf(msg,"snda %s",Get_Parameter("SoundName"));
	Console_Input(msg);
	Destroy_Script();
}



void sla_m03_buildcontroler::Killed(GameObject *obj,GameObject *shooter)
{
	if (destroy != true) { //check if can be destroyed or not..
	  Console_Input("amsg Mission Failed! Critical Building Destroyed (Map by sla.ro)");
	  Console_Input("snda m00_fail0001evag_snd.wav");
	  Console_Input("win 0");
	  Console_Input("gameover");
	}
	else {
		if (Get_Int_Parameter("CC") == 1) {
			Send_Message(15,255,0,"Nod Control Center destroyed.");
	        Console_Input("snda m00bncc_kill0053i1gbmg_snd.wav");
		}
		else {
			Send_Message(15,255,0,"Nod Power Plant destroyed.");
			Console_Input("snda nodstructdestr.wav");
		}
	}
}

void sla_m03_buildcontroler::Damaged(GameObject *obj,GameObject *damager,float damage)
{
	if (destroy != true) {
	    if (say == true) {
		     say = false;
		     Commands->Start_Timer(obj,this,10.0f,10);
	    	 Send_Message(15,255,0,"! ALERT ! Critical Building is under attack. Do not destroy it ! ALERT !");
	         Console_Input("snda paging_caution_2.wav");
	    }
	}
}

void sla_m03_buildcontroler::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		say = true;
	}
}

void sla_m03_buildcontroler::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 100) {
		destroy = true; // u can destroy that shit
	}
}



void sla_m03_start::Entered(GameObject *obj,GameObject *enter)
{
	if (entere != true) {
	 entere = true;
	 Commands->Start_Timer(obj,this,2.0f,10);
	 Commands->Start_Timer(obj,this,8.0f,20);
	}
}

void sla_m03_start::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Send_Message(15,255,0,"Primary Objective: Retrive informations about scientist.");
	    Console_Input("snda m03dsgn_dsgn0001i1eval_snd.wav");
	    
	}
	else if (number == 20) {
		Send_Message(15,255,0,"Secondary Objective: Secure Beach");
	    Console_Input("snda 00-n038e.wav");
		Destroy_Script();
	}
}

void sla_m03_ccterminal::Poked(GameObject *obj,GameObject *poker)
{
    if (poked != true) {
     poked=true; // block flood
     Commands->Start_Timer(obj,this,0.2f,100);
	 Commands->Start_Timer(obj,this,1.0f,200);
	 Commands->Start_Timer(obj,this,2.0f,300);
	 Commands->Start_Timer(obj,this,5.0f,400);
	 Commands->Start_Timer(obj,this,7.0f,500);
	 Commands->Start_Timer(obj,this,10.0f,550);
	 Commands->Start_Timer(obj,this,15.0f,600);
	 Commands->Start_Timer(obj,this,30.0f,25);
	}
}

void sla_m03_ccterminal::Timer_Expired(GameObject *obj, int number)
{
	if (number == 100) {
		Send_Message(15,255,0,"Transmiting..");
	    Console_Input("snda 00-n026e.wav");
	}
	else if (number == 300) {
		Send_Message(15,255,0,"Reciving..");
	    Console_Input("snda 00-n028e.wav");
	}
	else if (number == 400) {
		Send_Message(15,255,0,"Data Recived Complete");
	    Console_Input("snda 00-n030e.wav");
	}
	else if (number == 600) {
		Send_Message(15,255,0,"Proceed to Dock Area.");
	    Console_Input("snda 00-n150e.wav");
		//
	}
	else if (number == 500) {
		Send_Message(15,255,0,"Flight data download complete!");
	    Console_Input("snda m03dsgn_dsgn0013i1evag_snd.wav");
	}
	else if (number == 550) {
	    Console_Input("snda m03dsgn_dsgn0014r1eval_snd.wav");
	}
	else if (number == 700) {
		Send_Message(15,255,0,"ALERT: Volcanic activity detected !");
	    Console_Input("snda 00-n150e.wav");
	}
	else if (number == 25) {
		Send_Message(15,255,0,"ALERT: 25 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,20);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 20) {
		Send_Message(15,255,0,"ALERT: 20 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,15);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 15) {
		Send_Message(15,255,0,"ALERT: 15 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,10);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 10) {
		Send_Message(15,255,0,"ALERT: 10 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,5);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 5) {
		Send_Message(15,255,0,"ALERT: Five Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda m03dsgn_dsgn0019i1eval_snd.wav");
		Commands->Start_Timer(obj,this,180.0f,2);
	}
	else if (number == 2) {
		Send_Message(15,255,0,"ALERT: Two Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda m03dsgn_dsgn0019i1eval_snd.wav");
		Commands->Start_Timer(obj,this,60.0f,1);
	}
	else if (number == 1) {
		Send_Message(15,255,0,"ALERT: Less than one minute left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda m03dsgn_dsgn0019i1eval_snd.wav");
		Commands->Start_Timer(obj,this,60.0f,0);
	}
	else if (number == 0) {
	    Console_Input("snda m00_fail0001evag_snd.wav");
	    Console_Input("amsg Mission Failed ! Time Expired (Map by sla.ro)");
	    Console_Input("win 0");
	   // Console_Input("gameover");
	}
}

void sla_custom_teleport::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message")) {
	    int ob = Get_Int_Parameter("Object_ID");
		GameObject *gotoObject = Commands->Find_Object(ob);
		Vector3 gotoLocation = Commands->Get_Position(gotoObject);
		Commands->Set_Position(obj,gotoLocation);
   }
}

void sla_kill_message::Killed(GameObject *obj,GameObject *shooter)
{
	    Console_Input("snda com_ion_beep.wav");
	    Send_Message(15,255,0,Get_Parameter("message"));
	    Destroy_Script();
}


void sla_created_message::Created(GameObject *obj)
{
	    Commands->Create_2D_Sound("com_ion_beep.wav");
		Send_Message(255,204,0,Get_Parameter("message"));
		Destroy_Script();
}



void sla_poke_2soundplay::Poked(GameObject *obj,GameObject *poker)
{
    if (poked != true) {
     poked=true; // block flood
     Commands->Start_Timer(obj,this,0.2f,10);
	 Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),20);
	}
}

void sla_poke_2soundplay::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		char s[100];
		sprintf(s,"snda %s",Get_Parameter("Sound1"));
		Console_Input(s);
	}
	else if (number == 20) {
		char s[100];
		sprintf(s,"snda %s",Get_Parameter("Sound2"));
		Console_Input(s);
	}
}

void sla_m05_final::Poked(GameObject *obj,GameObject *poker)
{
    if (poked != true) {
     poked=true; // block flood
     Commands->Start_Timer(obj,this,0.2f,10);
	 Commands->Start_Timer(obj,this,2.0f,20);
	 Commands->Start_Timer(obj,this,4.0f,30);
	}
}

void sla_m05_final::Timer_Expired(GameObject *obj, int number) 
{
	if (number == 10) {
		Console_Input("snda m00aseg_kill0033i1gbmg_snd.wav");
		Send_Message(255,204,0,"Thank you for playing!");
	}

	if (number == 20) {
		Console_Input("snda m00_wins0005eval_snd.wav");
	}

	if (number == 30) {
		Console_Input("snda m00_wins0001evag_snd.wav");
		Console_Input("amsg Mission Accomplished! (Map by sla.ro)");
		Console_Input("gameover");
	}
}

void sla_noshoot::Created(GameObject *obj)
{
    Commands->Innate_Disable(obj);
}

void sla_m08_raveshaw::Killed(GameObject *obj,GameObject *shooter)
{
	    Console_Input("snda m00_wins0001evag_snd.wav");
		Console_Input("amsg Mission Accomplished! (Map by sla.ro)");
		Console_Input("gameover");
}

void sla_custom_cinematic_repeat::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message")) {
	   Remove_Script(obj,"JFW_Cinematic");
	   Commands->Attach_Script(obj,"JFW_Cinematic",Get_Parameter("Script_Name"));
	}
}

void sla_enter_parachute::Entered(GameObject *obj,GameObject *enter)
{
		Commands->Attach_Script(enter,"sla_parachute","");
}

void sla_m10_building_poke::Poked(GameObject *obj,GameObject *poker)
{
    if (poked != true) {
     poked=true; // block flood
	 int id;
	 GameObject *ob;
	 Commands->Enable_HUD_Pokable_Indicator(obj,false);
	 id = Get_Int_Parameter("Id");
	 ob = Commands->Find_Object(id);
	 Commands->Apply_Damage(ob,10000,"Explosive",0);
	}
}

// mutant coop

void sla_m02_poweroffswitch::Killed(GameObject *obj, GameObject *shooter)
{
    if (poked != true) {
     poked=true;
     Commands->Start_Timer(obj,this,1.0f,10);
	 Commands->Start_Timer(obj,this,3.0f,20);
	 Commands->Start_Timer(obj,this,5.0f,30);
	}
}

void sla_m02_poweroffswitch::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Send_Message(15,255,0,"Nod Power Plant Offline.");
		Console_Input("snda pplant_powerdown.wav");
	}
	if (number == 20) {
		Send_Message(15,255,0,"Obelisk Offline");
		Console_Input("snda mxxdsgn_dsgn0042i1evag_snd.wav");
		GameObject *object;
		int idx;
		idx = Get_Int_Parameter("Id");
		object = Commands->Find_Object(idx);
		//Commands->Apply_Damage(object,10000,"Explosive",0);
		Commands->Set_Building_Power(object,false);
	}
	if (number == 30) {
		Destroy_Script();
	}
}

void sla_objective_controller::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == 1) {
	  if (obj_an >= 1) {

	  }
	  else {
	   obj_an=1;
	   Commands->Start_Timer(obj,this,1.0f,100);
	  }
	}
	else if (message == 2) {
	  if (obj_an >= 2) {

	  }
	  else {
	   obj_an=2;
	   Commands->Start_Timer(obj,this,1.0f,100);
	  }
	}
	else if (message == 3) {
	  if (obj_an >= 3) {

	  }
	  else {
	   obj_an=3;
	   Commands->Start_Timer(obj,this,1.0f,100);
	  }
	}
	else if (message == 4) {
	  if (obj_an >= 4) {

	  }
	  else {
	   obj_an=4;
	   Commands->Start_Timer(obj,this,1.0f,100);
	  }
	}
	else if (message == 5) {
	  if (obj_an >= 5) {

	  }
	  else {
	   obj_an=5;
	   Commands->Start_Timer(obj,this,1.0f,100);
	  }
	}
}

void sla_objective_controller::Timer_Expired(GameObject *obj, int number) 
{
	char message[250];
	if (number == 100) {
	 if (obj_an == 1) {
		sprintf(message,"Objective: %s",Get_Parameter("Obj1_text"));
	    Send_Message(15,255,0,message);
	 }
	 else if (obj_an == 2) {
		sprintf(message,"Objective: %s",Get_Parameter("Obj2_text"));
	    Send_Message(15,255,0,message);
	 }
	 else if (obj_an == 3) {
		sprintf(message,"Objective: %s",Get_Parameter("Obj3_text"));
	    Send_Message(15,255,0,message);
	 }
	 else if (obj_an == 4) {
		sprintf(message,"Objective: %s",Get_Parameter("Obj4_text"));
	    Send_Message(15,255,0,message);
	 }
	 else if (obj_an == 5) {
		sprintf(message,"Objective: %s",Get_Parameter("Obj5_text"));
	    Send_Message(15,255,0,message);
	 }
	 Commands->Start_Timer(obj,this,300.0f,100); // repeat every 5 minutes
	}
}


void sla_coopinfomap::Created(GameObject *obj)
{
	Commands->Start_Timer(obj,this,25.0f,10);
	Commands->Start_Timer(obj,this,27.0f,20);
}

void sla_coopinfomap::Timer_Expired(GameObject *obj, int number) 
{
		char message[250];
	if (number == 10) {
		sprintf(message,"msg Scenario By %s (C) Sla Company",Get_Parameter("author"));
	    Console_Input(message);
	}
	else if (number == 20) {
		sprintf(message,"msg Scenario Name: %s",Get_Parameter("name"));
		Console_Input(message);
	 }
}

void sla_killed_sound::Killed(GameObject *obj,GameObject *shooter)
{
	 Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),10);
}

void sla_killed_sound::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		char s[100];
		sprintf(s,"snda %s",Get_Parameter("Sound1"));
		Console_Input(s);
	}
}

void sla_m03_beachsecure::Created(GameObject *obj)
{ 
	// zunnie
        bots = 0;
}

void sla_m03_beachsecure::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
        if (message == 1)
        {
                        bots++;
        }
        else if (message == 2)
        {
               bots--;
               if (bots == 0)
               {
	               Send_Message(15,255,0,"All bots cleared. Objective Completed!");  
				   	GameObject *object;
	                object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy"));
	                Commands->Destroy_Object(object);
	                Destroy_Script();
               }
        }
}

void sla_m03_bot::Created(GameObject *obj)
{ 
	Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("ID")), 1, 0, 0);
}

void sla_m03_bot::Destroyed(GameObject *obj)
{
	Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("ID")), 2, 0, 0);
	Destroy_Script();
}


void sla_enter_send_custom_repeatdelay::Entered(GameObject *obj,GameObject *enter)
{
	if (entered != true) {
		entered=true;
		repeats=0;
		Commands->Start_Timer(obj,this,1.0,10);
	}
}


void sla_enter_send_custom_repeatdelay::Timer_Expired(GameObject *obj, int number)
{
	if (!repeats) repeats=0;
	Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("ID")), Get_Int_Parameter("Message"), 0, 0);
	repeats++;

	if (repeats == Get_Int_Parameter("Repeats")) 
	{
		 Destroy_Script();
	}
	else 
	{
		 Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),10);
	}
}


void sla_Random_Tele_Enter::Entered(GameObject *obj, GameObject *enter)
{
	if (Commands->Is_A_Star(enter))
	{
		int random = Commands->Get_Random_Int(1,3);
		if (random == 1)
		{
			Commands->Set_Position(enter, Commands->Get_Position(Commands->Find_Object(Get_Int_Parameter("CP1_ID1"))));
		}
		else if (random == 2)
		{
			Commands->Set_Position(enter, Commands->Get_Position(Commands->Find_Object(Get_Int_Parameter("CP1_ID2"))));
		}
		else if (random == 3)
		{
			Commands->Set_Position(enter, Commands->Get_Position(Commands->Find_Object(Get_Int_Parameter("CP1_ID3"))));
		}
	}
}

void sla_m05_secure::Created(GameObject *obj)
{ 
        bots = 0;
}

void sla_m05_secure::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
        if (message == 1)
        {
           bots++;
        }
        else if (message == 2)
        {
               bots--;
               if (bots == 0)
               {
				   char message[150];
	               Send_Message(15,255,0,"Objective Completed");  
				   sprintf(message,"snda %s",Get_Parameter("Sound"));
				   Console_Input(message);
				   Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("ObjectDestroy")));
				   Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("ObjectDestroy2")));
				   Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("ObjectDestroy3")));
				   Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("ObjectDestroy4")));
	               Destroy_Script();
               }
        }
}

void sla_m05_securelast::Created(GameObject *obj)
{ 
        bots = 0;
}

void sla_m05_securelast::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
        if (message == 1)
        {
                        bots++;
        }
        else if (message == 2)
        {
               bots--;
               if (bots == 0)
               {
				   Console_Input("amsg Mission Accomplished ! (Map by sla.ro)");
				   Console_Input("win 1");
				   Console_Input("gameover");
	               Destroy_Script();
               }
        }
}


void sla_enter_enable_spawn10::Entered(GameObject *obj,GameObject *enter)
{
      Commands->Enable_Spawner(Get_Int_Parameter("ID1"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID2"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID3"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID4"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID5"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID6"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID7"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID8"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID9"),true);
	  Commands->Enable_Spawner(Get_Int_Parameter("ID10"),true);
}

void sla_m04_sabotage_controler::Created(GameObject *obj)
{ 
        bots = 4;
}

void sla_m04_sabotage_controler::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
        if (message == 2)
        {
               bots--;
               if (bots == 0)
               {
				   Commands->Start_Timer(obj,this,1.0f,100);
				   Commands->Start_Timer(obj,this,4.0f,200);
				   	GameObject *object;
	                object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy"));
	                Commands->Destroy_Object(object);
					object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy2"));
	                Commands->Destroy_Object(object);
					object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy3"));
	                Commands->Destroy_Object(object);
               }
			   else {
				   char message[250];
				   sprintf(message,"%d Missile racks left to sabotage.",bots);
				   Send_Message(15,255,0,message);
			   }
        }
}

void sla_m04_sabotage_controler::Timer_Expired(GameObject *obj, int number)
{
	if (number == 100) {
	    Send_Message(15,255,0,"Objective Completed");
        Console_Input("snda primary_update.wav");
	}

	if (number == 200) {
	    Send_Message(15,255,0,"New Objective: Locate & Destroy Large Sam Site.");
        Console_Input("snda paging_caution_2.wav");
		Destroy_Script();
	}

}


void sla_m04_sabotage_obj::Poked(GameObject *obj,GameObject *poker)
{
	int ID;
	GameObject *object;
    ID = Get_Int_Parameter("ID");
	object = Commands->Find_Object(ID);
	Commands->Send_Custom_Event(obj,object,2,0,0);
	Destroy_Script();
}


void sla_m04_enginecontroller::Created(GameObject *obj)
{
	bots = 4;
}

void sla_m04_enginecontroller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 2) {
	 bots--;
       if (bots == 0)
       {
		   Commands->Start_Timer(obj,this,1.0f,100);
		   Commands->Start_Timer(obj,this,4.0f,200);
		   	GameObject *object;
            object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy"));
            Commands->Destroy_Object(object);
			object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy2"));
            Commands->Destroy_Object(object);
			object = Commands->Find_Object(Get_Int_Parameter("ObjectDestroy3"));
            Commands->Destroy_Object(object);
       }
	   else {
		   char message[250];
		   sprintf(message,"%d engine terminals left to sabotage.",bots);
		   Send_Message(15,255,0,message);
	   }
	}
}


void sla_m04_enginecontroller::Timer_Expired(GameObject *obj, int number)
{
	if (number == 100) {
	    Send_Message(15,255,0,"Objective Completed");
        Console_Input("snda primary_update.wav");
	}

	if (number == 200) {
	    Send_Message(15,255,0,"New Objective: Locate & Save Prisoniers.");
        Console_Input("snda paging_caution_2.wav");
		Destroy_Script();
	}

}


void sla_m04_engine_obj::Killed(GameObject *obj,GameObject *shooter)
{
	int ID;
	GameObject *object;
    ID = Get_Int_Parameter("ID");
	object = Commands->Find_Object(ID);
	Commands->Send_Custom_Event(obj,object,2,0,0);
	Destroy_Script();
}


void sla_m04_ccterminal::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 100) {
     Commands->Start_Timer(obj,this,0.2f,100);
	 Commands->Start_Timer(obj,this,2.0f,200);
	 Commands->Start_Timer(obj,this,4.0f,300);
	 Commands->Start_Timer(obj,this,6.0f,400);
	 Commands->Start_Timer(obj,this,60.0f,15);
	}
}

void sla_m04_ccterminal::Timer_Expired(GameObject *obj, int number)
{
	if (number == 100) {
        int ID = Get_Int_Parameter("ID");
	    int message = Get_Int_Parameter("Message");
	    GameObject *object = Commands->Find_Object(ID);
	    Commands->Send_Custom_Event(obj,object,message,0,0);
		Send_Message(15,255,0,"Auto-Destruction Sequence Initiated..");
	    Console_Input("snda 00-n026e.wav");
	}
	else if (number == 300) {
		Send_Message(15,255,0,"You have 15 Minutes to escape from War Ship..");
	    Console_Input("snda 00-n028e.wav");
	}
	else if (number == 400) {
		Send_Message(15,255,0,"New Objective: Return to Submarine");
	    Console_Input("snda 00-n030e.wav");
	}
	else if (number == 15) {
		Send_Message(15,255,0,"ALERT: 15 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,10);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 10) {
		Send_Message(15,255,0,"ALERT: 10 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,5);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 5) {
		Send_Message(15,255,0,"ALERT: Five Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Commands->Start_Timer(obj,this,180.0f,2);
	}
	else if (number == 2) {
		Send_Message(15,255,0,"ALERT: Two Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Commands->Start_Timer(obj,this,60.0f,1);
	}
	else if (number == 1) {
		Send_Message(15,255,0,"ALERT: Less than one minute left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Commands->Start_Timer(obj,this,60.0f,0);
	}
	else if (number == 0) {
	    Console_Input("snda m00_fail0001evag_snd.wav");
	    Console_Input("amsg Mission Failed ! Time Expired..");
	    Console_Input("gameover");
	}
}

void sla_kill_message_delay::Killed(GameObject *obj,GameObject *shooter)
{
	    Commands->Start_Timer(obj,this,Get_Float_Parameter("Delay"),100);
}

void sla_kill_message_delay::Timer_Expired(GameObject *obj, int number)
{
	    Console_Input("snda com_ion_beep.wav");
	    Send_Message(15,255,0,Get_Parameter("message"));
	    Destroy_Script();
}

void sla_m04_submarine::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	 if (message == 100)
	 {
		 open=true;
	 }
}

void sla_m04_submarine::Entered(GameObject *obj,GameObject *enter)
{
	if (open == true) {
	    Console_Input("amsg Mission Accomplished..");
	    Console_Input("win 1");
	    Console_Input("gameover");
	}
}

void sla_m01_samsite_controller::Created(GameObject *obj)
{
	count = Get_Int_Parameter("count");
}


void sla_m01_samsite_controller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 2) {
	   count--;
	    if (count == 0) {
	   	   Send_Message(15,255,0,"Objective Completed");
	    }
	    else {
	   	   char message[250];
	   	   sprintf(message,"%d Sam sites left to destroy.",count);
	   	   Send_Message(15,255,0,message);
	    }
	}
}


void sla_m13_controller::Created(GameObject *obj)
{
     Commands->Start_Timer(obj,this,5.0f,100);
	 Commands->Start_Timer(obj,this,2.0f,200);
	 Commands->Start_Timer(obj,this,60.0f,20);
}

void sla_m13_controller::Timer_Expired(GameObject *obj, int number)
{
	if (number == 100) {
		Send_Message(15,255,0,"Player VS Player Map (Just for fun!)");
	}
	else if (number == 20) {
		Send_Message(15,255,0,"ALERT: 20 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,15);
	    Console_Input("snda paging_caution_2.wav");
	}
	else if (number == 200) {
		Send_Message(15,255,0,"Destroy the enemy Guard Tower");
	}
	else if (number == 15) {
		Send_Message(15,255,0,"ALERT: 15 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,10);
	    Console_Input("snda paging_caution_2.wav");
		Send_Message(15,255,0,"Destroy the enemy Guard Tower");
	}
	else if (number == 10) {
		Send_Message(15,255,0,"ALERT: 10 Minutes Left");
		Commands->Start_Timer(obj,this,300.0f,5);
	    Console_Input("snda paging_caution_2.wav");
		Send_Message(15,255,0,"Destroy the enemy Guard Tower");
	}
	else if (number == 5) {
		Send_Message(15,255,0,"ALERT: Five Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Send_Message(15,255,0,"Destroy the enemy guard tower.");
		Commands->Start_Timer(obj,this,180.0f,2);
	}
	else if (number == 2) {
		Send_Message(15,255,0,"ALERT: Two Minutes Left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Send_Message(15,255,0,"Destroy the enemy guard tower");
		Commands->Start_Timer(obj,this,60.0f,1);
	}
	else if (number == 1) {
		Send_Message(15,255,0,"ALERT: Less than one minute left");
	    Console_Input("snda paging_caution_2.wav");
	    Console_Input("snda paging_caution_2.wav");
		Send_Message(15,255,0,"Destroy the enemy guard tower");
		Commands->Start_Timer(obj,this,60.0f,0);
	}
	else if (number == 0) {
	    Console_Input("gameover");
	}
}

void sla_m00_poweroff::Killed(GameObject *obj, GameObject *shooter)
{
     Commands->Start_Timer(obj,this,0.1f,10);
	 Commands->Start_Timer(obj,this,1.0f,20);
}

void sla_m00_poweroff::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		Send_Message(15,255,0,"GDI Power Plant Destroyed.");
		Console_Input("snda pplant_powerdown.wav");
	}
	if (number == 20) {
		Send_Message(15,255,0,"GDI Advanced Guard Tower - Offline");
		GameObject *object;
		int idx;
		idx = Get_Int_Parameter("Id");
		object = Commands->Find_Object(idx);
		//Commands->Apply_Damage(object,10000,"Explosive",0);
		Commands->Set_Building_Power(object,false);
		Destroy_Script();
	}
}

void sla_enter_kill_team::Entered(GameObject *obj,GameObject *enter)
{
	int team;
	team = Get_Int_Parameter("Player_Type");
	if (CheckPlayerType(enter,team))
	{
		return;
	}
	else {
		char message[350];
		sprintf(message,"ppage %d You were killed because base sniping is not allowed. Read Map Rules!",Commands->Get_ID(enter));
		Console_Input(message);
		Commands->Destroy_Object(enter);
	}
}

void sla_m08_lasercontroller::Created(GameObject *obj)
{
	count = Get_Int_Parameter("count");
}

void sla_m08_lasercontroller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 2) {
		// killed
		count--;
		 if (count == 0) {
			 Send_Message(15,255,0,"Objective Completed");
			 Commands->Destroy_Object(Commands->Find_Object(Get_Int_Parameter("ID")));
		 }
		 else {
			 char m[250];
			 sprintf(m,"%d objects left to destroy.",count);
			 Send_Message(15,255,0,m);
		 }
	}
}

void sla_m08_terminallaser::Killed(GameObject *obj, GameObject *shooter)
{
	int ID;
	GameObject *object;
    ID = Get_Int_Parameter("ID");
	object = Commands->Find_Object(ID);
	Commands->Send_Custom_Event(obj,object,2,0,0);
	Destroy_Script();
}

void sla_m10_doors::Poked(GameObject *obj,GameObject *poker)
{
	int ID;
	GameObject *object;
    ID = Get_Int_Parameter("ID");
	object = Commands->Find_Object(ID);
//	Commands->Set_Animation(object,"basegate1",false,"basegate1",0.0f,-1f,false);
}

void sla_M11_boss::Created(GameObject *obj)
{
	Set_Max_Health(obj, 900);
    Commands->Start_Timer(obj,this,1.0f,100);
	Set_Object_Type(obj, 0);
}

void sla_M11_boss::Timer_Expired(GameObject *obj, int number)
{
  if (number == 100) {
   // regen health
	 if ((Commands->Get_Health(obj)+5) < Commands->Get_Max_Health(obj)) {
	  Commands->Set_Health(obj,Commands->Get_Health(obj)+5);
	 }
	 Commands->Start_Timer(obj,this,1.0f,100);
  }
}

void sla_M11_boss::Killed(GameObject *obj,GameObject *shooter)
{
	    Console_Input("snda m11dsgn_dsgn0076i1gbmg_snd.wav");
		Console_Input("amsg Mission Accomplished! (Map by sla.ro)");
		Console_Input("gameover");
}


// animal states
// 0 = idle
// 1 = moving
// 2 = attacking
// 3 = dying


void sla_animal_logic::Created(GameObject *obj)
{
	MY_STATE=0;
	Animal_Behivour=true;
	MY_TARGET=0;
	MY_TEAM=Get_Int_Parameter("Team");
	Change_Team(obj, MY_TEAM); // I'm in this team now
	Commands->Start_Timer(obj,this,0.5f,430); // loop
}

void sla_animal_logic::Killed(GameObject *obj,GameObject *shooter)
{
   MY_STATE=3;
   Animal_Behivour=false;
}



void sla_animal_logic::Timer_Expired(GameObject *obj, int number)
{
	if (number == 430) { // infinte loop till dies.
		if (Animal_Behivour == false) { return; } // check if our animal is still in normal state.
		Commands->Start_Timer(obj,this,0.5f,430); // loop
	   Vector3 mypos=Commands->Get_Position(obj);

		if (MY_TARGET == 0) { // if we don't have any target, we are goanna set one!
		 float closest_enemy=90; // we limit to 50, if he's further, he won't be seen by our bot. (thats fair XD)
		 for(int a = 0; a < 256; a++) {
		 	if (Get_GameObj(a) && Commands->Get_Distance(mypos, Commands->Get_Position(Get_GameObj(a))) < closest_enemy && Get_Team(a) != MY_TEAM ) {
		 	  // goto target here
		 	  MY_TARGET=Commands->Get_ID(Get_GameObj(a));
			  //Console_Output("New n00b found\n");
			  MY_STATE=0;
			  closest_enemy=Commands->Get_Distance(mypos, Commands->Get_Position(Get_GameObj(a))); // animals only will attack closest players.
		 	}
		 }
		}
		else { // does target is alive?
			if (!Commands->Find_Object(MY_TARGET) || Commands->Get_Distance(mypos, Commands->Get_Position(Commands->Find_Object(MY_TARGET))) >= 20) {
			  MY_TARGET=0; // he died proabably or too far
			  //Console_Output("Target lost..\n");
			  return;
			}

			if (Commands->Get_Distance(mypos, Commands->Get_Position(Commands->Find_Object(MY_TARGET))) <= 1.5f) {
			 // Console_Output("Attack!\n");
			   Commands->Create_3D_Sound_At_Bone(Get_Parameter("Attack_Sound"), obj, "c CHEST"); // attack sound
			   Commands->Apply_Damage(Commands->Find_Object(MY_TARGET), Get_Float_Parameter("Damage"), Get_Parameter("Warhead"), obj); // damage sound
			}
			else {
	         ActionParamsStruct params;
			 params.Set_Movement(Commands->Find_Object(MY_TARGET),1.0f,0.25f);
	         params.MoveFollow = true;
			  //params.MoveArrivedDistance = 1;
			 params.Set_Basic(this,100,777);
			 params.AttackActive = false;
			 params.AttackCheckBlocked = false;
			 //Console_Output("Moving!\n");
	         Commands->Action_Goto(obj,params);
		    }

		
		}

		
	}
}

void sla_animal_logic::Destroyed(GameObject *obj)
{
	   Commands->Create_3D_Sound_At_Bone(Get_Parameter("Die_Sound"), obj, "c CHEST");
}


void sla_soundmessage_custom::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == Get_Int_Parameter("custom")) {
		char sendsound[100];
		sprintf(sendsound, "snda %s", Get_Parameter("soundname"));
		Console_Input(sendsound);
	    Send_Message(15,255,0,Get_Parameter("message"));
		Destroy_Script();
	}
}


void sla_blamo::Created(GameObject *o)
{
	Set_Skin(o, "Blamo");
}



void sla_ai_vehicle_controller::Created(GameObject *obj)
{
	sla_ai_vehicle_tanks[0]="CnC_Nod_Flame_Tank";
	sla_ai_vehicle_tanks[1]="CnC_Nod_Light_Tank";
	sla_ai_vehicle_tanks[2]="CnC_Nod_Mobile_Artillery";
	sla_ai_vehicle_tanks[3]="CnC_Nod_APC";
}

void sla_ai_vehicle_controller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 999) {
	   Commands->Start_Timer(obj, this, 0.1f, 1000);
	}
	else if (message == 550) {
		delay=true;
	}
	else if (message == 660) {
	    Destroy_Script();
	}
}

void sla_ai_vehicle_controller::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1000) {
		 int index = 0;
	     for (SLNode<BaseGameObj>* node = GameObjManager::GameObjList.Head(); node; node = node->Next())
	     {
	     	GameObject *o = (GameObject *)node->Data();
	     	if (o->As_VehicleGameObj() && Get_Object_Type(o) == Get_Int_Parameter("team")) {
	     	  index++;
	     	}
	     }

		if ((int) Get_Vehicle_Limit() >= index) {
		  Create_Vehicle(sla_ai_vehicle_controller::sla_ai_vehicle_tanks[rand() % 4], 1.0f, 0, Get_Int_Parameter("team"));
		}

		if (delay == true) 	Commands->Start_Timer(obj, this, 30.f, 1000);
		else Commands->Start_Timer(obj, this, 15.f, 1000);
	}
}

void sla_ai_vehicle_script::Entered(GameObject *obj,GameObject *enter)
{
	if (Get_Int_Parameter("team") == Get_Object_Type(enter)) {
		Commands->Attach_Script(enter, "M03_Goto_Star", "");
		Commands->Enable_Vehicle_Transitions(enter, false);
	}
}

void sla_facility_mct_controll::Create(GameObject *obj)
{
	ind=4;

}

void sla_facility_mct_controll::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 1990) {
		ind--;
		 if (ind <= 0) {
			 Console_Input("amsg Mission Accomplished!\nWe've found informations about Rainbow Factory!\nMap by sla.ro");
			 Console_Input("win 1");
			 Destroy_Script();
			 //Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("id")), Get_Int_Parameter("message"), 1, 1.f);
		 }
		 else {
			 char msg[100];
			 if (ind < 1)
			 {
				sprintf(msg, "%d MCT left to destroy.", ind);
			 }
			 else
			 {
				 sprintf(msg, "%d MCTs left to destroy.", ind);
			 }
			 Send_Message(255, 255, 255, msg);
		 }
	}
}

void sla_poke_teleport::Created(GameObject *obj)
{
	tel=false;
	Commands->Enable_HUD_Pokable_Indicator(obj, true);
}

void sla_poke_teleport::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (Get_Int_Parameter("message") == message) {
		tel=true;
		Send_Message(255,255,255, "Teleport is now available in front of GDI Base.");
		Commands->Enable_HUD_Pokable_Indicator(obj, true);
	}
}

void sla_poke_teleport::Poked(GameObject *obj,GameObject *p)
{
	if (tel == true) {
		char c[3];
		sprintf(c, "t%d", rand() % 3);
		Commands->Set_Position(p, Commands->Get_Position(Commands->Find_Object(Get_Int_Parameter(c))));
	}
	else {
		Send_Message_Player(p, 255,255,255, "Teleport is not available yet. You need to access the Facility.");
	}
}


void sla_credit_tickle::Created(GameObject *obj)
{
	Commands->Start_Timer(obj, this, 1.f, 500);
}

void sla_credit_tickle::Timer_Expired(GameObject *obj, int number)
{
	if (number == 500) {
	 int totalofplayersfound=0;
	 int cred=Get_Int_Parameter("credits");
	 for(int i = 0;; i++)
	 {
	     if(Get_Player_Name_By_ID(i))
	     {
	 	  Set_Money(i, Get_Money(i)+cred);
	 	  totalofplayersfound++;
	     }
	 	if (totalofplayersfound >= Get_Player_Count()) { break; } // this little thing will save the day :)
	 }
	 Commands->Start_Timer(obj, this, 1.f, 500);
	}
}

void sla_destroy_building_on_start::Created(GameObject *obj)
{
	Commands->Start_Timer(obj, this, 0.1f, 100);
}

void sla_destroy_building_on_start::Timer_Expired(GameObject *obj, int number)
{
	Commands->Apply_Damage(obj, 10000, "Explosive", 0);
	Destroy_Script();
}

void sla_enter_cinematic5::Created(GameObject *obj)
{
	repeats=0;
	o[0]=Commands->Find_Object(Get_Int_Parameter("id1"));
	o[1]=Commands->Find_Object(Get_Int_Parameter("id2"));
	o[2]=Commands->Find_Object(Get_Int_Parameter("id3"));
	o[3]=Commands->Find_Object(Get_Int_Parameter("id4"));
	o[4]=Commands->Find_Object(Get_Int_Parameter("id5"));
}

void sla_enter_cinematic5::Entered(GameObject *obj,GameObject *enter)
{
	for (int a=0; a < 5; a++) {
	  if (o[a]) {
	   Commands->Attach_Script(Commands->Create_Object("Invisible_Object",Commands->Get_Position(o[a])),"JFW_Cinematic", Get_Parameter("Script_Name"));
	  }
	 }

	Destroy_Script();
}


// ------------------------------------------------
// sla_new_teleport
// ------------------------------------------------

void sla_new_teleport::Entered(GameObject *obj,GameObject *o)
{
	if (Get_Int_Parameter("type") == 0) { return; } // 1 = Entered
	teleport(o);
}

void sla_new_teleport::Poked(GameObject *obj, GameObject *o)
{
	if (Get_Int_Parameter("type") == 1) { return; } // 0 = Poked
	teleport(o);
}

void sla_new_teleport::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 900) {
		if (totals == 64) { return; }
		teleports[totals]=Commands->Get_ID(sender);
		totals++;
	}
}

void sla_new_teleport::teleport(GameObject *o)
{
	Commands->Set_Position(o, Commands->Get_Position(Commands->Find_Object(teleports[rand() % totals])));
}


/////////////////////////////////////////////////////////////////////////////////////
// Backward compatibility between 4.0 and 4.1 (yeah, contains some zunnie's scripts)
/////////////////////////////////////////////////////////////////////////////////////

void z_Set_Team::Created(GameObject *obj)
{
	Commands->Set_Player_Type(obj, Get_Int_Parameter("team"));
}

void z_Cinematic_Attack::Created(GameObject *obj)
{
	ActionParamsStruct params;
	int priority = Get_Int_Parameter("Priority");
	float range = Get_Float_Parameter("Range");
	float deviation = Get_Float_Parameter("Deviation");
	int primary = Get_Int_Parameter("Primary");
	GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("ID"));
	params.Set_Movement(GotoObject, 10.0f, 50.0f);
	params.MoveFollow = true;
	Commands->Action_Goto(obj, params);
	params.Set_Basic(this, (float)priority, 40016);
	params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("ID")), range, deviation, primary);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj, params);
}

void z_Cinematic_Attack::Action_Complete(GameObject *obj, int action, ActionCompleteReason reason)
{
	Commands->Action_Reset(obj, 100);
}


void z_Cinematic_Attack2::Created(GameObject *obj)
{
	ActionParamsStruct params;
	int priority = 100;
	float range = 1000.0f;
	float deviation = 0.0f;
	int primary = 1;
	GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("ID"));
	params.Set_Movement(GotoObject, 10.0f, 50.0f);
	params.MoveFollow = true;
	Commands->Action_Goto(obj, params);
	params.Set_Basic(this, (float)priority, 40016);
	params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("ID")), range, deviation, primary);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj, params);
	Commands->Start_Timer(obj, this, 3.0f, 250);
}

void z_Cinematic_Attack2::Timer_Expired(GameObject *obj, int number)
{
	if (number == 250)
	{
		ActionParamsStruct params;
		int priority = 100;
		float range = 1000.0f;
		float deviation = 0.0f;
		int primary = 1;
		GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("ID"));
		params.Set_Movement(GotoObject, 10.0f, 50.0f);
		params.MoveFollow = true;
		Commands->Action_Goto(obj, params);
		params.Set_Basic(this, (float)priority, 40016);
		params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("ID")), range, deviation, primary);
		params.AttackCheckBlocked = false;
		Commands->Action_Attack(obj, params);
		Commands->Start_Timer(obj, this, 3.0f, 250);
	}
}

void z_Cinematic_Attack2::Action_Complete(GameObject *obj, int action, ActionCompleteReason reason)
{
	Commands->Action_Reset(obj, 100);
}




void z_Cinematic_Attack3::Created(GameObject *obj)
{
	ActionParamsStruct params;
	int priority = 100;
	float range = 1000.0f;
	float deviation = 0.0f;
	int primary = 1;
	GameObject *GotoObject = Commands->Find_Object(Get_Int_Parameter("Move_ID"));
	params.Set_Movement(GotoObject, 10.0f, 50.0f);
	params.MoveFollow = true;
	Commands->Action_Goto(obj, params);
	params.Set_Basic(this, (float)priority, 40016);
	params.Set_Attack(Commands->Find_Object(Get_Int_Parameter("Attack_ID")), range, deviation, primary);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj, params);
	Commands->Start_Timer(obj, this, 3.0f, 250);
}

void z_Cinematic_Attack3::Action_Complete(GameObject *obj, int action, ActionCompleteReason reason)
{
	Commands->Action_Reset(obj, 100);
}


/////////////////////////
// REGISTRED SCRIPTS \\
/////////////////////////

// Facility Map by sla.ro
ScriptRegistrant<sla_facility_mct_controll> sla_facility_mct_controll_Registrant("sla_facility_mct_controll","id=0:int,message=0:int");
ScriptRegistrant<sla_poke_teleport> sla_poke_teleport_Registrant("sla_poke_teleport","message=0:int,t0=0:int,t1=0:int,t2=0:int");
ScriptRegistrant<sla_credit_tickle> sla_credit_tickle_Registrant("sla_credit_tickle","credits=25:int");
//ScriptRegistrant<sla_destroy_building_on_start> sla_destroy_building_on_start_Registrant("sla_destroy_building_on_start","");
ScriptRegistrant<sla_enter_cinematic5> sla_enter_cinematic5_Registrant("sla_enter_cinematic5","id1=0:int,id2=0:int,id3=0:int,id4=0:int,id5=0:int,Script_Name=cine.txt:string");


// Other
ScriptRegistrant<sla_new_teleport> sla_new_teleport_Registrant("sla_new_teleport","type=0:int");
ScriptRegistrant<sla_Random_Tele_Enter> sla_Random_Tele_Enter_Registrant("sla_Random_Tele_Enter","CP1_ID1=1:int,CP1_ID2=1:int,CP1_ID3=1:int");
ScriptRegistrant<sla_enable_poke> sla_enable_poke_Registrant("sla_enable_poke","");
ScriptRegistrant<sla_parachute> sla_parachute_Registrant("sla_parachute","");
ScriptRegistrant<sla_created_cinematic_attack> sla_created_cinematic_attack_Registrant("sla_created_cinematic_attack","ID=0:int");
ScriptRegistrant<sla_powerup_parachute> sla_powerup_parachute_Registrant("sla_powerup_parachute","");
ScriptRegistrant<sla_created_message> sla_created_message_Registrant("sla_created_message","message=hello:string");
ScriptRegistrant<sla_noshoot> sla_noshoot_Registrant("sla_noshoot","");
ScriptRegistrant<sla_animal_logic> sla_animal_logic_Registrant("sla_animal_logic","Die_Sound=dead:string,Attack_Sound=attack:string,Team=0:int,Damage=200:int,Warhead=idk:string");
ScriptRegistrant<sla_blamo> sla_blamo_Registrant("sla_blamo","");
ScriptRegistrant<sla_ai_vehicle_controller> sla_ai_vehicle_controller_Registrant("sla_ai_vehicle_controller","team=0:int");
ScriptRegistrant<sla_ai_vehicle_script> sla_ai_vehicle_script_Registrant("sla_ai_vehicle_script","team=0:int");

// Backward compatibility
ScriptRegistrant<z_Cinematic_Attack> z_Cinematic_Attack_Registrant("z_Cinematic_Attack", "ID=1:int,Priority=100:int,Range=100:float,Deviation=0:float,Primary=1:int");
ScriptRegistrant<z_Set_Team> z_Set_Team_Registrant("z_Set_Team", "team:int");
ScriptRegistrant<z_Cinematic_Attack3> z_Cinematic_Attack3_Registrant("z_Cinematic_Attack3", "Move_ID=1:int,Attack_ID=1:int");
ScriptRegistrant<z_Cinematic_Attack2> z_Cinematic_Attack2_Registrant("z_Cinematic_Attack2", "ID=1:int");

//poke
ScriptRegistrant<sla_teleport> sla_teleport_Registrant("sla_teleport","tel1=0:int,tel2=0:int,tel3=0:int,tel4=0:int");
ScriptRegistrant<sla_poke_msg> sla_poke_msg_Registrant("sla_poke_msg","message=hello world!:string");
ScriptRegistrant<sla_poke_distroyobject> sla_poke_distroyobject_Registrant("sla_poke_distroyobject","ID=0:int");
ScriptRegistrant<sla_poke_sendcustom> sla_poke_sendcustom_Registrant("sla_poke_sendcustom","ID=0:int,Message=0:int");
ScriptRegistrant<sla_poke_2soundplay> sla_poke_2soundplay_Registrant("sla_poke_2soundplay","Delay=0:float,Sound1=hehe:string,Sound2=hehe:string");
ScriptRegistrant<sla_m05_final> sla_m05_final_Registrant("sla_m05_final","");

//kill
ScriptRegistrant<sla_kill_distroyobject> sla_kill_distroyobject_Registrant("sla_kill_distroyobject","ID=0:int");
ScriptRegistrant<sla_killed_dropkey> sla_killed_dropkey_Registrant("sla_killed_dropkey","KeyLevel=1:int");
ScriptRegistrant<sla_kill_sendcustom> sla_kill_sendcustom_Registrant("sla_kill_sendcustom","ID=0:int,Message=0:int,Param=0:int");
ScriptRegistrant<sla_Building_NOD> sla_Building_NOD_Registrant("sla_Building_NOD","");
ScriptRegistrant<sla_Building_GDI> sla_Building_GDI_Registrant("sla_Building_GDI","");
ScriptRegistrant<sla_kill_message> sla_kill_message_Registrant("sla_kill_message","message=Hello:string");
ScriptRegistrant<sla_killed_sound> sla_killed_sound_Registrant("sla_killed_sound","Delay=0:float,Sound1=hehe:string");
ScriptRegistrant<sla_kill_message_delay> sla_kill_message_delay_Registrant("sla_kill_message_delay","message=Hello:string,Delay=0:float");

//enter
ScriptRegistrant<sla_enter_changeteam> sla_enter_changeteam_Registrant("sla_enter_changeteam","Team=0:int");
ScriptRegistrant<sla_enter_enable_spawn> sla_enter_enable_spawn_Registrant("sla_enter_enable_spawn","ID=0:int,Enable=0:int");
ScriptRegistrant<sla_enter_send_custom> sla_enter_send_custom_Registrant("sla_enter_send_custom","ID=0:int,Message=0:int");
ScriptRegistrant<sla_on_enter_message> sla_on_enter_message_Registrant("sla_on_enter_message","message=hello world!:string");
ScriptRegistrant<sla_enter_sound> sla_enter_sound_Registrant("sla_enter_sound","SoundName=sound:string");
ScriptRegistrant<sla_enter_parachute> sla_enter_parachute_Registrant("sla_enter_parachute","");
ScriptRegistrant<sla_enter_send_custom_repeatdelay> sla_enter_send_custom_repeatdelay_Registrant("sla_enter_send_custom_repeatdelay","ID=0:int,Message=0:int,Delay=0:float,Repeats=1:int");
ScriptRegistrant<sla_enter_enable_spawn10> sla_enter_enable_spawn10_Registrant("sla_enter_enable_spawn10","ID1=0:int,ID2=0:int,ID3=0:int,ID4=0:int,ID5=0:int,ID6=0:int,ID7=0:int,ID8=0:int,ID9=0:int,ID10=0:int");
ScriptRegistrant<sla_enter_kill_team> sla_enter_kill_team_Registrant("sla_enter_kill_team","Player_Type=0:int");

//custom
ScriptRegistrant<sla_custom_cinematic> sla_custom_cinematic_Registrant("sla_custom_cinematic","Script_Name=something.txt:string,Message=0:int");
ScriptRegistrant<sla_custom_playcinematic> sla_custom_playcinematic_Registrant("sla_custom_playcinematic","Script_Name:string");
ScriptRegistrant<sla_custom_waypathfollow> sla_custom_waypathfollow_Registrant("sla_custom_waypathfollow","Waypathid=0:int,Speed=0:int,msg=100:int");
ScriptRegistrant<sla_picksound> sla_picksound_Registrant("sla_picksound","");
ScriptRegistrant<sla_custom_teleport> sla_custom_teleport_Registrant("sla_custom_teleport","Message=0:int,Object_ID=0:int");
ScriptRegistrant<sla_custom_cinematic_repeat> sla_custom_cinematic_repeat_Registrant("sla_custom_cinematic_repeat","Script_Name=something.txt:string,Message=0:int");
ScriptRegistrant<sla_soundmessage_custom> sla_soundmessage_custom_Registrant("sla_soundmessage_custom","custom=1432:int,soundname=bla.wav:string,message=bla:string");

// --------- COOP & STDM ONLY -------- \\

// General Coop
ScriptRegistrant<sla_objective_controller> sla_objective_controller_Registrant("sla_objective_controller","Obj1_text=text:string,Obj2_text=text:string,Obj3_text=text:string,Obj4_text=text:string,Obj5_text=text:string");
ScriptRegistrant<sla_coopinfomap> sla_coopinfomap_Registrant("sla_coopinfomap","name=text:string,author=text:string");

// M00 tutorial
ScriptRegistrant<sla_tut_phase1> sla_tut_phase1_Registrant("sla_tut_phase1","ID1=0:int,ID2=0:int,ID3=0:int,ID4=0:int,ID5=0:int,ID6=0:int,ID7=0:int,ID8=0:int,ID9=0:int,ID10=0:int");
ScriptRegistrant<sla_tut_final> sla_tut_final_Registrant("sla_tut_final","");
ScriptRegistrant<sla_tut_start> sla_tut_start_Registrant("sla_tut_start","gateid=0:int");
ScriptRegistrant<sla_m00_poweroff> sla_m00_poweroff_Registrant("sla_m00_poweroff","Id=0:int");

// M01
ScriptRegistrant<sla_m01_prisoniercont> sla_m01_prisoniercont_Registrant("sla_m01_prisoniercont","distroyid1=0:int,distroyid2=0:int,distroyid3=0:int,distroyid4=0:int");
ScriptRegistrant<sla_m01_start> sla_m01_start_Registrant("sla_m01_start","");
ScriptRegistrant<sla_m01_foodcart> sla_m01_foodcart_Registrant("sla_m01_foodcart","id=0:int");
ScriptRegistrant<sla_m01_final> sla_m01_final_Registrant("sla_m01_final","");
ScriptRegistrant<sla_m01_finaldoor> sla_m01_finaldoor_Registrant("sla_m01_finaldoor","GateID=0:int");
ScriptRegistrant<sla_m01_doorkey> sla_m01_doorkey_Registrant("sla_m01_doorkey","SwitchID=0:int");
ScriptRegistrant<sla_m01_samsite_controller> sla_m01_samsite_controller_Registrant("sla_m01_samsite_controller","count=0:int");

// M02
ScriptRegistrant<sla_m02_poweroffswitch> sla_m02_poweroffswitch_Registrant("sla_m02_poweroffswitch","Id=0:int");

// M03
ScriptRegistrant<sla_m03_start> sla_m03_start_Registrant("sla_m03_start","");
ScriptRegistrant<sla_m03_ccterminal> sla_m03_ccterminal_Registrant("sla_m03_ccterminal","");
ScriptRegistrant<sla_m03_buildcontroler> sla_m03_buildcontroler_Registrant("sla_m03_buildcontroler","CC=0:int");
ScriptRegistrant<sla_m03_beachsecure> sla_m03_beachsecure_Registrant("sla_m03_beachsecure","ObjectDestroy=0:int");
ScriptRegistrant<sla_m03_bot> sla_m03_bot_Registrant("sla_m03_bot","ID=0:int");

// M04
ScriptRegistrant<sla_m04_sabotage_controler> sla_m04_sabotage_controler_Registrant("sla_m04_sabotage_controler","ObjectDestroy=0:int,ObjectDestroy2=0:int,ObjectDestroy3=0:int");
ScriptRegistrant<sla_m04_sabotage_obj> sla_m04_sabotage_obj_Registrant("sla_m04_sabotage_obj","ID=0:int");
ScriptRegistrant<sla_m04_enginecontroller> sla_m04_enginecontroller_Registrant("sla_m04_enginecontroller","ObjectDestroy=0:int,ObjectDestroy2=0:int,ObjectDestroy3=0:int");
ScriptRegistrant<sla_m04_engine_obj> sla_m04_engine_obj_Registrant("sla_m04_engine_obj","ID=0:int");
ScriptRegistrant<sla_m04_ccterminal> sla_m04_ccterminal_Registrant("sla_m04_ccterminal","ID=0:int,Message=0:int");
ScriptRegistrant<sla_m04_submarine> sla_m04_submarine_Registrant("sla_m04_submarine","");

// M05
ScriptRegistrant<sla_m05_secure> sla_m05_secure_Registrant("sla_m05_secure","ObjectDestroy=0:int,ObjectDestroy2=0:int,ObjectDestroy3=0:int,ObjectDestroy4=0:int,Sound=BlaBlaBla:string");
ScriptRegistrant<sla_m05_securelast> sla_m05_securelast_Registrant("sla_m05_securelast","");

// M08
ScriptRegistrant<sla_m08_raveshaw> sla_m08_raveshaw_Registrant("sla_m08_raveshaw","");
ScriptRegistrant<sla_m08_terminallaser> sla_m08_terminallaser_Registrant("sla_m08_terminallaser","ID=0:int");
ScriptRegistrant<sla_m08_lasercontroller> sla_m08_lasercontroller_Registrant("sla_m08_lasercontroller","count=0:int,ID=0:int");

// M10
ScriptRegistrant<sla_m10_doors> sla_m10_doors_Registrant("sla_m10_doors","ID=0:int,ID2=0:int");

// M11
ScriptRegistrant<sla_M11_boss> sla_M11_boss_Registrant("sla_M11_boss","");

// M13
ScriptRegistrant<sla_m13_controller> sla_m13_controller_Registrant("sla_m13_controller","");

/*
 Sounds:
   EVA
  mxxdsgn_dsgn0010i1evag_snd - gameover
  mxxdsgn_dsgn0009i1evag_snd - enemy presents detected
  mxxdsgn_dsgn0008i1evag_snd - defeat

   Nod
  mx2dsgn_dsgn0014i1dsgn_snd - intruder stop him

   GDI
  mx2dsgn_dsgn0001i1dsgn_snd - take the humvee M02

   M02
  m02dsgn_dsgn0001i1eval_snd - objective and humvee
  m02dsgn_dsgn0003i1eval_snd - get inside, i dont see a main enterance (to be near bridge)
  m02dsgn_dsgn0005i1eval_snd - NOD OBELISK U IDIOT ! lol, joke, is a ob (to be near bridge)
  m02dsgn_dsgn0011i1eval_snd - find mct for big door (to be when get inside of tunnel)
  m02dsgn_dsgn0013i1eval_snd - big door open now (to be after u poke console terminal)
  m02dsgn_dsgn0017i1eval_snd - get on board of that cargo plane
  m02dsgn_dsgn0021i1eval_snd - destroy hand of nod
  m02dsgn_dsgn0043i1eval_snd - officer on tower (to be when approching tower)
  m02dsgn_dsgn0118i1evag_snd - eliminating sam sites will allow gdi airforces to advance
  m02dsgn_dsgn0140i1eval_snd - (when approach the tib machines)

   M03
  m03dsgn_dsgn0001i1eval_snd - startup
  m03dsgn_dsgn0003i1eval_snd - that building in the right is the cc
  m03dsgn_dsgn0004i1eval_snd - your approching cc
  m03dsgn_dsgn0006i1eval_snd - find keycard
  m03dsgn_dsgn0011r1eval_snd - check raf
  m03dsgn_dsgn0013i1evag_snd - flight data download complete (after poking that shit)
  m03dsgn_dsgn0012i1eval_snd - head inside and locate blabla
  m03dsgn_dsgn0014r1eval_snd - flight data part 2
  m03dsgn_dsgn0017i1eval_snd - GOTO SUBMARINE (flight data part 3) -btw.. timer started for blowing up the island
  m03dsgn_dsgn0019i1eval_snd - not much time, goto submarine !!
  m03dsgn_dsgn0069i1eval_snd - DONT DISTROY CC (idiot..lol)
  mxxdsgn_dsgn0048i1evag_snd - TIME EXPIRED
  m00_fail0001evag_snd - MISSION FAILED
  m00_wins0001evag_snd - MISSION ACCOMPLISHED
  00-n184e - Objective : locate cc and retrive info about missing idiots
  00-n186e - Objective : help n00bs to secure last beach
  00-n214e - Objective : use blabla sub (when approch that sub)
  00-n156e - VULCANO BOOM!lol
  00-n154e - Obj: Disable shore cannon
  00-n166e - WARNING nod base detected
  00-n150e - proced to dock area
  00-n100e - Your mission is a faliure
  00-n038e - New mision obj
  00-n026e - transmiting
  00-n028e - reciving
  00-n030e - data aqusition completed
  m00bncc_kill0053i1gbmg_snd - sorry kids, no tv tonight (to make m03 more funny :P ,lol)

   m05
 m05dsgn_dsgn0110i1gbmg_snd - hotwire poke 1
 m05dsgn_dsgn0111r1gsen_snd - hotwire poke 2
 m05dsgn_dsgn0112i1gsen_snd - help hoty 1
 m05dsgn_dsgn0113r1gbmg_snd - help hoty 2
 m05dsgn_dsgn0108i1gsen_snd - hoty mission objective announce
 m05dsgn_dsgn0119i1gsrs_snd - gunner mission objective announce
 m05dsgn_dsgn0121i1gbmg_snd - gunner poke 1
 m05dsgn_dsgn0122r1gsrs_snd - gunner poke 2
 m05dsgn_dsgn0134i1gsmg_snd - deadeye mission announce
 m05dsgn_dsgn0136i1gbmg_snd - deadeye poke 1
 m05dsgn_dsgn0137r1gsmg_snd - deadeye poke 2
 m05dsgn_dsgn0142i1gsgr_snd - meet with patch on catedral
 m05dsgn_dsgn0152i1vnch_snd - heli crash announce



*/