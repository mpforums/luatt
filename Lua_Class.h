extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}

#pragma warning(disable: 4291)

class LUA_Vector3 
{
private:
    float X_;
	float Y_;
	float Z_;
  public:
	LUA_Vector3()
	{
	}
    LUA_Vector3(float x, float y, float z)
	{
	  X_ = x;
	  Y_ = y;
	  Z_ = z;
	}
	float GetX()
	{
	  return X_;
	}
	float GetY()
	{
	  return Y_;
	}
	float GetZ()
	{
	  return Z_;
	}
	
	void AssignX(float x)
	{
	  X_ = x;
	}
	void AssignY(float y)
	{
	  Y_ = y;
	}
	void AssignZ(float z)
	{
	  Z_ = z;
	}
  

	float X()
	{
	  return X_;
	}
	float Y()
	{
	  return Y_;
	}
	float Z()
	{
	  return Z_;
	}

	void X(float x)
	{
	  X_ = x;
	}
	void Y(float y)
	{
	  Y_ = y;
	}
	void Z(float z)
	{
	  Z_ = z;
	}
};

class LuaVector3
{
  static const luaL_reg methods[];
  

  static int RegisterTable(lua_State *L) 
  {
	float x = (float)luaL_checknumber(L, 1);
	float  y = (float)luaL_checknumber(L, 2);
	float z = (float)luaL_checknumber(L, 3);
	LUA_Vector3 *a = new LUA_Vector3(x, y, z);
	lua_boxpointer(L, a);
	luaL_getmetatable(L, "Vector3");
	lua_setmetatable(L, -2);
	return 1;
  }
  static int GetX(lua_State *L)
  {
	LUA_Vector3 *a = GetInstance(L, 1);
	lua_pushnumber(L, a->GetX());
	return 1;
  }
  static int GetY(lua_State *L)
  {
    LUA_Vector3 *a = GetInstance(L, 1);
	lua_pushnumber(L, a->GetY());
	return 1;
  }
  static int GetZ(lua_State *L)
  {
	LUA_Vector3 *a = GetInstance(L, 1);
	lua_pushnumber(L, a->GetZ());
	return 1;
  }

  static int AssignX(lua_State *L)
  {
	LUA_Vector3 *a = GetInstance(L, 1);
	a->AssignX((float)luaL_checknumber(L, 2));
	return 0;
  }
  static int AssignY(lua_State *L)
  {
    LUA_Vector3 *a = GetInstance(L, 1);
	a->AssignY((float)luaL_checknumber(L, 2));
	return 0;
  }
  static int AssignZ(lua_State *L)
  {
	LUA_Vector3 *a = GetInstance(L, 1);
	a->AssignZ((float)luaL_checknumber(L, 2));
	return 0;
  }



	static int X(lua_State *L)
	{
		int argc = lua_gettop(L);
		if(argc < 1)
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			lua_pushnumber(L, a->GetX());
			return 1;
		}
		else
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			a->AssignX((float)luaL_checknumber(L, 2));
			return 0;
		}
	}
	static int Y(lua_State *L)
	{
		int argc = lua_gettop(L);
		if(argc < 1)
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			lua_pushnumber(L, a->GetY());
			return 1;
		}
		else
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			a->AssignY((float)luaL_checknumber(L, 2));
			return 0;
		}
	}
	static int Z(lua_State *L)
	{
		int argc = lua_gettop(L);
		if(argc < 1)
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			lua_pushnumber(L, a->GetZ());
			return 1;
		}
		else
		{
			LUA_Vector3 *a = GetInstance(L, 1);
			a->AssignZ((float)luaL_checknumber(L, 2));
			return 0;
		}
	}
	static int DeleteInstance(lua_State *L) 
	{
		 LUA_Vector3  *a = (LUA_Vector3 *)lua_unboxpointer(L, 1);
		  delete a;
		  return 0;
	}
public:
	static void Register(lua_State *L)
	{
		lua_newtable(L);                
		int methodtable = lua_gettop(L);
		luaL_newmetatable(L, "Vector3"); 
		int metatable = lua_gettop(L);

		lua_pushliteral(L, "__metatable");
		lua_pushvalue(L, methodtable);
		lua_settable(L, metatable);

		lua_pushliteral(L, "__index");
		lua_pushvalue(L, methodtable);
		lua_settable(L, metatable);

		lua_pushliteral(L, "__gc");
		lua_pushcfunction(L, DeleteInstance);
		lua_settable(L, metatable);

		lua_pop(L, 1);

		luaL_openlib(L, 0, methods, 0);
		lua_pop(L, 1);  

		lua_register(L, "Vector3", RegisterTable);
	}
	static LUA_Vector3 *GetInstance(lua_State *L, int n)
	{
		luaL_checktype(L, n, LUA_TUSERDATA);
		void *ud = luaL_checkudata(L, n, "Vector3");
		if(!ud) 
		{
			luaL_typerror(L, n, "Vector3");
		}
		return *(LUA_Vector3**)ud;
	}
};



class Box
{
public:
	OBBoxClass box;
	Box()
	{
		box.Center = Vector3(0.0, 0.0, 0.0);
		box.Extent = Vector3(0.0, 0.0, 0.0);
		for(int i = 0; i < 3; i++)
		{
			for(int y = 0; y < 3; y++)
			{
				box.Basis[i][y] = 0.0;
			}
		}
		
	}
    Box(float X, float Y, float Z, float Width, float Length, float Height)
	{
		for(int i = 0; i < 3; i++)
		{
			for(int y = 0; y < 3; y++)
			{
				box.Basis[i][y] = 0.0;
			}
		}
		box.Center = Vector3(X, Y, Z);
		box.Extent = Vector3(Width, Length, Height);
	}
};

class LuaBox
{
	static const luaL_reg methods[];
  

	static int RegisterTable(lua_State *L) 
	{
		int argc = lua_gettop(L);
		if(argc < 6)
		{
			Box *a = new Box();
			lua_boxpointer(L, a);
			luaL_getmetatable(L, "Box");
			lua_setmetatable(L, -2);
		}
		else
		{
			Box *a = new Box((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3), (float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5), (float)lua_tonumber(L, 6));
			lua_boxpointer(L, a);
			luaL_getmetatable(L, "Box");
			lua_setmetatable(L, -2);
		}
		
		return 1;
	}
	static int GetCenter(lua_State *L)
	{
		Box *b = GetInstance(L, 1);

		LUA_Vector3 *a = new LUA_Vector3(b->box.Center.X, b->box.Center.Y, b->box.Center.Z);
		lua_boxpointer(L, a);
		luaL_getmetatable(L, "Vector3");
		lua_setmetatable(L, -2);
		return 1;
	}

	static int SetCenter(lua_State *L)
	{
		int argc = lua_gettop(L);
		if(argc < 3)
		{
			return 0;
		}
		Box *b = GetInstance(L, 1);
		b->box.Center = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		return 0;
	}
	static int GetExtent(lua_State *L)
	{
		Box *b = GetInstance(L, 1);

		LUA_Vector3 *a = new LUA_Vector3((float)b->box.Extent.X, (float)b->box.Extent.Y, (float)b->box.Extent.Z);
		lua_boxpointer(L, a);
		luaL_getmetatable(L, "Vector3");
		lua_setmetatable(L, -2);
		return 1;
	}

	static int SetExtent(lua_State *L)
	{
		int argc = lua_gettop(L);
		if(argc < 3)
		{
			return 0;
		}
		Box *b = GetInstance(L, 1);
		b->box.Extent = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		return 0;
	}
	static int DeleteInstance(lua_State *L) 
	{
		Box  *a = (Box *)lua_unboxpointer(L, 1);
		delete a;
		return 0;
	}
public:
  static void Register(lua_State *L)
  {
	lua_newtable(L);                
	int methodtable = lua_gettop(L);
	luaL_newmetatable(L, "Box"); 
	int metatable = lua_gettop(L);

	lua_pushliteral(L, "__metatable");
    lua_pushvalue(L, methodtable);
    lua_settable(L, metatable);

    lua_pushliteral(L, "__index");
    lua_pushvalue(L, methodtable);
    lua_settable(L, metatable);

	lua_pushliteral(L, "__gc");
    lua_pushcfunction(L, DeleteInstance);
    lua_settable(L, metatable);

	lua_pop(L, 1);

	luaL_openlib(L, 0, methods, 0);
    lua_pop(L, 1);  

    lua_register(L, "Box", RegisterTable);
  }
	static Box *GetInstance(lua_State *L, int n)
	{
		luaL_checktype(L, n, LUA_TUSERDATA);
		void *ud = luaL_checkudata(L, n, "Box");
		if(!ud) luaL_typerror(L, n, "Box");
		return *(Box**)ud;
	}
};

/*
class SLNodeWrapper
{
	int *Data;


public:
	int _Type;
    SLNodeWrapper(int Type)
	{
		GenericSLNode *Iterator = 0;
		_Type = Type;
		if(Type == 0 || (Type < 0 || Type > 3))
		{
			_Type = 0;
			Iterator = PlayerList->HeadNode;
		}
		else if(Type == 1)
		{
			Iterator = BuildingGameObjList->HeadNode;
		}
		else if(Type == 2)
		{
			Iterator = GameObjManager::SmartGameObjList.Head();
		}
		else if(Type == 3)
		{		
			Iterator = GameObjManager::SmartGameObjList
		}

		Data = (int *)malloc(4);
		*Data = 0;
		for(int x = 0; Iterator != 0; Iterator = Iterator->NodeNext, x++)
		{
			Data = (int *)realloc(Data, (x+2)*4);
			Data[x] = Type == 0 ? ((cPlayer *)Iterator->NodeData)->PlayerId : Commands->Get_ID((GameObject *)Iterator->NodeData);
			Data[x+1] = 0;
		}
	}

	int Next()
	{
		if(*Data != 0)
		{
			return *(Data++);
		}

		return -1;
	}

	int End()
	{
		if(*Data != 0)
		{
			return 0;
		}
		else
		{
			return -1;
		}
	}
};

class LuaSLNodeWrapper
{
  static const luaL_reg methods[];
  

	static int RegisterTable(lua_State *L) 
	{
		int argc = lua_gettop(L);

		SLNodeWrapper *a = new SLNodeWrapper(argc < 1 ? 0 : (int)lua_tonumber(L, 1));
		lua_boxpointer(L, a);
		luaL_getmetatable(L, "SLNode");
		lua_setmetatable(L, -2);		
		return 1;
	}

	static int Next(lua_State *L)
	{
		SLNodeWrapper *a = GetInstance(L, 1);
		lua_pushnumber(L, a->Next());
		return 1;
	}
	static int End(lua_State *L)
	{
		SLNodeWrapper *a = GetInstance(L, 1);
		lua_pushnumber(L, a->End());
		return 1;
	}

	static int DeleteInstance(lua_State *L) 
	{
		SLNodeWrapper *a = (SLNodeWrapper *)lua_unboxpointer(L, 1);
		delete a;
		return 0;
	}
public:
	static void Register(lua_State *L)
	{
		lua_newtable(L);                
		int methodtable = lua_gettop(L);
		luaL_newmetatable(L, "SLNode"); 
		int metatable = lua_gettop(L);

		lua_pushliteral(L, "__metatable");
		lua_pushvalue(L, methodtable);
		lua_settable(L, metatable);

		lua_pushliteral(L, "__index");
		lua_pushvalue(L, methodtable);
		lua_settable(L, metatable);

		lua_pushliteral(L, "__gc");
		lua_pushcfunction(L, DeleteInstance);
		lua_settable(L, metatable);

		lua_pop(L, 1);

		luaL_openlib(L, 0, methods, 0);
		lua_pop(L, 1);  

		lua_register(L, "SLNode", RegisterTable);
	}
	static SLNodeWrapper *GetInstance(lua_State *L, int n)
	{
		luaL_checktype(L, n, LUA_TUSERDATA);
		void *ud = luaL_checkudata(L, n, "SLNode");
		if(!ud) luaL_typerror(L, n, "SLNode");
		return *(SLNodeWrapper**)ud;
	}
};

*/

