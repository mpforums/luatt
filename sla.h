/*	Renegade Scripts.dll
	Scripts by Stan "sla.ro" Laurentiu Alexandru 
	Copyright 2010-2011 Sla Company (http://sla-co.webs.com)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it 
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code 
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#pragma once

class sla_tut_start : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Entered(GameObject *obj, GameObject *enter);
 void Created(GameObject *obj);
 bool used;
};

class sla_enable_poke : public ScriptImpClass {
 void Created(GameObject *obj);
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_kill_distroyobject : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
};

class sla_tut_final : public ScriptImpClass {
	bool final;
	void Entered(GameObject *obj,GameObject *enter);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj, int number);
};

class sla_custom_playcinematic : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_tut_phase1 : public ScriptImpClass {
int poked;
void Poked(GameObject *obj,GameObject *poker);
void Timer_Expired(GameObject *obj, int number);
};

class sla_poke_distroyobject : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_poke_msg : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_on_enter_message : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
};

class sla_custom_waypathfollow : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_enter_send_custom : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_enter_enable_spawn : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_enter_changeteam : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_parachute : public ScriptImpClass {
 void Created(GameObject *obj);
 void Timer_Expired(GameObject *obj, int number);
 GameObject *parachute;
 int parachuteID;
};

class sla_m01_prisoniercont : public ScriptImpClass {
 int killx;
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_m01_start : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_teleport : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
  void Poked(GameObject *obj,GameObject *poker);
 bool spawn1;
 bool spawn2;
 bool spawn3;
 bool spawn4;
 bool tele;
 void Timer_Expired(GameObject *obj, int number);
 void Created(GameObject *obj);
};


class sla_m01_foodcart : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_custom_cinematic : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 bool can;
};

class sla_created_cinematic_attack : public ScriptImpClass {
	void Created(GameObject *obj);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj, int number);
};


class sla_m01_finaldoor : public ScriptImpClass {
 bool canbeopen;
 bool pokeable;
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_m01_doorkey : public ScriptImpClass {
 bool poked;
 void Poked(GameObject *obj,GameObject *poker);
};


class sla_m01_final : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Entered(GameObject *obj,GameObject *enter);
};


class sla_powerup_parachute : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_kill_sendcustom : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
 void Destroyed(GameObject *obj);
};

class sla_poke_sendcustom : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_picksound : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_killed_dropkey : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
};

class sla_Building_GDI : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
 void Timer_Expired(GameObject *obj, int number);
};

class sla_Building_NOD : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
 void Timer_Expired(GameObject *obj, int number);
};


class sla_m02_poweroffswitch : public ScriptImpClass {
 bool poked;
 void Timer_Expired(GameObject *obj, int number);
 void Killed(GameObject *obj, GameObject *shooter);
};

class sla_enter_sound : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_m03_start : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
 void Timer_Expired(GameObject *obj, int number);
 bool entere;
};

class sla_m03_ccterminal : public ScriptImpClass {
 bool poked;
 void Timer_Expired(GameObject *obj, int number);
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_m03_buildcontroler : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 void Damaged(GameObject *obj,GameObject *damager,float damage);
 void Timer_Expired(GameObject *obj, int number);
 bool destroy;
 bool say;
};

class sla_custom_teleport : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};


class sla_kill_message : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
};


class sla_created_message : public ScriptImpClass {
void Created(GameObject *obj);
};

class sla_poke_2soundplay : public ScriptImpClass {
 bool poked;
 void Timer_Expired(GameObject *obj, int number);
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_m05_final : public ScriptImpClass {
 bool poked;
 void Timer_Expired(GameObject *obj, int number);
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_noshoot : public ScriptImpClass {
void Created(GameObject *obj); // sets innate disabled
};

class sla_m08_raveshaw : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
};

class sla_custom_cinematic_repeat : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_objective_controller : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
 int obj_an;
};

class sla_enter_parachute : public ScriptImpClass {
 void Entered(GameObject *obj,GameObject *enter);
};

class sla_m10_building_poke : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
 bool poked;
};

class sla_coopinfomap : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Created(GameObject *obj);
};

class sla_killed_sound : public ScriptImpClass {
 bool poked;
 void Timer_Expired(GameObject *obj, int number);
 void Killed(GameObject *obj,GameObject *shooter);
};

class sla_m03_beachsecure : public ScriptImpClass {
 void Created(GameObject *obj);
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 int bots;
};

class sla_m03_bot : public ScriptImpClass {
 void Created(GameObject *obj);
 void Destroyed(GameObject *obj);
};

class sla_enter_send_custom_repeatdelay : public ScriptImpClass {
 bool entered;
 int repeats;
 void Entered(GameObject *obj,GameObject *enter);
 void Timer_Expired(GameObject *obj, int number);
};
class sla_Random_Tele_Enter : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
};

class sla_m01_samsite_controller : public ScriptImpClass {
 void Created(GameObject *obj);
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 int count;
};

class sla_m13_controller : public ScriptImpClass {
 void Created(GameObject *obj);
 void Timer_Expired(GameObject *obj, int number);
};

class sla_m05_secure : public ScriptImpClass {
 void Created(GameObject *obj);
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 int bots;
};

class sla_m05_securelast : public ScriptImpClass {
 void Created(GameObject *obj);
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 int bots;
};

class sla_enter_enable_spawn10 : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
};

class sla_m04_sabotage_controler : public ScriptImpClass {
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
 int bots;
 void Created(GameObject *obj);
};

class sla_m04_sabotage_obj : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_m04_enginecontroller : public ScriptImpClass {
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
 int bots;
 void Created(GameObject *obj);
};

class sla_m04_engine_obj : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
};

class sla_m04_ccterminal : public ScriptImpClass {
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
};

class sla_kill_message_delay : public ScriptImpClass {
 void Killed(GameObject *obj,GameObject *shooter);
 void Timer_Expired(GameObject *obj, int number);
};

class sla_m04_submarine : public ScriptImpClass {
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 bool open;
 void Entered(GameObject *obj, GameObject *enter);
};


class sla_m00_poweroff : public ScriptImpClass {
 void Killed(GameObject *obj, GameObject *shooter);
 void Timer_Expired(GameObject *obj, int number);
};

class sla_enter_kill_team : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
};

class sla_m08_lasercontroller : public ScriptImpClass {
 void Custom(GameObject *obj, int message, int param, GameObject *sender);
 int count;
 void Created(GameObject *obj);
};

class sla_m08_terminallaser : public ScriptImpClass {
 void Killed(GameObject *obj, GameObject *shooter);
};

class sla_m10_doors : public ScriptImpClass {
 void Poked(GameObject *obj,GameObject *poker);
};

class sla_M11_boss : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Created(GameObject *obj);
 void Killed(GameObject *obj,GameObject *shooter);
};


class sla_animal_logic : public ScriptImpClass {
 void Timer_Expired(GameObject *obj, int number);
 void Created(GameObject *obj);
 void Killed(GameObject *obj,GameObject *shooter);
 void Destroyed(GameObject *obj);

 int MY_STATE;
 Vector3 oldpos;
 bool Animal_Behivour;
 int MY_TARGET;
 int MY_TEAM;
};

class sla_soundmessage_custom : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class sla_blamo : public ScriptImpClass {
 void Created(GameObject *o);
};

class sla_ai_vehicle_controller : public ScriptImpClass {
 void Custom(GameObject *obj,int message,int param,GameObject *sender);
 void Timer_Expired(GameObject *obj, int number);
 void Created(GameObject *obj);
 bool delay;
 const char *sla_ai_vehicle_tanks[4];
};

class sla_ai_vehicle_script : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
};

class sla_facility_mct_controll : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	int ind;
	void Create(GameObject *obj);
};

class sla_poke_teleport : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Poked(GameObject *obj,GameObject *p);
	bool tel;
};

class sla_credit_tickle : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
};

class sla_destroy_building_on_start : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
};

class sla_enter_cinematic5 : public ScriptImpClass {
 void Entered(GameObject *obj, GameObject *enter);
 void Created(GameObject *obj);
 GameObject *o[5];
 int repeats;
};


class sla_new_teleport : public ScriptImpClass {
	void Poked(GameObject *obj,GameObject *o);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Entered(GameObject *obj, GameObject *o);
	void teleport(GameObject *o);
	int teleports[64];
	int totals;
};

class z_Set_Team : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_Cinematic_Attack : public ScriptImpClass {
	void Created(GameObject *obj);
	void Action_Complete(GameObject *obj, int action, ActionCompleteReason reason);
};

class z_Cinematic_Attack2 : public ScriptImpClass {
	void Created(GameObject *obj);
	void Action_Complete(GameObject *obj, int action, ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj, int number);
};

class z_Cinematic_Attack3 : public ScriptImpClass {
	void Created(GameObject *obj);
	void Action_Complete(GameObject *obj, int action, ActionCompleteReason reason);
};

