#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "GameObjManager.h"
#include "gmlog.h"
#include "engine_player.h"

extern "C" 
{
	//#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}

#include "LuaLib\lua.hpp"

#define lspop lua_tostring
#define lnpop lua_tonumber
#define lpushn lua_pushnumber
#define lpushs lua_pushstring
#define lua_checkboolean lua_toboolean
#define CCS const char *

#include "Misc.h"
#include "LuaManager.h"
#include "Lua_Class.h"
#include "time.h"

#include "gmvehicle.h"
#include "HarvesterClass.h"
#include "PhysClass.h"
#include "MoveablePhysClass.h"
#include "PhysicsSceneClass.h"
#include "CombatManager.h"
#include "PowerupGameObj.h"
#include "PowerupGameObjDef.h"

/*void* HookupAT3x(void* a, void* b, void* c, void* patch_start, void* patch_end, int (*version_selector)())
{
	return HookupAT3(a,b,c,patch_start,patch_end,version_selector);
}*/

#pragma warning(disable: 4244 4291)

int Get_ID(GameObject *O)
{
	if(O)
	{
		return Commands->Get_ID(O);	
	}
	return 0;
}

int Lua_Get_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushnumber(L, Get_ID(Commands->Find_Object(lua_tonumber(L, 1))));
	return 1;
}

int Lua_Console_Input(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Console_Input(lua_tostring(L, 1));	
	return 1;
}

int Lua_Console_Output(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Console_Output(lua_tostring(L, 1));	
	return 1;
}

int Lua_Get_Definition_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushstring(L, Get_Definition_Name(lua_tonumber(L, 1)));
	return 1;
}
int Lua_Get_Definition_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushnumber(L, Get_Definition_ID(lua_tostring(L, 1)));
	return 1;
}
int Lua_Get_Definition_Class_ID(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushnumber(L, Get_Definition_Class_ID(lua_tostring(L, 1)));
	return 1;
}
int Lua_Is_Valid_Preset_ID(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L,  Is_Valid_Preset_ID(lua_tonumber(L, 1)));
	return 1;
}
int Lua_Is_Valid_Preset(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L,  Is_Valid_Preset(lua_tostring(L, 1)));
	return 1;
}
int Lua_Set_Max_Health(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Max_Health(Commands->Find_Object(lnpop(L, 1)), lua_tonumber(L, 2));
	return 1;
}
int Lua_Set_Max_Shield_Strength(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Max_Shield_Strength(Commands->Find_Object(lnpop(L, 1)), lua_tonumber(L, 2));
	return 1;
}
int Lua_Get_Shield_Type(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushstring(L, Get_Shield_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Skin(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushstring(L, Get_Skin(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Set_Skin(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Skin(Commands->Find_Object(lnpop(L, 1)), lua_tostring(L, 2));
	return 1;
}
int Lua_Power_Base(lua_State *L) 
{
	if(lua_gettop(L) < 2) return 0;
	Power_Base(lua_tonumber(L, 1), lua_toboolean(L, 2));
	return 1;
}
int Lua_Set_Can_Generate_Soldiers(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Can_Generate_Soldiers(lua_tonumber(L, 1), lua_toboolean(L, 2));
	return 1;
}
int Lua_Set_Can_Generate_Vehicles(lua_State *L) 
{
	if(lua_gettop(L) < 2) return 0;
	Set_Can_Generate_Vehicles(lua_tonumber(L, 1), lua_toboolean(L, 2));
	return 1;
}
int Lua_Destroy_Base(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Destroy_Base(lua_tonumber(L, 1));
	return 1;
}
int Lua_Beacon_Destroyed_Base(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Beacon_Destroyed_Base(lua_tonumber(L, 1), lua_toboolean(L, 2));
	return 1;
}
int Lua_Enable_Base_Radar(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Enable_Base_Radar(lnpop(L, 1), lua_toboolean(L, 2));
	return 1;
}
int Lua_Is_Harvester(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *o = Commands->Find_Object(lnpop(L, 1));
	if (o == NULL || !o->As_VehicleGameObj()) return 0;
	lua_pushboolean(L,  Is_Harvester(o));
	return 1;
}
int Lua_Is_Radar_Enabled(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Radar_Enabled(lnpop(L, 1)));
	return 1;
}
int Lua_Building_Type(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Building_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_Building_Dead(lua_State *L) 
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Building_Dead(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Building(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_ID(Find_Building_By_Type(lnpop(L, 1), lnpop(L, 2)))); 
	return 1;
}
int Lua_Find_Base_Defense(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Base_Defense(lnpop(L, 1)))); 
	return 1;
}
int Lua_Is_Map_Flying(lua_State *L) 
{
	lua_pushboolean(L, Is_Map_Flying());
	return 0;
}
int Lua_Find_Harvester(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Harvester(lnpop(L, 1)))); 
	return 1;
}
int Lua_Is_Base_Powered(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Base_Powered(lnpop(L, 1)));
	return 1;
}
int Lua_Can_Generate_Vehicles(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Can_Generate_Vehicles(lnpop(L, 1)));
	return 1;
}
int Lua_Can_Generate_Soliders(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Can_Generate_Soldiers(lnpop(L, 1)));
	return 1;
}
int Lua_Is_A_Building(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	bool i=false;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()) 
	 {
		 i=true;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Get_Building_Count_Team(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Building_Count_Team(lnpop(L, 1)));
	return 1;
}

int Lua_Find_Building_By_Team(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Building_By_Team(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Building_By_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Building_By_Preset(lnpop(L, 1), lua_tostring(L, 2))));
	return 1;
}
int Lua_Find_Power_Plant(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Power_Plant(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Refinery(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Refinery(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Repair_Bay(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Repair_Bay(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Soldier_Factory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Soldier_Factory(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Airstrip(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Vehicle_Factory(lnpop(L, 1))));
	return 1;
}
int Lua_Find_War_Factory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Vehicle_Factory(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Vehicle_Factory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Vehicle_Factory(lnpop(L, 1))));
	return 1;
}
int Lua_Find_Com_Center(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Com_Center(lnpop(L, 1))));
	return 1;
}
int Lua_Is_Gameplay_Permitted(lua_State *L)
{
	lua_pushboolean(L, Is_Gameplay_Permitted());
	return 1;
}
int Lua_Is_Dedicated(lua_State *L) 
{
	lua_pushboolean(L, Is_Dedicated());
	return 1;
}

int Lua_Get_Current_Game_Mode(lua_State *L)
{
	lua_pushnumber(L, Get_Current_Game_Mode());
	return 1;
}

int Lua_Get_Harvester_Preset_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Harvester_Preset_ID(lnpop(L, 1)));
	return 1;
}
int Lua_Is_Harvester_Preset(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Harvester_Preset(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Destroy_Connection(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Evict_Client(lnpop(L, 1), "Destroyed Connection");
	return 1;
}

int Lua_Get_IP_Address(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *IP = Get_IP_Address(lnpop(L, 1));
	if(!IP)
	{
		return 0;
	}
	lpushs(L, IP);
	//delete []IP;
	return 1;
}
int Lua_Get_IP_Port(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *IP = Get_IP_Port(lnpop(L, 1));
	if(!IP)
	{
		return 0;
	}
	lpushs(L, IP);
	//delete []IP;
	return 1;
}
int Lua_Get_Bandwidth(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Bandwidth(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Ping(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Ping(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Kbits(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Kbits(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Object_Type(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Object_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Set_Object_Type(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Object_Type(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 1;
}
int Lua_Is_Building(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}

int Lua_Is_Soldier(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_SoldierGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Vehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_VehicleGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	//lpushn(L, Is_Vehicle(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
/*
int Lua_Is_Cinematic(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_Cinematic(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}*/

int Lua_Is_ScriptZone(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
		 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_ScriptZoneGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	//lpushn(L, Is_ScriptZone(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Is_Powerup(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
			 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_PhysicalGameObj() && obj->As_PhysicalGameObj()->As_PowerUpGameObj())
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	//lpushn(L, Is_Powerup(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_C4(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_PhysicalGameObj() && obj->As_PhysicalGameObj()->As_C4GameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Beacon(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
		int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_PhysicalGameObj() && obj->As_PhysicalGameObj()->As_BeaconGameObj())
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Armed(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_PhysicalGameObj() && obj->As_PhysicalGameObj()->As_ArmedGameObj())
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
/*
int Lua_Is_Simple(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_Simple(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}*/
int Lua_Is_PowerPlant(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
		 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_PowerPlantGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_SoldierFactory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
		 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_SoldierFactoryGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_VehicleFactory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
		 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_VehicleFactoryGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Airstrip(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_AirStripGameObj()) 
	 {
		 i=1;
	 }
	lpushn(L, i);
	return 1;
}
int Lua_Is_WarFactory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_WarFactoryGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Refinery(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_RefineryGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_ComCenter(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_ComCenterGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_RepairBay(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i=0;
	 GameObject *obj = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (obj->As_BuildingGameObj()->As_RepairBayGameObj()) 
	 {
		 i=1;
	 }
	lua_pushboolean(L, i);
	return 1;
}
/*
int Lua_Is_Scriptable(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_Scriptable(Commands->Find_Object(lnpop(L, 1))));
	return 1;

int Lua_Get_Building_Type(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushs(L, Get_Building_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
}*/
int Lua_Get_Object_Count(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Object_Count(lnpop(L, 2), lspop(L, 1)));
	return 1;
}
int Lua_Find_Random_Preset(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	lpushn(L, Get_ID(Find_Random_Preset(lspop(L, 1), lnpop(L, 2), lnpop(L, 2))));
	return 1;
}
int Lua_Send_Custom_To_Team_Buildings(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;
	Send_Custom_To_Team_Buildings(lnpop(L, 1), Commands->Find_Object(lnpop(L, 2)), lnpop(L, 3), lnpop(L,3), lnpop(L, 4));
	return 1;
}
int Lua_Send_Custom_To_Team_Preset(lua_State *L)
{
	if(lua_gettop(L) < 6) return 0;
	Send_Custom_To_Team_Preset(lnpop(L, 1), lspop(L, 2), Commands->Find_Object(lnpop(L, 3)), lnpop(L, 4), lnpop(L, 5), lnpop(L, 6));
	return 1;
}
int Lua_Send_Custom_All_Objects(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Send_Custom_All_Objects(lnpop(L, 1), Commands->Find_Object(lnpop(L, 2)), lnpop(L, 3));
	return 1;
}


int Lua_Send_Custom_Event_To_Object(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;
	Send_Custom_Event_To_Object(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lnpop(L, 3), lnpop(L, 4), lnpop(L, 5));
	return 1;
}

int Lua_Get_Is_Powerup_Persistant(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Get_Is_Powerup_Persistant(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Get_Powerup_Always_Allow_Grant(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Get_Powerup_Always_Allow_Grant(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Set_Powerup_Always_Allow_Grant(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Powerup_Always_Allow_Grant(Commands->Find_Object(lnpop(L, 1)), lua_toboolean(L, 2));
	return 1;
}
int Lua_Get_Powerup_Grant_Sound(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Powerup_Grant_Sound(Commands->Find_Object(lnpop(L, 1))));
	return 1;	
}
int Lua_Grant_Powerup(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Grant_Powerup(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2));
	return 1;
}
int Lua_Get_Vehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Grant_Refill(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Grant_Refill(Commands->Find_Object(lnpop(L, 1)));
	return 1;
}
int Lua_Change_Character(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lua_pushboolean(L, Change_Character(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Create_Vehicle(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Create_Vehicle(lspop(L, 4), lnpop(L, 3), Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)); 
	return 1;
}
int Lua_Toggle_Fly_Mode(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Toggle_Fly_Mode(Commands->Find_Object(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Vehicle_Occupant_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Vehicle_Occupant_Count(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Vehicle_Occupant(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Occupant(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2))));
	return 1;
}
int Lua_Get_Vehicle_Driver(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Driver(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Get_Vehicle_Gunner(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Gunner(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Force_Occupant_Exit(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Force_Occupant_Exit(Commands->Find_Object(lnpop(L, 2)), lnpop(L, 1));
	return 0;
}
int Lua_Force_Occupants_Exit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Force_Occupants_Exit(Commands->Find_Object(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Vehicle_Return(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Return(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Is_Stealth(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Stealth(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Fly_Mode(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Get_Fly_Mode(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Vehicle_Seat_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Vehicle_Seat_Count(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Soldier_Transition_Vehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Soldier_Transition_Vehicle(Commands->Find_Object(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Vehicle_Mode(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Get_Vehicle_Mode(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Team_Vehicle_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	//lpushn(L, Get_Team_Vehicle_Count(lnpop(L, 1)));
	lpushn(L, 0);
	
	return 1;
}
int Lua_Get_Vehicle_Owner(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Owner(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Force_Occupants_Exit_Team(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Force_Occupants_Exit_Team(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 1;
}
int Lua_Get_Vehicle_Definition_Mode(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Vehicle_Definition_Mode(lspop(L, 1)));
	return 1;
}

int Lua_IsInsideZone(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lua_pushboolean(L, IsInsideZone(Commands->Find_Object(lnpop(L, 1)), Commands->Find_Object(lnpop(L, 2))));
	return 1;
}
int Lua_Get_Vehicle_Definition_Mode_By_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Vehicle_Definition_Mode_By_ID(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Zone_Type(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Zone_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_IsAvailableForPurchase(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Is_Available_For_Purchase(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Vehicle_Gunner_Pos(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Vehicle_Gunner_Pos(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
/*int Lua_Set_Vehicle_Is_Visible(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Vehicle_Is_Visible(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 1;
}*/
int Lua_Set_Vehicle_Gunner(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Vehicle_Gunner(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 0;
}
int Lua_Get_Model(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushs(L, Get_Model(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Animation_Frame(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Animation_Frame(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_TrackedVehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
			 int i;
	 GameObject *sender = Commands->Find_Object(lnpop(L, 1));
	 if (!sender) { return 0; }
	 if (sender->As_PhysicalGameObj() && sender->As_PhysicalGameObj()->Peek_Physical_Object() && sender->As_PhysicalGameObj()->Peek_Physical_Object()->As_TrackedVehicleClass())
	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_VTOLVehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
				 int i;
	 GameObject *sender = Commands->Find_Object(lnpop(L, 1));
	 if (!sender) { return 0; }
	 if (sender->As_PhysicalGameObj() && sender->As_PhysicalGameObj()->Peek_Physical_Object() && sender->As_PhysicalGameObj()->Peek_Physical_Object()->As_VTOLVehicleClass())
	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_WheeledVehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
					 int i;
	 GameObject *sender = Commands->Find_Object(lnpop(L, 1));
	 if (!sender) { return 0; }
	 if (sender->As_PhysicalGameObj() && sender->As_PhysicalGameObj()->Peek_Physical_Object() && sender->As_PhysicalGameObj()->Peek_Physical_Object()->As_WheeledVehicleClass())
	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	lua_pushboolean(L, i);
	return 1;
}
int Lua_Is_Motorcycle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
					 int i;
	 GameObject *sender = Commands->Find_Object(lnpop(L, 1));
	 if (!sender) { return 0; }
	 if (sender->As_PhysicalGameObj() && sender->As_PhysicalGameObj()->Peek_Physical_Object() && sender->As_PhysicalGameObj()->Peek_Physical_Object()->As_MotorcycleClass())
	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	lua_pushboolean(L, i);
	return 1;
}
/*
int Lua_Is_Door(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
						 int i;
	 GameObject *sender = Commands->Find_Object(lnpop(L, 1));
	 if (!obj) { return 0; }
	 if (sender->As_PhysicalGameObj() && sender->As_PhysicalGameObj()->As_Do() && sender->As_PhysicalGameObj()->Peek_Physical_Object()->As_MotorcycleClass())
	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	return 1;
}

int Lua_Is_Elevator(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_Elevator(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Is_DamageableStaticPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_DamageableStaticPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_AccessablePhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_AccessablePhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_DecorationPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_DecorationPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_HumanPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_HumanPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Is_MotorVehicle(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_MotorVehicle(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_Phys3(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_Phys3(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_RigidBody(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_RigidBody(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_ShakeableStatricPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_ShakeableStatricPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_StaticAnimPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_StaticAnimPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_StaticPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_StaticPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_TimedDecorationPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_TimedDecorationPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_VehiclePhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_VehiclePhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}

int Lua_Is_DynamicAnimPhys(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_DynamicAnimPhys(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Is_BuildingAggregate(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushn(L, Is_BuildingAggregate(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
*/
int Lua_Is_Projectile(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 int i;
	 GameObject *seen = Commands->Find_Object(lnpop(L, 1));
	 if (!seen) { return 0; }
	 if (!(seen->As_PhysicalGameObj() && seen->As_PhysicalGameObj()->Peek_Physical_Object() && seen->As_PhysicalGameObj()->Peek_Physical_Object()->As_ProjectileClass()))

	 {
		 i=1;
	 }
	 else
	 {
		 i=0;
	 }
	lua_pushboolean(L, i);
	return 1;
}

int Lua_Copy_Transform(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Copy_Transform(Commands->Find_Object(lnpop(L, 1)), Commands->Find_Object(lnpop(L, 2)));
	return 0;
}
int Lua_Get_Mass(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Mass(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Htree_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushs(L, Get_Htree_Name(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Sex(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Sex(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Create_Effect_All_Of_Preset(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Create_Effect_All_Of_Preset(lspop(L, 1), lspop(L, 2), lnpop(L, 3), lua_toboolean(L, 4));
	return 0;
}
int Lua_Get_GameObj(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_GameObj(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Player_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Player_ID(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Player_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Player_Name(Commands->Find_Object(lnpop(L, 1)));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Player_Name_By_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Player_Name_By_ID(lnpop(L, 1));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Change_Team(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Change_Team(Commands->Find_Object(lnpop(L, 2)), lnpop(L, 1));
	return 0;
}
int Lua_Change_Team_By_ID(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Change_Team_By_ID(lnpop(L, 2), lnpop(L, 1));
	return 0;
}
int Lua_Get_Player_Count(lua_State *L)
{
	lpushn(L, Get_Player_Count());
	return 1;
}
int Lua_Get_Team_Player_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Team_Player_Count(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Team(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Team(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Rank(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Rank(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Kills(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Kills(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Deaths(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Deaths(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Score(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Score(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Money(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Money(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Kill_To_Death_Ratio(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Kill_To_Death_Ratio(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Part_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Rank(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Part_Names(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Part_Names(lspop(L, 1)));
	return 1;
}

int Lua_Get_GameObj_By_Player_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_GameObj_By_Player_Name(lspop(L, 1))));
	return 1;
}
int Lua_Purchase_Item(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Purchase_Item(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Set_Ladder_Points(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Ladder_Points(lnpop(L,1), lnpop(L,2));
	return 0;
}
int Lua_Set_Rung(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Rung(lnpop(L, 1), lnpop(L, 3));
	return 0;
}
int Lua_Set_Money(lua_State *L)
{
    if(lua_gettop(L) < 2) return 0;
	Set_Money(lnpop(L, 1), lnpop(L, 2));
	return 0;
}
int Lua_Set_Score(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Score(lnpop(L, 1), lnpop(L, 2));
	return 0;
}
int Lua_Find_First_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_First_Player(lnpop(L, 1))));
	return 1;
}
int Lua_Change_Player_Team(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	lpushn(L, Change_Player_Team(Commands->Find_Object(lnpop(L, 1)), lua_toboolean(L, 2), lua_toboolean(L, 3), lua_toboolean(L, 4)));
	return 1;
}
int Lua_Tally_Team_Size(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Tally_Team_Size(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Team_Score(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Team_Score(lnpop(L, 1)));
	return 1;
}
int Lua_Send_Custom_All_Players(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	 Send_Custom_All_Players(lnpop(L, 1), Commands->Find_Object(lnpop(L, 2)), lnpop(L, 3));
	return 0;
}
int Lua_Steal_Team_Credits(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Steal_Team_Credits(lnpop(L, 1), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Team_Credits(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Team_Credits(lnpop(L, 1)));
	return 1;
}
int Lua_Change_Team_2(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Change_Team_2(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 0;
}
int Lua_Get_Player_Type(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Player_Type(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Team_Cost(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Team_Cost(lspop(L, 1), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Cost(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Cost(lspop(L, 1)));
	return 1;
}
int Lua_Get_Team_Icon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushs(L, Get_Team_Icon(lspop(L, 1), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Icon(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushs(L, Get_Icon(lspop(L, 1)));
	return 1;
}
int Lua_Remove_Script(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Remove_Script(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2));
	return 1;
}
int Lua_Remove_All_Scripts(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Remove_All_Scripts(Commands->Find_Object(lnpop(L, 1)));
	return 0;
}
int Lua_Attach_Script_Preset(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	 Attach_Script_Preset(lspop(L, 1), lspop(L, 2), lspop(L, 3), lnpop(L, 4));
	return 0;
}
int Lua_Attach_Script_Type(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Attach_Script_Type(lspop(L, 1), lspop(L, 2), lnpop(L, 3), lnpop(L, 4));
	return 0;
}
int Lua_Remove_Script_Preset(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Remove_Script_Preset(lspop(L, 1), lspop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Remove_Script_Type(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	 Remove_Script_Type(lspop(L, 1), lnpop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Is_Script_Attached(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lua_pushboolean(L, Is_Script_Attached(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Attach_Script_Once(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Attach_Script_Once(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lspop(L, 3));
	return 0;
}
int Lua_Attach_Script_Preset_Once(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Attach_Script_Preset_Once(lspop(L, 1), lspop(L, 2), lspop(L, 3), lnpop(L, 4));	
	return 0;
}
int Lua_Attach_Script_Type_Once(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Attach_Script_Type_Once(lspop(L, 1), lspop(L, 2), lnpop(L, 3), lnpop(L, 4));
	return 0;
}
int Lua_Attach_Script_Building(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Attach_Script_Building(lspop(L, 1), lspop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Attach_Script_Is_Preset(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;
	Attach_Script_Is_Preset(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lspop(L, 3), lspop(L, 4), lnpop(L, 5));

	return 0;
}
int Lua_Attach_Script_Is_Type(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;
	Attach_Script_Is_Type(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2), lspop(L, 3), lspop(L, 4), lnpop(L, 5));
	return 0;
}
int Lua_Attach_Script_Player_Once(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Attach_Script_Player_Once(lspop(L, 1), lspop(L, 2), lnpop(L, 2));
	return 0;
}
int Lua_Remove_Duplicate_Script(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Remove_Duplicate_Script(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2));
	return 0;
}
int Lua_Attach_Script_All_Buildings_Team(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Attach_Script_All_Buildings_Team(lnpop(L, 1), lspop(L, 2), lspop(L, 3), lua_toboolean(L, 4));
	return 0;
}
int Lua_Attach_Script_All_Turrets_Team(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Attach_Script_All_Turrets_Team(lnpop(L, 1), lspop(L, 2), lspop(L, 3), lua_toboolean(L, 4));
	return 0;
}
int Lua_Find_Building_With_Script(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	lpushn(L, Get_ID(Find_Building_With_Script(lnpop(L, 1), lnpop(L, 2), lspop(L, 3), Commands->Find_Object(lnpop(L, 4)))));
	return 1;
}
int Lua_Find_Object_With_Script(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Find_Object_With_Script(lspop(L, 1))));
	return 1;
}
int Lua_Get_Translated_String(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Translated_String(lnpop(L, 1));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Translated_Preset_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Translated_Preset_Name(Commands->Find_Object(lnpop(L, 1)));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Translated_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	const char *ptr = Get_Translated_Weapon(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Current_Translated_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Current_Translated_Weapon(Commands->Find_Object(lnpop(L, 1)));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Team_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Team_Name(lnpop(L, 1));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Vehicle_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Vehicle_Name(Commands->Find_Object(lnpop(L, 1)));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Translated_Definition_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *ptr = Get_Translated_Definition_Name(lspop(L, 1));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	delete []ptr;
	return 1;
}
int Lua_Get_Current_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Current_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Clip_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Current_Total_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Total_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Total_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Total_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Clip_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Current_Max_Bullets(lua_State *L)
{
    if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Max_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Current_Clip_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Clip_Max_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Current_Total_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Total_Max_Bullets(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Max_Total_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Max_Total_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Max_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Max_Total_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Max_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Get_Position_Total_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Total_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Position_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Position_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Clip_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Position_Total_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Total_Max_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Position_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Max_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Position_Clip_Max_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Clip_Max_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Set_Current_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Current_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 0;
}
int Lua_Set_Current_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Current_Clip_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2));
	return 0;
}
int Lua_Set_Position_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Set_Position_Bullets(Commands->Find_Object(lnpop(L, 3)), lnpop(L, 1), lnpop(L, 2));
	return 0;
}
int Lua_Set_Position_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Set_Position_Clip_Bullets(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Set_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Set_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Set_Clip_Bullets(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	if (!lnpop(L, 1) || !lnpop(L, 2) || !lnpop(L, 3) || !Commands->Find_Object(lnpop(L, 1))) { return 0; }

	Set_Clip_Bullets(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lnpop(L, 3));
	return 1;
}
int Lua_Get_Powerup_Weapon(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	lpushs(L, Get_Powerup_Weapon(lspop(L, 1)));
	return 1;
}
int Lua_Get_Powerup_Weapon_By_Obj(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushs(L, Get_Powerup_Weapon_By_Obj(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Current_Weapon_Style(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Current_Weapon_Style(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Get_Position_Weapon_Style(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_Position_Weapon_Style(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Get_Weapon_Style(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	if (!lspop(L, 1)) { return 0; }
	if (!Commands->Find_Object(lnpop(L, 1))) { return 0; }
	if (!lspop(L, 2)) { return 0; }

	lpushn(L, Get_Weapon_Style(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Disarm_Beacon(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Disarm_Beacon(Commands->Find_Object(lnpop(L, 1)));
	return 0;
}
int Lua_Disarm_Beacons(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Disarm_Beacons(lnpop(L, 1));
	return 0;
}
int Lua_Disarm_C4(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Disarm_C4(Commands->Find_Object(lnpop(L, 1)));
	return 0;
}

int Lua_Get_Current_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *obj=Commands->Find_Object(lnpop(L, 1));
	const char *weaponname=Get_Current_Weapon(obj);
	if (!weaponname) { lpushs(L, "ERROR"); return 0; }
	lpushs(L, weaponname);
	return 1;
}

int Lua_Get_Weapon_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Weapon_Count(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushs(L, Get_Weapon(Commands->Find_Object(lnpop(L, 1)), lnpop(L, 2)));
	return 1;
}
int Lua_Has_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Has_Weapon(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2)));
	return 1;
}
int Lua_Find_Beacon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	lpushn(L, Get_ID(Find_Beacon(lnpop(L, 1), lnpop(L, 2))));
	return 1;
}
int Lua_Get_C4_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	if (lua_gettop(L) == 1) {
		lpushn(L, Get_C4_Count(lnpop(L, 1)));
	}
	else
	{
		lpushn(L, Get_C4_Count(lnpop(L, 1), lnpop(L, 2)));
	}

	return 1;
}
int Lua_Get_Beacon_Count(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_Beacon_Count(lnpop(L, 1)));
	return 1;
}
int Lua_Get_Mine_Limit(lua_State *L)
{
	lpushn(L, Get_Mine_Limit());
	return 1;
}
int Lua_Get_C4_Mode(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_C4_Mode(Commands->Find_Object(lnpop(L, 1))));
	return 1;
}
int Lua_Get_C4_Planter(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_C4_Planter(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Get_C4_Attached(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_C4_Attached(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}
int Lua_Get_Beacon_Planter(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lpushn(L, Get_ID(Get_Beacon_Planter(Commands->Find_Object(lnpop(L, 1)))));
	return 1;
}

int Lua_Create_Object(lua_State *L)
{
  if(lua_gettop(L) < 2) return 0;
  LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
  Vector3 pos = Vector3(a->GetX(), a->GetY(), a->GetZ());
  GameObject *obj = Commands->Create_Object(lspop(L, 1), pos);
  if(obj)
  {
	  lpushn(L, Get_ID(obj));
  }
  else
  {
	  return 0;
  }
  return 1;
}
int Lua_Destroy_Object(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
  Commands->Destroy_Object(Commands->Find_Object(lnpop(L, 1)));
  return 0;
}

int Lua_Get_Preset_Name(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
	const char *ptr = Commands->Get_Preset_Name(Commands->Find_Object(lnpop(L, 1)));
	if(!ptr)
	{
		return 0;
	}
	lpushs(L, ptr);
	return 1;
}

int Lua_Get_Position(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
  Vector3 pos = Commands->Get_Position(Commands->Find_Object(lnpop(L, 1)));
  LUA_Vector3 *a = new LUA_Vector3(pos.X, pos.Y, pos.Z);
  lua_boxpointer(L, a);
  luaL_getmetatable(L, "Vector3");
  lua_setmetatable(L, -2);

  return 1;
}
int Lua_Enable_Vehicle_Transitions(lua_State *L)
{
  if(lua_gettop(L) < 2) return 0;
  Commands->Enable_Vehicle_Transitions(Commands->Find_Object(lnpop(L, 1)), lua_toboolean(L, 2));
  return 0;
}

int Lua_Set_Model(lua_State *L)
{
  if(lua_gettop(L) < 2) return 0;
  Commands->Set_Model(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2));
  return 0;
}

int Lua_Goto_Location(lua_State *L)
{
  if(lua_gettop(L) < 7) return 0;
  ActionParamsStruct aps;
  aps.Set_Movement(Vector3(lnpop(L, 2), lnpop(L, 3), lnpop(L, 4)), lnpop(L, 5), lnpop(L, 6), lua_toboolean(L, 7));
  Commands->Action_Goto(Commands->Find_Object(lnpop(L, 1)), aps); 
  return 0;
}
int Lua_Goto_Object(lua_State *L)
{
  if(lua_gettop(L) < 4) return 0;
  ActionParamsStruct aps;
  aps.Set_Movement(Commands->Find_Object(lnpop(L, 2)), lnpop(L, 3), lnpop(L,4));
  Commands->Action_Goto(Commands->Find_Object(lnpop(L, 1)), aps); 
  return 0;
}

int Lua_Disable_Physical_Collisions(lua_State *L)
{
  if(lua_gettop(L) < 1) return 0;
	Commands->Disable_Physical_Collisions(Commands->Find_Object(lnpop(L, 1)));
  return 0;
}

int Lua_Enable_Collisions(lua_State *L)
{
  if(lua_gettop(L) < 1) return 0;
	Commands->Enable_Collisions(Commands->Find_Object(lnpop(L, 1)));
  return 0;
}

int Lua_Random_Building(lua_State *L)
{
	GameObject *obj = Find_Building_By_Type(Commands->Get_Random_Int(0, 1), Commands->Get_Random_Int(0, 9));
	while(!obj)
	{
		obj = Find_Building_By_Type(Commands->Get_Random_Int(0, 1), Commands->Get_Random_Int(0, 9));
	}
	lua_pushnumber(L, Commands->Get_ID(obj));
	return 1;
}

int Lua_Get_Current_Map(lua_State *L)
{
	lua_pushstring(L, The_Game()->Get_Map_Name());
	return 1;
}

int Lua_Get_Next_Map(lua_State *L)
{
  const char *map; 
	map = Get_Map(Get_Current_Map_Index() + 1); 
	 if (!map) {
	  map = Get_Map(0);
	 }
	 lua_pushstring(L, map);
	return 1;
}

int Lua_Set_Position(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Commands->Set_Position(obj, pos);
	return 0;
}
int Lua_Apply_Damage(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 3)
	{
		return 0;
	}
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	float amount = (float)lua_tonumber(L, 2);
	const char *type = lua_tostring(L, 3);
	GameObject *obj2 = (argc > 3 ? Commands->Find_Object(lua_tonumber(L, 4)) : 0);
	Commands->Apply_Damage(obj, amount, type, obj2);
	return 0;
}
int Lua_Get_Facing(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushnumber(L, Commands->Get_Facing(Commands->Find_Object(lua_tonumber(L, 1))));
	return 1;
}
int Lua_Set_Facing(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Commands->Set_Facing(obj, (float)lua_tonumber(L, 2));
	return 0;
}

int Lua_Set_Clouds(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Commands->Set_Clouds(lnpop(L, 1), lnpop(L, 2), lnpop(L, 3));
	return 0;
}
int Lua_Set_Ash(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	bool enable = (lua_tonumber(L, 3) > 0 ? 1 : 0);
	Commands->Set_Ash(lnpop(L, 1), lnpop(L, 2), enable);
	return 0;
}
int Lua_Set_Rain(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	bool enable = (lua_tonumber(L, 3) > 0 ? 1 : 0);
	Commands->Set_Snow(lnpop(L, 1), lnpop(L, 2), enable);
	return 0;
}
int Lua_Set_Snow(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	bool enable = (lua_tonumber(L, 3) > 0 ? 1 : 0);
	Commands->Set_Snow(lnpop(L, 1), lnpop(L, 2), enable);
	return 0;
}
int Lua_Set_Wind(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Commands->Set_Wind(lnpop(L, 1), lnpop(L, 2), lnpop(L, 3), lnpop(L, 4));
	return 0;
}
int Lua_Attach_Script(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Commands->Attach_Script(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lspop(L, 3));
	return 0;
}
int Lua_Create_Object_At_Bone(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	lua_pushnumber(L, Commands->Get_ID(Commands->Create_Object_At_Bone(Commands->Find_Object(lnpop(L, 1)), lspop(L, 2), lspop(L, 3))));
	return 1;
}
int Lua_Attach_To_Object_Bone(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Commands->Attach_To_Object_Bone(Commands->Find_Object(lnpop(L, 1)), Commands->Find_Object(lnpop(L, 2)), lspop(L, 3));
	return 0;
}
int Lua_Display_Health_Bar(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Commands->Display_Health_Bar(Commands->Find_Object(lnpop(L, 1)), lua_toboolean(L, 2));
	return 0;
}

int Lua_Create_Script_Zone(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	Box *a = LuaBox::GetInstance(L, 2);
	OBBoxClass box;
	for(int i = 0; i < 3; i++)
	{
		for(int y = 0; y < 3; y++)
		{
			box.Basis[i][y] = a->box.Basis[i][y];
		}
	}
	box.Center = a->box.Center;
	box.Extent = a->box.Extent;
	lua_pushnumber(L, Commands->Get_ID(Create_Zone(lspop(L, 1), box)));
	return 1;
}

int Lua_tClock(lua_State *L)
{
	lua_pushnumber(L, clock());
	return 1;
}

int Lua_Invoke(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	LuaManager::CallInvoke(lua_tostring(L, 1), lua_tostring(L, 2));
	return 0;
}


int Lua_Enable_Stealth(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	unsigned int obj = luaL_checknumber(L, 1);
	int stealth = luaL_checknumber(L, 2);
	GameObject *o = Commands->Find_Object(obj);
	if(o)
	{
		Commands->Enable_Stealth(o, stealth == 0 ? false : true);
	}
	else
	{
		luaL_argerror(L, 1, "Not a valid GameObject");
	}
	return 0;	
}

int Lua_Get_All_Buildings(lua_State *L)
{
	lua_newtable(L);
	int buildingTable = lua_gettop(L);
	int index = 1;

	for (SLNode<BaseGameObj>* node = GameObjManager::GameObjList.Head(); node; node = node->Next())
	{
		GameObject *o = (GameObject *)node->Data();

		lua_pushnumber(L, Commands->Get_ID(o));
		lua_rawseti(L, buildingTable, index++);
	}
	return 1;
}

int Lua_Set_Shield_Type(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *o = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!o) return 0;

	const char *s = luaL_checkstring(L, 2);
	if(!s) return 0;

	Commands->Set_Shield_Type(o, s);
	return 0;
}

 int Lua_FDSMessage(lua_State *L) 
 {
	 if(lua_gettop(L) < 2) return 0;
	const char *m = luaL_checkstring(L, 1);
	const char *h = luaL_checkstring(L, 2);
	SSGMGameLog::Log_Message( m,h);
	return 0;

 }

 int Lua_Set_Health(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	Commands->Set_Health(obj, luaL_checknumber(L, 2));
	return 0;
}

int Lua_Set_Shield_Strength(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	Commands->Set_Shield_Strength(obj, luaL_checknumber(L, 2));
	return 0;

}

int Lua_Is_A_Star(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	unsigned int obj = luaL_checknumber(L, 1);
	GameObject *x = Commands->Find_Object(obj);
	if(!x)
	{
		lua_pushboolean(L, 0);
	}
	else
	{
		if(Commands->Is_A_Star(x))
		{
			lua_pushboolean(L, 1);
		}
		else
		{
			lua_pushboolean(L, 0);
		}
	}
	return 1;
}	




int Lua_The_Game(lua_State *L)
{
	lua_newtable(L);
	cGameData *data = The_Game();
	if (!data) { return 0; }
	lua_pushboolean(L, data->Can_Repair_Buildings());
	lua_setfield(L, -2, "CanRepairBuildings");
	lua_pushnumber(L, data->Get_Current_Players());
	lua_setfield(L, -2, "CurrentPlayers");
	lua_pushboolean(L, data->Do_Maps_Loop());
	lua_setfield(L, -2, "DoMapsLoop");
	lua_pushboolean(L, data->Driver_Is_Always_Gunner());
	lua_setfield(L, -2, "DriverIsAlwaysGunner");
	lua_pushnumber(L, data->Get_Game_Duration_S());
	lua_setfield(L, -2, "GameDuration_Seconds");
	char GameTitle[256];
	wcstombs(GameTitle, (const wchar_t *)data->Get_Game_Title(), 256);
	lua_pushstring(L, GameTitle);
	lua_setfield(L, -2, "GameTitle");
	lua_pushnumber(L, data->Get_Intermission_Time_Remaining());
	lua_setfield(L, -2, "IntermissionTimeLeft");
	lua_pushnumber(L, data->Get_Ip_Address());
	lua_setfield(L, -2, "IP");
	lua_pushboolean(L, data->Is_Auto_Restart());
	lua_setfield(L, -2, "IsAutoRestart");
	lua_pushboolean(L, data->Is_Clan_Game());
	lua_setfield(L, -2, "IsClanGame");
	lua_pushboolean(L, data->Is_Dedicated());
	lua_setfield(L, -2, "IsDedicated");
	lua_pushboolean(L, data->Is_Friendly_Fire_Permitted());
	lua_setfield(L, -2, "IsFriendlyFirePermitted");
	lua_pushboolean(L, data->Is_Laddered());
	lua_setfield(L, -2, "IsLaddered");
	lua_pushboolean(L, data->Is_Passworded());
	lua_setfield(L, -2, "IsPassworded");
	lua_pushboolean(L, data->Is_QuickMatch_Server());
	lua_setfield(L, -2, "IsQuickMatch");
	lua_pushboolean(L, data->Is_Team_Changing_Allowed());
	lua_setfield(L, -2, "IsTeamChangingAllowed");
	lua_pushboolean(L, data->Is_Map_Cycle_Over());
	lua_setfield(L, -2, "MapCycleOver");
	lua_pushstring(L, data->Get_Map_Name());
	lua_setfield(L, -2, "MapName");
	lua_pushnumber(L, data->Get_Map_Number());
	lua_setfield(L, -2, "MapNumber");
	lua_pushnumber(L, data->Get_Max_Players());
	lua_setfield(L, -2, "MaxPlayers");
	lua_pushnumber(L, data->Get_Maximum_World_Distance());
	lua_setfield(L, -2, "MaxWorldDistance");
	lua_pushstring(L, data->Get_Mod_Name());
	lua_setfield(L, -2, "ModName");
	char Motd[256];
	wcstombs(Motd, (const wchar_t *)data->Get_Motd(), 256);
	lua_pushstring(L, Motd);
	lua_setfield(L, -2, "Motd");
	lua_pushnumber(L, data->Get_Mvp_Count());
	lua_setfield(L, -2, "MVPCount");
	char MVPName[256];
	wcstombs(MVPName, (const wchar_t *)data->Get_Mvp_Name(), 256);
	lua_pushstring(L, MVPName);
	lua_setfield(L, -2, "MVPName");
	char Owner[256];
	wcstombs(Owner, (const wchar_t *)data->Get_Owner(), 256);
	lua_pushstring(L, Owner);
	lua_setfield(L, -2, "Owner");
	char Password[256];
	wcstombs(Password, (const wchar_t *)data->Get_Password(), 256);
	lua_pushstring(L, Password);
	lua_setfield(L, -2, "Password");
	lua_pushnumber(L, data->Get_Port());
	lua_setfield(L, -2, "Port");
	lua_pushnumber(L, data->Get_Radar_Mode());
	lua_setfield(L, -2, "RadarMode");
	lua_pushboolean(L, data->Is_Remix_Teams());
	lua_setfield(L, -2, "RemixTeams");
	lua_pushboolean(L, data->Spawn_Weapons());
	lua_setfield(L, -2, "SpawnWeapons");
	lua_pushboolean(L, data->Do_String_Versions_Match());
	lua_setfield(L, -2, "StringVersionsMatch");
	lua_pushnumber(L, data->Get_Time_Limit_Minutes());
	lua_setfield(L, -2, "TimeLimit_Minutes");
	lua_pushnumber(L, data->Get_Time_Remaining_Seconds());
	lua_setfield(L, -2, "TimeRemaining_Seconds");
	lua_pushnumber(L, data->Get_Winner_ID());
	lua_setfield(L, -2, "WinnerID");
	lua_pushnumber(L, data->Get_Win_Type());
	lua_setfield(L, -2, "WinType");
	return 1;
}

/*
int Lua_cPlayer(lua_State *L)
{
	
	int ID = luaL_checknumber(L, 1);
	if(ID == 0)
	{
		return 0;
	}

	cPlayer *p = Find_Player(ID);	

	if(p)
	{
		lua_newtable(L);

		lua_pushnumber(L, p->AlliesKilled);
		lua_setfield(L, -2, "AlliesKilled");
		lua_pushnumber(L, p->ArmHit);
		lua_setfield(L, -2, "ArmHit");
		lua_pushnumber(L, p->ArmShots);
		lua_setfield(L, -2, "ArmShots");
		lua_pushnumber(L, p->BuildingDestroyed);
		lua_setfield(L, -2, "BuildingDestroyed");
		lua_pushnumber(L, p->CreditGrant);
		lua_setfield(L, -2, "CreditGrant");
		lua_pushnumber(L, p->CrotchHit);
		lua_setfield(L, -2, "CrotchHit");
		lua_pushnumber(L, p->CrotchShots);
		lua_setfield(L, -2, "CrotchShots");
		lua_pushnumber(L, p->DamageScaleFactor);
		lua_setfield(L, -2, "DamageScaleFactor");
		lua_pushnumber(L, p->Deaths);
		lua_setfield(L, -2, "Deaths");
		lua_pushnumber(L, p->EnemiesKilled);
		lua_setfield(L, -2, "EnemiesKilled");
		lua_pushnumber(L, p->FinalHealth);
		lua_setfield(L, -2, "FinalHealth");
		lua_pushnumber(L, p->Fps);
		lua_setfield(L, -2, "Fps");
		lua_pushnumber(L, p->GameTime);
		lua_setfield(L, -2, "GameTime");
		lua_pushnumber(L, p->HeadHit);
		lua_setfield(L, -2, "HeadHit");
		lua_pushnumber(L, p->HeadShots);
		lua_setfield(L, -2, "HeadShots");
		lua_pushnumber(L, p->IpAddress);
		lua_setfield(L, -2, "IpAddress");
		lua_pushboolean(L, p->IsActive);
		lua_setfield(L, -2, "IsActive");
		lua_pushboolean(L, p->god);
		lua_setfield(L, -2, "IsHuman");
		lua_pushboolean(L, p->IsInGame);
		lua_setfield(L, -2, "IsInGame");
		lua_pushboolean(L, p->IsWaitingForIntermission);
		lua_setfield(L, -2, "IsWaitingForIntermission");
		lua_pushnumber(L, p->JoinTime);
		lua_setfield(L, -2, "JoinTime");
		lua_pushnumber(L, p->Kills);
		lua_setfield(L, -2, "Kills");
		lua_pushnumber(L, p->KillsFromVehicle);
		lua_setfield(L, -2, "KillsFromVehicle");
		lua_pushnumber(L, p->Get_Last_Object_Id_I_Damaged());
		lua_setfield(L, -2, "LastDamaged");
		lua_pushnumber(L, p->Get_Last_Object_Id_I_Got_Damaged_By());
		lua_setfield(L, -2, "LastDamager");
		lua_pushnumber(L, p->LegHit);
		lua_setfield(L, -2, "LegHit");
		lua_pushnumber(L, p->LegShots);
		lua_setfield(L, -2, "LegShots");
		lua_pushnumber(L, p->Money);
		lua_setfield(L, -2, "Money");
		lua_pushnumber(L, p->Ping);
		lua_setfield(L, -2, "Ping");
		lua_pushnumber(L, p->PlayerId);
		lua_setfield(L, -2, "PlayerId");
		
		char pName[256];
		sprintf(pName, "%S", (const wchar_t *)p->PlayerName);
		lua_pushstring(L, pName);
		lua_setfield(L, -2, "PlayerName");

		lua_pushnumber(L, p->PlayerType);
		lua_setfield(L, -2, "PlayerType");
		lua_pushnumber(L, p->PowerupsCollected);
		lua_setfield(L, -2, "PowerupsCollected");
		lua_pushnumber(L, p->Rung);
		lua_setfield(L, -2, "Rung");
		lua_pushnumber(L, p->Score);
		lua_setfield(L, -2, "Score");
		lua_pushnumber(L, p->SessionTime);
		lua_setfield(L, -2, "SessionTime");
		lua_pushnumber(L, p->ShotsFired);
		lua_setfield(L, -2, "ShotsFired");
		lua_pushnumber(L, p->Squishes);
		lua_setfield(L, -2, "Squishes");
		lua_pushnumber(L, p->TorsoHit);
		lua_setfield(L, -2, "TorsoHit");
		lua_pushnumber(L, p->TorsoShots);
		lua_setfield(L, -2, "TorsoShots");
		lua_pushnumber(L, p->TotalTime);
		lua_setfield(L, -2, "TotalTime");
		lua_pushnumber(L, p->VehiclesDestroyed);
		lua_setfield(L, -2, "VehiclesDestroyed");
		lua_pushnumber(L, p->VehicleTime);
		lua_setfield(L, -2, "VehicleTime");

		return 1;
	}
	return 1;
}*/

int Lua_LongToIP(lua_State*L)
{
	if(lua_gettop(L) < 1) return 0;
	unsigned long lIP = luaL_checknumber(L, 1);	
	unsigned char *IP = (unsigned char *)&lIP;
	lua_pushnumber(L, IP[3]);
	lua_pushnumber(L, IP[2]);
	lua_pushnumber(L, IP[1]);
	lua_pushnumber(L, IP[0]);
	return 4;
}

int Lua_IPToLong(lua_State*L)
{
	if(lua_gettop(L) < 4) return 0;
	unsigned long IP = 0;
	unsigned char *x = (unsigned char *)&IP;
	x[3] = luaL_checknumber(L, 1);
	x[2] = luaL_checknumber(L, 2);
	x[1] = luaL_checknumber(L, 3);
	x[0] = luaL_checknumber(L, 4);
	char str[256];
	sprintf(str, "%lu", IP);
	lua_pushstring(L, str);
	return 1;
}


int Lua_GetTimeRemaining(lua_State *L)
{
	lua_pushnumber(L, The_Game()->Get_Time_Remaining_Seconds());
	//lua_pushnumber(L, The_Game()->Get_Game_Duration_S() > 0 ? The_Game()->Get_Time_Remaining_Seconds() : -1);
	return 1;
}

int Lua_Get_Health(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *x = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!x) return 0;

	lua_pushnumber(L, Commands->Get_Health(x));
	return 1;
}

int Lua_Get_Max_Health(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *x = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!x) return 0;

	lua_pushnumber(L, Commands->Get_Max_Health(x));
	return 1;
}

int Lua_Get_Shield_Strength(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *x = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!x) return 0;

	lua_pushnumber(L, Commands->Get_Shield_Strength(x));
	return 1;
}

int Lua_Get_Max_Shield_Strength(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *x = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!x) return 0;

	lua_pushnumber(L, Commands->Get_Max_Shield_Strength(x));
	return 1;
}


int Lua_Display_GDI_Player_Terminal_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *o = Commands->Find_Object(luaL_checknumber(L, 1));
	if(o)
	{
		Display_GDI_Player_Terminal_Player(o);
	}
	return 0;
}

int Lua_Display_Nod_Player_Terminal_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *o = Commands->Find_Object(luaL_checknumber(L, 1));
	if(o)
	{
		Display_NOD_Player_Terminal_Player(o);
	}
	return 0;
}

int Lua_Get_IDobj(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *x = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!x) return 0;
    int n = Commands->Get_ID(x);
	lua_pushnumber(L, n);
	return 1;
}
 

int Lua_Get_Map_By_Number(lua_State *L)
{
 if(lua_gettop(L) < 1) return 0;

	const char *map; 
	map = Get_Map(luaL_checknumber(L, 1)); 
	 if (!map) {
	  lua_pushstring(L, "NULL");
	  return 1;
	 }
	 lua_pushstring(L, map);
	return 1;
}

int Lua_Specate(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(obj)
	{
		Vector3 pos = Commands->Get_Position(obj);
		Remove_Script(obj, "MDB_Weapon_Scope");
		Commands->Clear_Weapons(obj);
		//Change_Team_2(obj, 2);
		Commands->Set_Is_Visible(obj, false);
		Commands->Set_Model(obj, "null");
		Commands->Set_Shield_Type(obj, "Blamo");
		bool flying;
		flying = Get_Fly_Mode(obj);
		if(!flying)
		{
			Toggle_Fly_Mode(obj);
		}
		Commands->Disable_All_Collisions(obj);
		//Set_Obj_Radar_Blip_Shape_Team(1, obj, 0);
		//Set_Obj_Radar_Blip_Shape_Team(0, obj, 0);
		pos.Z += 3.0f; 
		Commands->Set_Position(obj, pos);
		return 1;

		
	}
	return 0;
}

int Lua_HUD_Pokable(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	int stealth = luaL_checknumber(L, 2);
	GameObject *o = Commands->Find_Object(luaL_checknumber(L, 1));
	if(o)
	{
		Commands->Enable_HUD_Pokable_Indicator(o, stealth == 0 ? false : true);
		return 1;
	}
	return 0;
	
}

int Lua_Set_Background_Music(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	Commands->Set_Background_Music(lua_tostring(L, 1));
	return 1;
}

int Lua_Get_Background_Music(lua_State *L)
{
	lua_pushstring(L, GetCurrentMusicTrack());
	return 1;
}


int Lua_Kill_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	GameObject *obj = Get_GameObj(luaL_checknumber(L, 1));
	if (!obj) { return 0; }

	 GameObject *Veh = Get_Vehicle(obj);
	if (Veh)
	{
	  if (Get_Vehicle_Driver(Veh) == obj) 
	  {
	    Commands->Apply_Damage(Veh,99999.0f,"Death",false);
	  }
		Commands->Destroy_Object(obj);
	}

	Commands->Apply_Damage(obj,99999.0f,"Death",false);
	 return 1;
}


int Lua_Freeze_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

    int stealth = luaL_checknumber(L, 2);
	Commands->Control_Enable(Get_GameObj(luaL_checknumber(L, 1)), stealth == 0 ? false : true);
	return 1;
}

int Lua_Get_BW_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	lua_pushnumber(L, Get_Bandwidth(luaL_checknumber(L, 1)));
	return 1;
}


int Lua_Get_Vehicles_Limit(lua_State *L)
{
	lua_pushnumber(L, Get_Vehicle_Limit());
	return 1;
}

int Lua_Get_Player_Version(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	lua_pushnumber(L, Get_Client_Version(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Kick_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

    Evict_Client(lnpop(L, 1), lua_tostring(L, 2));
	return 1;
}


int Lua_Add_Console_Hook(lua_State *L)
{
	AddConsoleOutputHook(LuaManager::Call_Console_Output_Hook);
	return 1;
}


int Lua_Remove_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if (!obj) {
		return 0;
	}

	Remove_Weapon(obj,lua_tostring(L, 2));
	return 1;
}

int Lua_Remove_All_Weapons(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Commands->Clear_Weapons(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}


int Lua_Get_Preset_Name_By_Preset_ID(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushstring(L, Get_Translated_Definition_Name_Ini(Get_Definition_Name(luaL_checknumber(L, 1))));
	return 1;
}

int Lua_Get_Client_Serial_Hash(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	int pID = luaL_checknumber(L, 1);
	if (!pID) { return 0; }
	lua_pushstring(L, Get_Client_Serial_Hash(pID));
	return 1;
}

int Lua_Create_Explosion(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;

	LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 3));

	Commands->Create_Explosion(lua_tostring(L, 1),pos,obj);
	return 1;
}

int Lua_Create_Explosion_At_Bone(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 2));
	GameObject *creator = Commands->Find_Object(lua_tonumber(L, 4));
	Commands->Create_Explosion_At_Bone(lua_tostring(L, 1),obj,lua_tostring(L, 3),creator);
	return 1;
}

int Lua_Set_Fog_Enable(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	int stealth = luaL_checknumber(L, 1);
	Commands->Set_Fog_Enable(stealth == 0 ? false : true);
	return 1;
}

int Lua_Set_Fog_Range(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Commands->Set_Fog_Range(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3));
	return 1;
}

int Lua_Set_War_Blitz(lua_State *L)
{
	if(lua_gettop(L) < 6) return 0;
	Commands->Set_War_Blitz(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
	return 1;
}

int Lua_Play_Building_Announcement(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Commands->Play_Building_Announcement(obj, lua_tonumber(L, 2));
	return 1;
}

int Lua_Shake_Camera(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 1);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());

	Commands->Shake_Camera(pos, lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
	return 1;
}

int Lua_Add_RadioHook(lua_State *L)
{
	AddRadioHook(LuaManager::RadioHook);
	return 1;
}

int Lua_Is_Crate(lua_State *L) // this is how is done in SSGM.
{
	if(lua_gettop(L) < 1) return 0;

	bool craate=false; 
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	if (obj->As_PhysicalGameObj() && obj->As_PhysicalGameObj()->As_PowerUpGameObj())
		{
			if (stristr(Commands->Get_Preset_Name(obj),"crate"))
			{
				craate=true;
			}
		}

	lua_pushboolean(L, craate);
	return 1;
}

int Lua_Set_Air_Vehicle_Limit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	Set_Air_Vehicle_Limit(lua_tonumber(L, 1));
	return 1;
}

int Lua_Get_Air_Vehicle_Limit(lua_State *L)
{
	lua_pushnumber(L, Get_Air_Vehicle_Limit());
	return 1;
}

int Lua_Set_Vehicle_Limit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	Set_Vehicle_Limit(lua_tonumber(L, 1));
	return 1;
}


int Lua_Get_Vehicle_Limit(lua_State *L)
{
	lua_pushnumber(L, Get_Vehicle_Limit());
	return 1;
}

int Lua_Force_Camera_Look_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());

	Force_Camera_Look_Player(obj, pos);
	return 1;
}

int Lua_Set_Screen_Fade_Opacity_Player(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Set_Screen_Fade_Opacity_Player(obj ,lua_tonumber(L, 2), lua_tonumber(L, 3));
	return 1;
}


int Lua_Set_Screen_Fade_Color_Player(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Set_Screen_Fade_Color_Player(obj, lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
	return 1;
}

int Lua_Enable_Radar_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	int stealth = luaL_checknumber(L, 2);
	Enable_Radar_Player(obj, stealth == 0 ? false : true);
	return 1;
}

int Lua_Get_Build_Time_Multiplier(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	lua_pushnumber(L ,Get_Build_Time_Multiplier(lua_tonumber(L, 1)));
	return 1;
}

int Lua_Stop_Background_Music_Player(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Stop_Background_Music_Player(obj);
	return 1;
}

int Lua_Change_Time_Remaining(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	
	Change_Time_Remaining(lua_tonumber(L, 1));
	return 1;
}

int Lua_Change_Time_Limit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	
	Change_Time_Limit(lua_tonumber(L, 1));
	return 1;
}

int Lua_Create_3D_WAV_Sound_At_Bone(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Commands->Create_3D_WAV_Sound_At_Bone(lua_tostring(L, 1), Commands->Find_Object(lua_tonumber(L, 2)), lua_tostring(L, 3));
	return 1;
}


int Lua_Send_Message(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	
	Send_Message(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
	return 1;
}

int Lua_Select_Weapon(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Commands->Select_Weapon(obj, lua_tostring(L, 2));
	return 1;
}

int Lua_Create_2D_Sound_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Create_2D_Sound_Player(obj, lua_tostring(L, 2));
	return 1;
}

int Lua_Create_2D_Sound(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	Commands->Create_2D_WAV_Sound(lua_tostring(L, 1));
	return 1;
}

int Lua_DNS_IP_To_Host(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	hostent *host;  
    in_addr ipHost;
	ipHost.s_addr = inet_addr(lua_tostring(L, 1));
    host = gethostbyaddr( (const char*) &ipHost, sizeof(struct in_addr), AF_INET );
	//host = gethostbyname(lua_tostring(L, 1));

	if (host) {
	 lua_pushstring(L, host->h_name);
	}
	else {
	 lua_pushstring(L, "ERROR");
	}

	return 1;
}

int Lua_Get_Build_Version(lua_State *L)
{
	int versionlua = LUATT_BUILD;
	lua_pushnumber(L, versionlua);
	return 1;
}

int Lua_Get_Distance(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
	LUA_Vector3 *a2 = LuaVector3::GetInstance(L, 1);
	Vector3 pos1 = Vector3(a2->X(), a2->Y(), a2->Z());

	LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
	Vector3 pos2 = Vector3(a->X(), a->Y(), a->Z());

	lua_pushnumber(L, Commands->Get_Distance(pos1, pos2));
	return 1;
}

// Windows API


int Lua_Get_File_List(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	//char basepath[64];
	//GetCurrentDirectory(64, basepath);
	char basepath[64];
	char *fileslist[256];
	char folderlist[256];

	memset((void *)fileslist, 0, 256*4);
	GetCurrentDirectory(64, basepath);
	sprintf(folderlist, "%s\\%s", basepath,lua_tostring(L, 1));
	
	GetFiles(folderlist, fileslist, 256);
	lua_newtable(L);

	for(int i = 0; i < 256; i++)
	{
		const char *x = fileslist[i];
		if(!x)
		{
			continue;
		}

		lua_pushstring(L, x);
		lua_setfield(L, -2, x);

		delete []x;
	}

 return 1;
}

// Lua reload


int Lua_Reload_Lua(lua_State *L)
{
 LuaManager::Reload_Flag = true;
 return 1;
}

int Lua_Set_Animation(lua_State *L)
{
	if(lua_gettop(L) < 7) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	if (!obj) { return 0; }
	const char *anim = lua_tostring(L, 2);
	bool loop = lua_toboolean(L, 3);
	const char *subobj = lua_tostring(L, 4);
	float start = lua_tonumber(L, 5);
	float end = lua_tonumber(L, 6);
	bool blend = lua_toboolean(L, 7);

	if (strcmp(subobj, "") == 0) {
	   subobj=0;
	}

	if (blend != 0 && blend != 1) { blend=0; }
	if (loop != 0 && loop != 1) { loop=0; }

	Commands->Set_Animation(obj, anim, loop, subobj, start, end, blend);
 return 1;
}

int Lua_Add_TTDamageHook(lua_State *L)
{
	AddTtDamageHook(LuaManager::TT_Damage_Hook);
	return 1;
}

int Lua_Restore_Building(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));
	Restore_Building(obj);
	return 1;
}

int Lua_Revive_Building(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	GameObject *obj = Commands->Find_Object(lua_tonumber(L, 1));

	Restore_Building(obj);
	float max = Commands->Get_Max_Health(obj);
		Commands->Set_Health(obj, max);



		if (Is_Base_Powered(Get_Object_Type(obj)))
		{
			Commands->Set_Building_Power(obj, true);
		}
		else
		{
			Commands->Set_Building_Power(obj, false);
		}
		
	/*	BaseControllerClass* base = BaseControllerClass::Find_Base(Get_Object_Type(Building));
		if (base) 
		{
			if (Is_VehicleFactory(Building))
				base->Set_Can_Generate_Vehicles(true);
		}*/
		//Commands->Apply_Damage(obj, 1.0f, "Shrapnel", 0); // DEBUG CRAP

	return 1;
}

int Lua_Create_Building(lua_State *L)
{
   if(lua_gettop(L) < 2) return 0;

	LUA_Vector3 *a2 = LuaVector3::GetInstance(L, 2);
	Vector3 pos = Vector3(a2->X(), a2->Y(), a2->Z());
	const char *pres = lua_tostring(L, 1);

	GameObject * obj = Create_Building(pres, pos);
	lua_pushnumber(L, Commands->Get_ID(obj));
	return 1;
}


int Lua_Damage_Occupants(lua_State *L)
{
   if(lua_gettop(L) < 3) return 0;
	int ob = lua_tonumber(L, 1);
	Damage_Occupants(Commands->Find_Object(ob) ,lua_tonumber(L, 2) ,lua_tostring(L, 3));
	return 1;
}


int Lua_Set_Game_Title(lua_State *L)
{
   if(lua_gettop(L) < 1) return 0;
	
	const char *newtitle;
	newtitle = lua_tostring(L, 1);

  char *p=(char *)newtitle; //just for proper syntax highlighting ..."
  const WCHAR *pwcsName;
  // required size
  int nChars = MultiByteToWideChar(CP_ACP, 0, p, -1, NULL, 0);
  // allocate it
  pwcsName = new WCHAR[nChars];
  MultiByteToWideChar(CP_ACP, 0, p, -1, (LPWSTR)pwcsName, nChars);
  // use it....
    The_Cnc_Game()->Set_Game_Title(pwcsName);
  // delete it
  delete [] pwcsName;
 return 1;
}

int Lua_Set_Kills(lua_State *L)
{
	if (lua_gettop(L) < 2) return 0;
	Set_Kills(lua_tonumber(L, 1), lua_tonumber(L, 2));
	return 1;
}

/*
int Lua_Set_cPlayer(lua_State *L)
{
	int ID = luaL_checknumber(L, 1);

	// doesn't make sense.. if(ID == 0)	return 0;

	if(!lua_istable(L, 2))
	{
		luaL_error(L, "Invalid argument #2 to Set_cPlayer. Expected a table.");
		return 0;
	}

	cPlayer *p = Find_Player(ID);	

	if(!p) return 0;
		lua_getfield(L, -1, "AlliesKilled");
		if(lua_type(L, -1) > 0) p->AlliesKilled = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "ArmHit");
		if(lua_type(L, -1) > 0) p->ArmHit = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "ArmShots");
		if(lua_type(L, -1) > 0) p->ArmShots = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "BuildingDestroyed");
		if(lua_type(L, -1) > 0) p->BuildingDestroyed = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "CreditGrant");
		if(lua_type(L, -1) > 0) p->CreditGrant = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "CrotchHit");
		if(lua_type(L, -1) > 0) p->CrotchHit = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "CrotchShots");
		if(lua_type(L, -1) > 0) p->CrotchShots = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "DamageScaleFactor");
		if(lua_type(L, -1) > 0) p->DamageScaleFactor = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "Deaths");
		if(lua_type(L, -1) > 0) p->Deaths = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "EnemiesKilled");
		if(lua_type(L, -1) > 0) p->EnemiesKilled = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "GameTime");
		if(lua_type(L, -1) > 0) p->GameTime = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "HeadHit");
		if(lua_type(L, -1) > 0) p->HeadHit = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "HeadShots");
		if(lua_type(L, -1) > 0) p->HeadShots = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "Kills");
		if(lua_type(L, -1) > 0) p->Kills = lua_tonumber(L, -1);
		lua_pop(L, 1);
		
		lua_getfield(L, -1, "KillsFromVehicle");
		if(lua_type(L, -1) > 0) p->KillsFromVehicle = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "LegHit");
		if(lua_type(L, -1) > 0) p->LegHit = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "LegShots");
		if(lua_type(L, -1) > 0) p->LegShots = lua_tonumber(L, -1);
		lua_pop(L, 1);

	    lua_getfield(L, -1, "Money");
		if(lua_type(L, -1) > 0) p->Money = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "Ping");
		if(lua_type(L, -1) > 0) p->Ping = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "PlayerId");
		if(lua_type(L, -1) > 0) p->PlayerId = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "PlayerType");
		if(lua_type(L, -1) > 0) p->PlayerType = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "PowerupsCollected");
		if(lua_type(L, -1) > 0) p->PowerupsCollected = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "Rung");
		if(lua_type(L, -1) > 0) p->Rung = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "Score");
		if(lua_type(L, -1) > 0) p->Score = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "SessionTime");
		if(lua_type(L, -1) > 0) p->SessionTime = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "ShotsFired");
		if(lua_type(L, -1) > 0) p->ShotsFired = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "Squishes");
		if(lua_type(L, -1) > 0) p->Squishes = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "TorsoHit");
		if(lua_type(L, -1) > 0) p->TorsoHit = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "TorsoShots");
		if(lua_type(L, -1) > 0) p->TorsoShots = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "TotalTime");
		if(lua_type(L, -1) > 0) p->TotalTime = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "VehiclesDestroyed");
		if(lua_type(L, -1) > 0) p->VehiclesDestroyed = lua_tonumber(L, -1);
		lua_pop(L, 1);

		lua_getfield(L, -1, "VehicleTime");
		if(lua_type(L, -1) > 0) p->VehicleTime = lua_tonumber(L, -1);
		lua_pop(L, 1);

	
		return 1;
}*/

/* can't be fixed. only if TT will unblock them. they are protected.
int Lua_Set_The_Game(lua_State *L)
{
	if(!lua_istable(L, -1))
	{
		luaL_error(L, "Invalid argument #1 to Set_The_Game. Expected table.");
		return 0;
	}
	cGameData *data = The_Game();
	lua_getfield(L, -1, "CanRepairBuildings");
	if(lua_type(L, -1) > 0) data->Can_Repair_Buildings = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "DoMapsLoop");
	if(lua_type(L, -1) > 0) data->Do_Maps_Loop = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "DriverIsAlwaysGunner");
	if(lua_type(L, -1) > 0) data->DriverIsAlwaysGunner = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "GameplayPermitted");
	if(lua_type(L, -1) > 0) data->GameplayPermitted = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "GameTitle");
	if(lua_type(L, -1) > 0) data->GameTitle.Convert_From(lua_tostring(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "GrantWeapons");
	if(lua_type(L, -1) > 0) data->GrantWeapons = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IntermissionTime_Seconds");
	if(lua_type(L, -1) > 0) data->IntermissionTime_Seconds = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IntermissionTimeLeft");
	if(lua_type(L, -1) > 0) data->IntermissionTimeLeft = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsAutoRestart");
	if(lua_type(L, -1) > 0) data->IsAutoRestart = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsClanGame");
	if(lua_type(L, -1) > 0) data->IsClanGame = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsDedicated");
	if(lua_type(L, -1) > 0) data->IsDedicated = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsFriendlyFirePermitted");
	if(lua_type(L, -1) > 0) data->IsFriendlyFirePermitted = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsLaddered");
	if(lua_type(L, -1) > 0) data->IsLaddered = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsPassworded");
	if(lua_type(L, -1) > 0) data->IsPassworded = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsQuickMatch");
	if(lua_type(L, -1) > 0) data->IsQuickMatch = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "IsTeamChangingAllowed");
	if(lua_type(L, -1) > 0) data->IsTeamChangingAllowed = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MapCycleOver");
	if(lua_type(L, -1) > 0) data->MapCycleOver = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MapNumber");
	if(lua_type(L, -1) > 0) data->MapNumber = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MaxPlayers");
	if(lua_type(L, -1) > 0) data->MaxPlayers = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MaxWorldDistance");
	if(lua_type(L, -1) > 0) data->MaxWorldDistance = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MinQualifyingTime_Minutes");
	if(lua_type(L, -1) > 0) data->MinQualifyingTime_Minutes = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "Motd");
	if(lua_type(L, -1) > 0) data->Motd.Convert_From(lua_tostring(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "MVPCount");
	if(lua_type(L, -1) > 0) data->MVPCount = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "MVPName");
	if(lua_type(L, -1) > 0) data->MVPName.Convert_From(lua_tostring(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "Password");
	if(lua_type(L, -1) > 0) data->Password.Convert_From(lua_tostring(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "RadarMode");
	if(lua_type(L, -1) > 0) data->RadarMode = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "RemixTeams");
	if(lua_type(L, -1) > 0) data->RemixTeams = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "SettingsDescription");
	if(lua_type(L, -1) > 0) data->SettingsDescription.Convert_From(lua_tostring(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "SpawnWeapons");
	if(lua_type(L, -1) > 0) data->SpawnWeapons = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "StringVersionsMatch");
	if(lua_type(L, -1) > 0) data->StringVersionsMatch = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "TimeLimit_Minutes");
	if(lua_type(L, -1) > 0) data->TimeLimit_Minutes = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "TimeRemaining_Seconds");
	if(lua_type(L, -1) > 0) data->TimeRemaining_Seconds = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "UseLagReduction");
	if(lua_type(L, -1) > 0) data->UseLagReduction = lua_toboolean(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "WinnerID");
	if(lua_type(L, -1) > 0) data->WinnerID = lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "WinType");
	if(lua_type(L, -1) > 0) data->WinType = lua_tonumber(L, -1);
	lua_pop(L, 1);

	return 0;
}

*/

void inline MemoryWrite(void *address, unsigned char c)
{
	DWORD old;
	VirtualProtect((void *)address, 1, PAGE_EXECUTE_READWRITE, &old);
	memcpy((void *)address, &c, 1);
	VirtualProtect((void *)address, 1, old, &old);
}

void inline MemoryRead(void *address, unsigned char *c)
{
	DWORD old;
	VirtualProtect((void *)address, 1, PAGE_EXECUTE_READWRITE, &old);
	memcpy(c, address, 1);
	VirtualProtect((void *)address, 1, old, &old);
}

int Lua_Memory_Write(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	unsigned int Address = luaL_checknumber(L, 1);
	if(Address == 0)
	{
		return 0;
	}

	if(!lua_istable(L, 2))
	{
		luaL_typerror(L, 2, lua_typename(L, LUA_TTABLE));
		return 0;
	}

	for(int i = 1;;i++)
	{
		lua_rawgeti(L, 2, i);
		if(lua_type(L, -1) <= 0)
		{
			lua_pop(L, 1);
			break;
		}
		else
		{
			unsigned char c = lua_tonumber(L, -1);
			MemoryWrite((void *)(Address+i-1), c);
			lua_pop(L, 1);
		}
	}
	return 0;
}

int Lua_Memory_Read(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	unsigned int Address = luaL_checknumber(L, 1);
	int Count = luaL_checknumber(L, 2);

	if(Address == 0 || Count == 0)
	{
		return 0;
	}

	lua_newtable(L);

	for(int i = 0; i < Count; i++)
	{
		unsigned char c;
		MemoryRead((void *)(Address+i), &c);
		lua_pushnumber(L, c);
		lua_rawseti(L, -2, i+1);
	}

	return 1;
}



int Lua_Is_Vehicle_Owner(lua_State *L)
{
   if(lua_gettop(L) < 2) return 0;

	GameObject *o = Commands->Find_Object(lua_tonumber(L, 1));
    GameObject *os = Commands->Find_Object(lua_tonumber(L, 2));

	SSGM_Vehicle *scr = (SSGM_Vehicle *)Find_Script_On_Object(o,"SSGM_Vehicle");

	if (scr)
	{
		if (Commands->Find_Object(scr->OwnerID) == os)
		{
			lua_pushboolean(L, true);
		}
		else {
			lua_pushboolean(L, false);
		}
	}

	return 1;
}


int Lua_Warp_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;

	GameObject *Soldier=Commands->Find_Object(lnpop(L, 1));
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 2);
	Vector3 WarpPos = Vector3(a->GetX(), a->GetY(), a->GetZ());
	Vector3 WarpPos2 = WarpPos;
	PhysClass *Phys = Soldier->As_PhysicalGameObj()->Peek_Physical_Object();

    if(Phys && Phys->As_MoveablePhysClass())
    {
    	if(Phys->As_MoveablePhysClass()->Find_Teleport_Location(WarpPos, 0.5f, &WarpPos)) { // check..
    		Commands->Set_Position(Soldier, WarpPos2); // warp
			lua_pushboolean(L, true);
			return 1;
		}
    }
	lua_pushboolean(L, false);
	return 1;
}

int Lua_Set_Max_Speed(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	obj->As_SoldierGameObj()->Set_Max_Speed((float)lnpop(L, 2));
	return 0;
}

int Lua_Get_Max_Speed(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	lpushn(L, obj->As_SoldierGameObj()->Get_Max_Speed());
	return 1;
}


int Lua_Hide_Object_Player(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if (!obj) return 0;
	int ID = luaL_checknumber(L, 2);
	bool pending = obj->Is_Delete_Pending();
	obj->Set_Is_Delete_Pending(true);
	Update_Network_Object_Player(obj, ID); // say goodbye to object :P
	obj->Set_Is_Delete_Pending(pending);
	return 1;
}

int Lua_Set_Is_Powerup_Persistant(lua_State *L)
{
	if (lua_gettop(L) < 1) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if (!obj) return 0;
	((PowerUpGameObjDef &)(obj->As_PhysicalGameObj()->As_PowerUpGameObj()->Get_Definition())).Persistent = lua_checkboolean(L, 2);
	return 1;
}


int Lua_Is_WOL_User(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;

	bool n = Is_WOL_User(WideStringClass(lua_tostring(L,1)).Peek_Buffer());
    lua_pushboolean(L, n);

  return 1;
}
#pragma warning(disable: 4533)

int Lua_OColision(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	GameObject *Player = Commands->Find_Object(luaL_checknumber(L, 3));
	if(!Player) return 0;
	Matrix3D NewMat;

	float Facing=luaL_checknumber(L, 4);

	LUA_Vector3 *a = LuaVector3::GetInstance(L, 1);
	Vector3 Pos = Vector3(a->X(), a->Y(), a->Z());

	LUA_Vector3 *ab = LuaVector3::GetInstance(L, 2);
	Vector3 Size = Vector3(ab->X(), ab->Y(), ab->Z());
	int ERROR_CODE = 0;

	PhysClass *PObj = Player ? (Player->As_PhysicalGameObj() ? Player->As_PhysicalGameObj()->Peek_Physical_Object() : 0) : 0; // Get our PhysClass obj if any.
	if(PObj)
		PObj->Inc_Ignore_Counter(); // Ignore that in our ray/box casting for a second cause we dont want to include that.

	// Make sure we got the correct Height distance from the ground..
	Vector3 RealPos = Pos;
	RealPos.Z = -9999;
	LineSegClass Ray1(Pos, RealPos);
	CastResultStruct Result1;
	Result1.ComputeContactPoint = true;
	PhysRayCollisionTestClass Ray_Test1(Ray1, &Result1, TERRAIN_ONLY_COLLISION_GROUP, COLLISION_TYPE_ALL);
	PhysicsSceneClass::Get_Instance()->Cast_Ray(Ray_Test1);

	if(Result1.ContactPoint == Vector3(0,0,0)) // No ground here..
	{
		ERROR_CODE = 1;
		goto Exit;
	}

	CastResultStruct Result2;
	PhysRayCollisionTestClass Ray_Test2(Ray1, &Result2, DEFAULT_COLLISION_GROUP, COLLISION_TYPE_PHYSICAL);
	Ray_Test2.CheckDynamicObjs = false;
	PhysicsSceneClass::Get_Instance()->Cast_Ray(Ray_Test2);

	if(Ray_Test2.CollidedPhysObj && Ray_Test2.CollidedPhysObj->Get_Observer() && ((CombatPhysObserverClass *)Ray_Test2.CollidedPhysObj->Get_Observer())->As_BuildingGameObj()) // Hmm we collided with a building.. no good.
	{
		ERROR_CODE = 2;
		goto Exit;
	}

	RealPos.Z = Result1.ContactPoint.Z + (Size.Z / 2) + 0.1f; // Now calculate the real Z position so it will be at the ground level but add 0.1 for error so the box test will go well without contact with the ground

	// Now compute the terrain stuff;

	// Get the ground rotation.
	Matrix3D Mat;
	Vector3 Contact = Result1.ContactPoint;
	Mat.Look_At(Contact, Contact - Result1.Normal, 0);

	// Apply our facing.
	Matrix3 Rotation = Matrix3(Mat);
	Rotation.Rotate_Z(0);
	Rotation.Rotate_Z(Facing * (3.14159265f / 180));
	
	// Create an OBBoxClass.. We need that one cause AABoxClass does not support rotations.
	OBBoxClass Box(RealPos, Size / 2, Rotation);

	// Cast that shit.
	CastResultStruct Result3;
	Result3.ComputeContactPoint = true;
	PhysOBBoxCollisionTestClass Col_Test(Box, RealPos, &Result3, DEFAULT_COLLISION_GROUP, COLLISION_TYPE_PHYSICAL);
	PhysicsSceneClass::Get_Instance()->Cast_OBBox(Col_Test);
	
	if(Result3.StartBad) // Apparently we couldn't create it here..
	{
		ERROR_CODE = 3;
		goto Exit;
	}

Exit:
	NewMat = Matrix3D(Rotation, RealPos); // Safe to set that to the Rotation and RealPos.

	if(PObj)
		PObj->Dec_Ignore_Counter(); // Make it count in our physics tests again.

	lpushn(L, ERROR_CODE);
	return 1;
}

int Lua_ShowTexture(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
    GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	Set_Info_Texture(obj, lua_tostring(L, 2));
	return 1;
}

int Lua_HideTexture(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
    GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if(!obj) return 0;
	Clear_Info_Texture(obj);
	return 1;
}


// LuaTT.lua ported here..

int Lua_Get_All_Players(lua_State *L)
{
	lua_newtable(L);
	int rotationTable = lua_gettop(L);
	int totalofplayersfound=0;
	if (Get_Player_Count() == 0) return 1;

	for(int i = 0;; i++)
	{

		if (Get_GameObj(i))
	    {
	      lua_pushnumber(L, i);
		  totalofplayersfound++;
		  lua_rawseti(L, rotationTable, totalofplayersfound);
	    }

		if (totalofplayersfound >= Get_Player_Count()) { return 1; } // this little thing will save the day :)
		if (i > 999) return 1;
	}
	return 0;
}

int Lua_Get_Rotation(lua_State *L)
{
	lua_newtable(L);
	int rotationTable = lua_gettop(L);
	for(int i = 0;;)
	{
		if (i > 999) { return 1; }
		if (Get_Map(i)) { return 1; }
		lua_pushstring(L, Get_Map(i));
		lua_rawseti(L, rotationTable, i+1);
		i++;
	}


	return 1;
}


int Lua_Get_All_Vehicles(lua_State *L)
{
	lua_newtable(L);
	int buildingTable = lua_gettop(L);
	int index = 1;

	for (SLNode<BaseGameObj>* node = GameObjManager::GameObjList.Head(); node; node = node->Next())
	{
		GameObject *o = (GameObject *)node->Data();

		if (o->As_VehicleGameObj()) {
		 lua_pushnumber(L, Commands->Get_ID(o));
		 lua_rawseti(L, buildingTable, index++);
		}
	}

	return 1;
}

int Lua_Get_All_Buildings2(lua_State *L)
{
	lua_newtable(L);
	int buildingTable = lua_gettop(L);
	int index = 1;

	for (SLNode<BuildingGameObj>* node = GameObjManager::BuildingGameObjList.Head(); node; node = node->Next())
	{
		GameObject *o = (GameObject *)node->Data();
		  lua_pushnumber(L, Commands->Get_ID(o));
		  lua_rawseti(L, buildingTable, index++);
	}

	return 1;
}

// 1.5 by sla.ro

int Lua_Set_Damage_Points(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Damage_Points(Commands->Find_Object(luaL_checknumber(L, 1)),luaL_checknumber(L, 2));
	return 1;
}

int Lua_Set_Death_Points(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Death_Points(Commands->Find_Object(luaL_checknumber(L, 1)),luaL_checknumber(L, 2));
	return 1;
}

int Lua_Get_Damage_Points(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Get_Damage_Points(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Get_Death_Points(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Get_Death_Points(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Damage_Objects_Half(lua_State *L)
{
	Damage_Objects_Half();
	return 1;
}

int Lua_Kill_Occupants(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Kill_Occupants(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Damage_All_Objects_Area(lua_State *L)
{
	if(lua_gettop(L) < 6) return 0;
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 1);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());
	Damage_All_Objects_Area(luaL_checknumber(L, 1), luaL_checkstring(L, 2), pos, luaL_checknumber(L, 4), Commands->Find_Object(luaL_checknumber(L, 5)), Commands->Find_Object(luaL_checknumber(L, 6)));
	return 1;
}

int Lua_Damage_All_Buildings_By_Team(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Damage_All_Buildings_By_Team(luaL_checknumber(L, 1), luaL_checknumber(L, 2), luaL_checkstring(L, 3), Commands->Find_Object(luaL_checknumber(L, 4)));
	return 1;
}

int Lua_Damage_All_Vehicles_Area(lua_State *L)
{
	if(lua_gettop(L) < 6) return 0;
	LUA_Vector3 *a = LuaVector3::GetInstance(L, 1);
	Vector3 pos = Vector3(a->X(), a->Y(), a->Z());
	Damage_All_Vehicles_Area(luaL_checknumber(L, 1), luaL_checkstring(L, 2), pos, luaL_checknumber(L, 4), Commands->Find_Object(luaL_checknumber(L, 5)), Commands->Find_Object(luaL_checknumber(L, 6)));
	return 1;
}

int Lua_Set_Info_Texture(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Info_Texture(Commands->Find_Object(luaL_checknumber(L, 1)), luaL_checkstring(L, 2));
	return 1;
}

int Lua_Clear_Info_Texture(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Clear_Info_Texture(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Set_Naval_Vehicle_Limit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_Naval_Vehicle_Limit(luaL_checknumber(L, 1));
	return 1;
}

int Lua_Get_Naval_Vehicle_Limit(lua_State *L)
{
	lua_pushnumber(L, Get_Naval_Vehicle_Limit());
	return 1;
}

int Lua_Send_Message_Player(lua_State *L)
{
	if(lua_gettop(L) < 5) return 0;
	Send_Message_Player(Commands->Find_Object(luaL_checknumber(L, 1)), luaL_checknumber(L, 2), luaL_checknumber(L, 3), luaL_checknumber(L, 4), luaL_checkstring(L, 5));
	return 1;
}
/*
int Lua_Set_Wireframe_Mode(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_Wireframe_Mode(luaL_checknumber(L, 1));
	return 1;
}*/

int Lua_Load_New_HUD_INI(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Load_New_HUD_INI(Commands->Find_Object(luaL_checknumber(L, 1)), luaL_checkstring(L, 2));
	return 1;
}

int Lua_Change_Radar_Map(lua_State *L)
{
	if(lua_gettop(L) < 4) return 0;
	Change_Radar_Map(luaL_checknumber(L, 1), luaL_checknumber(L, 2), luaL_checknumber(L, 3), luaL_checkstring(L, 4));
	return 1;
}

int Lua_Set_Currently_Building(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Set_Currently_Building(lua_toboolean(L, 1), lua_toboolean(L, 2));
	return 1;
}

int Lua_Is_Currently_Building(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	 if (Is_Currently_Building(luaL_checknumber(L, 1))) {
		 lua_pushboolean(L, true);
	 }
	 else {
		 lua_pushboolean(L, false);
	 }

	return 1;
}

int Lua_Set_Fog_Color(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Set_Fog_Color(luaL_checknumber(L, 1), luaL_checknumber(L, 2), luaL_checknumber(L, 3));
	return 1;
}

int Lua_Display_Security_Dialog(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Display_Security_Dialog(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Do_Objectives_Dlg(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Do_Objectives_Dlg(Commands->Find_Object(luaL_checknumber(L, 1)), luaL_checkstring(L, 2));
	return 1;
}

int Lua_Set_Player_Limit(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_Player_Limit(luaL_checknumber(L, 1));
	return 1;
}

int Lua_Get_Player_Limit(lua_State *L)
{
	lua_pushnumber(L, Get_Player_Limit());
	return 1;
}

int Lua_Set_GDI_Soldier_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_GDI_Soldier_Name(luaL_checkstring(L, 1));
	return 1;
}

int Lua_Set_Nod_Soldier_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_Nod_Soldier_Name(luaL_checkstring(L, 1));
	return 1;
}

int Lua_Set_Moon_Is_Earth(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Set_Moon_Is_Earth(lua_checkboolean(L, 1));
	return 1;
}

int Lua_Get_Revision(lua_State *L)
{
	lua_pushnumber(L, Get_Revision(0));
	return 1;
}

int Lua_Can_Team_Build_Vehicle(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Can_Team_Build_Vehicle(luaL_checknumber(L, 1));
	return 1;
}

int Lua_Find_Naval_Factory(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	Find_Naval_Factory(luaL_checknumber(L, 1));
	return 1;
}
/*
int Lua_Is_Night_Map(lua_State *L)
{
	lua_pushboolean(L, Is_Night_Map());
	return 1;
}*/

int Lua_Vehicle_Preset_Is_Air(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Vehicle_Preset_Is_Air(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Vehicle_Preset_Is_Naval(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushboolean(L, Vehicle_Preset_Is_Naval(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Hide_Preset_By_Name(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Hide_Preset_By_Name(luaL_checknumber(L, 1), luaL_checkstring(L, 2), lua_checkboolean(L, 3));
	return 1;
}

int Lua_Busy_Preset_By_Name(lua_State *L)
{
	if(lua_gettop(L) < 3) return 0;
	Busy_Preset_By_Name(luaL_checknumber(L, 1), luaL_checkstring(L, 2), lua_checkboolean(L, 3));
	return 1;
}

int Lua_Attach_Script_Occupants(lua_State *L)
{
	if(lua_gettop(L) < 2) return 0;
	Attach_Script_Occupants(Commands->Find_Object(luaL_checknumber(L, 1)), luaL_checkstring(L, 2), luaL_checkstring(L, 3));
	return 1;
}

int Lua_Destroy_Lua_Thread(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	int id=luaL_checknumber(L, 1);
	if (!LuaManager::Lua[id]) return 0;
	lua_close(LuaManager::Lua[id]);
	LuaManager::Lua[id] = 0;
	//LuaManager::LuaNames[id]= 0;
	return 1;
}

int Lua_Create_Lua_Thread(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	LuaManager::LoadLua(luaL_checkstring(L, 1));
	return 1;
}

int Lua_Restart_Lua(lua_State *L)
{
	LuaManager::Reload_Flag=true;
	return 1;
}

int Lua_Get_Lua_Thread_By_Name(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	const char *whatfind=luaL_checkstring(L, 1);
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		if(LuaManager::Lua[lua_it])
		{
			/*if (strcmp(LuaManager::LuaNames[lua_it],whatfind) == 0) {
			 lua_pushnumber(L, lua_it);
			}*/
		}
	 }
	return 1;
}

// New Revision: 16.09.2013->

int Lua_Get_Current_Map_Index(lua_State *L)
{
	lua_pushnumber(L, Get_Current_Map_Index());
	return 1;
}

int Lua_Set_Map(lua_State *L)
{ 
	if(lua_gettop(L) < 2) return 0;

	if (strcmp(luaL_checkstring(L, 1), "NULL") == 0) {
	 Set_Map("", luaL_checknumber(L, 2));
	} 
	else {
	 Set_Map(luaL_checkstring(L, 1), luaL_checknumber(L, 2));
	}

	return 1;
}

int Lua_Get_Client_Revision(lua_State *L)
{
	if(lua_gettop(L) < 1) return 0;
	lua_pushnumber(L, Get_Client_Revision(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Set_Is_Visible(lua_State *L)
{
	if (lua_gettop(L) < 1) return 0;
	Commands->Set_Is_Visible(Commands->Find_Object(luaL_checknumber(L, 1)), lua_checkboolean(L, 2));
	return 1;
}

int Lua_Clear_Weapons(lua_State *L)
{
	if (lua_gettop(L) < 1) return 0;
	Commands->Clear_Weapons(Commands->Find_Object(luaL_checknumber(L, 1)));
	return 1;
}

int Lua_Set_Is_Rendered(lua_State *L)
{
	if (lua_gettop(L) < 2) return 0;
	Commands->Set_Is_Rendered(Commands->Find_Object(luaL_checknumber(L, 1)), lua_toboolean(L, 2));
	return 1;
}

int Lua_Get_Vehicle_Lock_Owner(lua_State *L)
{
	if (lua_gettop(L) < 1) return 0;
	GameObject *obj = Commands->Find_Object(luaL_checknumber(L, 1));
	if (obj->As_VehicleGameObj())
	{
		lua_pushnumber(L, Commands->Get_ID(obj->As_VehicleGameObj()->Get_Lock_Owner()));
	}
	return 1;
}




void AddFunctions(lua_State *L)
{
	// 2.01
	lua_register(L, "Get_Vehicle_Lock_Owner", Lua_Get_Vehicle_Lock_Owner);
	lua_register(L, "Set_Is_Powerup_Persistant", Lua_Set_Is_Powerup_Persistant);
	
	// 2
	lua_register(L, "Set_Is_Rendered", Lua_Set_Is_Rendered);
	lua_register(L, "Clear_Weapons", Lua_Clear_Weapons);
	lua_register(L, "Set_Is_Visible", Lua_Set_Is_Visible); // WORK
	lua_register(L, "Set_Map", Lua_Set_Map); // WORK
	lua_register(L, "Get_Current_Map_Index", Lua_Get_Current_Map_Index); // WORK
	lua_register(L, "Get_Client_Revision", Lua_Get_Client_Revision); // WORK
	lua_register(L, "Set_Damage_Points", Lua_Set_Damage_Points);
	lua_register(L, "Set_Death_Points", Lua_Set_Death_Points);
	lua_register(L, "Damage_Objects_Half", Lua_Damage_Objects_Half);
	lua_register(L, "Get_Death_Points", Lua_Get_Death_Points);
	lua_register(L, "Get_Damage_Points", Lua_Get_Damage_Points);
	lua_register(L, "Kill_Occupants", Lua_Kill_Occupants);
	lua_register(L, "Damage_All_Objects_Area", Lua_Damage_All_Objects_Area);
	lua_register(L, "Damage_All_Vehicles_Area", Lua_Damage_All_Vehicles_Area);
    lua_register(L, "Damage_All_Buildings_By_Team", Lua_Damage_All_Buildings_By_Team);
	lua_register(L, "Set_Info_Texture", Lua_Set_Info_Texture);
	lua_register(L, "Clear_Info_Texture", Lua_Clear_Info_Texture);
	lua_register(L, "Set_Naval_Vehicle_Limit", Lua_Set_Naval_Vehicle_Limit);
    lua_register(L, "Get_Naval_Vehicle_Limit", Lua_Get_Naval_Vehicle_Limit);
	lua_register(L, "Send_Message_Player", Lua_Send_Message_Player);
	lua_register(L, "Load_New_HUD_INI", Lua_Load_New_HUD_INI);
	lua_register(L, "Change_Radar_Map", Lua_Change_Radar_Map);
	lua_register(L, "Set_Currently_Building", Lua_Set_Currently_Building);
	lua_register(L, "Is_Currently_Building", Lua_Is_Currently_Building);
	lua_register(L, "Set_Fog_Color", Lua_Set_Fog_Color);
	lua_register(L, "Display_Security_Dialog", Lua_Display_Security_Dialog);
	lua_register(L, "Do_Objectives_Dlg", Lua_Do_Objectives_Dlg);
	lua_register(L, "Set_Player_Limit", Lua_Set_Player_Limit);
	lua_register(L, "Get_Player_Limit", Lua_Get_Player_Limit);
	lua_register(L, "Set_GDI_Soldier_Name", Lua_Set_GDI_Soldier_Name);
	lua_register(L, "Set_Nod_Soldier_Name", Lua_Set_Nod_Soldier_Name);
	lua_register(L, "Set_Moon_Is_Earth", Lua_Set_Moon_Is_Earth);
	lua_register(L, "Get_Revision", Lua_Get_Revision);
	lua_register(L, "Can_Team_Build_Vehicle", Lua_Can_Team_Build_Vehicle);
	lua_register(L, "Find_Naval_Factory", Lua_Find_Naval_Factory);
	lua_register(L, "Vehicle_Preset_Is_Air", Lua_Vehicle_Preset_Is_Air);
	lua_register(L, "Vehicle_Preset_Is_Naval", Lua_Vehicle_Preset_Is_Naval);
	lua_register(L, "Busy_Preset_By_Name", Lua_Busy_Preset_By_Name);
	lua_register(L, "Hide_Preset_By_Name", Lua_Hide_Preset_By_Name);
	lua_register(L, "Attach_Script_Occupants", Lua_Attach_Script_Occupants);
	lua_register(L, "Create_Lua_Thread", Lua_Create_Lua_Thread);
	lua_register(L, "Restart_Lua", Lua_Restart_Lua); // WORK
	lua_register(L, "Get_Lua_Thread_By_Name", Lua_Get_Lua_Thread_By_Name);
	lua_register(L, "Set_Kills", Lua_Set_Kills);

	// older
	lua_register(L, "Get_All_Buildings", Lua_Get_All_Buildings2); // WORK
	lua_register(L, "Get_All_Vehicles", Lua_Get_All_Vehicles); // WORK
	lua_register(L, "Get_All_Players", Lua_Get_All_Players); // WORK
    lua_register(L, "HideTexture", Lua_HideTexture); // WORK
	lua_register(L, "ShowTexture", Lua_ShowTexture); // WORK
    lua_register(L, "OColision", Lua_OColision); // WORK
	lua_register(L, "Is_WOL_User", Lua_Is_WOL_User); // WORK
    lua_register(L, "Hide_Object_Player", Lua_Hide_Object_Player); // work
	lua_register(L, "Warp_Soldier", Lua_Warp_Player); // Neijwiert and sla.ro (not work well)
	lua_register(L, "Set_Max_Speed", Lua_Set_Max_Speed); // XiiXeno (work)
	lua_register(L, "Get_Max_Speed", Lua_Get_Max_Speed); // XiiXeno (work)
	lua_register(L, "Is_Vehicle_Owner", Lua_Is_Vehicle_Owner); // work
	lua_register(L, "Revive_Building", Lua_Revive_Building); // work
	lua_register(L, "Memory_Read", Lua_Memory_Read); // work
	lua_register(L, "Memory_Write", Lua_Memory_Write); // work
	//lua_register(L, "Set_cPlayer", Lua_Set_cPlayer); // work
	lua_register(L, "Create_Building", Lua_Create_Building);  // work
	lua_register(L, "Restore_Building", Lua_Restore_Building); // work
	lua_register(L, "Set_Game_Title", Lua_Set_Game_Title); // work
	lua_register(L, "Damage_Occupants", Lua_Damage_Occupants); // work
	//lua_register(L, "Is_Night_Map", Lua_Is_Night_Map); // work
	lua_register(L, "Add_TTDamageHook", Lua_Add_TTDamageHook); // work
	lua_register(L, "Set_Animation", Lua_Set_Animation); // work
	lua_register(L, "Enable_Radar_Player", Lua_Enable_Radar_Player); // work
	lua_register(L, "Get_Distance", Lua_Get_Distance); // work
	lua_register(L, "Get_File_List", Lua_Get_File_List); // work
	lua_register(L, "Reload_Lua", Lua_Reload_Lua); // work
	lua_register(L, "Resolve_IP", Lua_DNS_IP_To_Host); // work
	lua_register(L, "Create_2D_Sound", Lua_Create_2D_Sound); // work
	lua_register(L, "Create_2D_Sound_Player", Lua_Create_2D_Sound_Player); // work
	lua_register(L, "Select_Weapon", Lua_Select_Weapon); // work
	lua_register(L, "Send_Message", Lua_Send_Message); // work
	lua_register(L, "Create_3D_WAV_Sound_At_Bone", Lua_Create_3D_WAV_Sound_At_Bone); // work
	lua_register(L, "Change_Time_Limit", Lua_Change_Time_Limit); // work
	lua_register(L, "Change_Time_Remaining", Lua_Change_Time_Remaining); // work
	lua_register(L, "Stop_Background_Music_Player", Lua_Stop_Background_Music_Player); // work
	lua_register(L, "Get_Build_Time_Multiplier", Lua_Get_Build_Time_Multiplier); // work
	lua_register(L, "Set_Screen_Fade_Color_Player", Lua_Set_Screen_Fade_Color_Player); // work
	lua_register(L, "Set_Screen_Fade_Opacity_Player", Lua_Set_Screen_Fade_Opacity_Player); // work
	lua_register(L, "Force_Camera_Look_Player", Lua_Force_Camera_Look_Player); // work
	lua_register(L, "Get_Vehicle_Limit", Lua_Get_Vehicle_Limit); // work
	lua_register(L, "Set_Vehicle_Limit", Lua_Set_Vehicle_Limit); // work
	lua_register(L, "Get_Air_Vehicle_Limit", Lua_Get_Air_Vehicle_Limit); // work
	lua_register(L, "Set_Air_Vehicle_Limit", Lua_Set_Air_Vehicle_Limit); // work
	lua_register(L, "Is_Crate", Lua_Is_Crate); //work
	lua_register(L, "Add_RadioHook", Lua_Add_RadioHook); //work
	lua_register(L, "Shake_Camera", Lua_Shake_Camera); //work
	lua_register(L, "Play_Building_Announcement", Lua_Play_Building_Announcement); // work
	lua_register(L, "Set_War_Blitz", Lua_Set_War_Blitz);  //work
	lua_register(L, "Set_Fog_Range", Lua_Set_Fog_Range);  //work
	lua_register(L, "Set_Fog_Enable", Lua_Set_Fog_Enable);  // work
	lua_register(L, "Create_Explosion_At_Bone", Lua_Create_Explosion_At_Bone); //work 
	lua_register(L, "Create_Explosion", Lua_Create_Explosion); //work
	lua_register(L, "Get_Client_Serial_Hash", Lua_Get_Client_Serial_Hash); //work
	lua_register(L, "Get_Weapon_Style", Lua_Get_Weapon_Style); //work
	lua_register(L, "Remove_Weapon", Lua_Remove_Weapon); //work
	lua_register(L, "Get_Preset_Name_By_Preset_ID", Lua_Get_Preset_Name_By_Preset_ID);  // works
	lua_register(L, "Remove_All_Weapons", Lua_Remove_All_Weapons); // work
	lua_register(L, "Add_Console_Hook", Lua_Add_Console_Hook); // work
    lua_register(L, "Set_Background_Music", Lua_Set_Background_Music); 
	lua_register(L, "HUD_Pokable", Lua_HUD_Pokable);  // work
	lua_register(L, "Get_Map_By_Number", Lua_Get_Map_By_Number); // work
	lua_register(L, "Spectate", Lua_Specate); // work
	lua_register(L, "Get_Background_Music", Lua_Get_Background_Music); // works
	lua_register(L, "Get_C4_Count", Lua_Get_C4_Count); // works
	lua_register(L, "Get_Mine_Limit", Lua_Get_Mine_Limit); // works
	lua_register(L, "Kill_Player", Lua_Kill_Player); // works
	lua_register(L, "Freeze_Player", Lua_Freeze_Player); // works
	lua_register(L, "Get_BW_Player", Lua_Get_BW_Player); // works
	lua_register(L, "Get_Vehicles_Limit", Lua_Get_Vehicles_Limit);
	lua_register(L, "Get_Player_Version", Lua_Get_Player_Version); // work
    lua_register(L, "Kick_Player", Lua_Kick_Player); // works
	
	// LuaV5
	lua_register(L, "Set_Health", Lua_Set_Health); // not work idk why
	lua_register(L, "Set_Shield_Strength", Lua_Set_Shield_Strength); // not work idk why
	lua_register(L, "Set_Shield_Type", Lua_Set_Shield_Type); // works
	lua_register(L, "FDSMessage", Lua_FDSMessage); // works
	lua_register(L, "Get_All_Objects", Lua_Get_All_Buildings); // works
	lua_register(L, "Enable_Stealth", Lua_Enable_Stealth); // works
	lua_register(L, "Get_Rotation", Lua_Get_Rotation); // works
    lua_register(L, "Is_A_Star", Lua_Is_A_Star); // works
	lua_register(L, "The_Game", Lua_The_Game); // works
	//lua_register(L, "cPlayer", Lua_cPlayer); // works
	lua_register(L, "LongToIP", Lua_LongToIP); // works
	lua_register(L, "IPToLong", Lua_IPToLong); // works
	lua_register(L, "GetTimeRemaining", Lua_GetTimeRemaining); // works
	lua_register(L, "Get_Health", Lua_Get_Health); // works
	lua_register(L, "Get_Max_Health", Lua_Get_Max_Health); // works
	lua_register(L, "Get_Shield_Strength", Lua_Get_Shield_Strength); // works
	lua_register(L, "Get_Max_Shield_Strength", Lua_Get_Max_Shield_Strength); // works
	lua_register(L, "Display_GDI_Player_Terminal_Player", Lua_Display_GDI_Player_Terminal_Player); // works
	lua_register(L, "Display_Nod_Player_Terminal_Player", Lua_Display_Nod_Player_Terminal_Player); // works

	// Lua V4
	lua_register(L, "Console_Input",Lua_Console_Input);
	lua_register(L, "Console_Output",Lua_Console_Output);
	lua_register(L, "Get_Definition_Name",Lua_Get_Definition_Name);
	lua_register(L, "Get_Definition_ID",Lua_Get_Definition_ID);
	lua_register(L, "Get_Definition_Class_ID",Lua_Get_Definition_Class_ID);
	lua_register(L, "Is_Valid_Preset_ID",Lua_Is_Valid_Preset_ID);
	lua_register(L, "Is_Valid_Preset",Lua_Is_Valid_Preset);
	lua_register(L, "Set_Max_Health",Lua_Set_Max_Health);
	lua_register(L, "Set_Max_Shield_Strength",Lua_Set_Max_Shield_Strength);
	lua_register(L, "Get_Shield_Type",Lua_Get_Shield_Type);
	lua_register(L, "Get_Skin",Lua_Get_Skin);
	lua_register(L, "Set_Skin",Lua_Set_Skin);
	lua_register(L, "Power_Base",Lua_Power_Base); 
	lua_register(L, "Set_Can_Generate_Soldiers",Lua_Set_Can_Generate_Soldiers);
	lua_register(L, "Set_Can_Generate_Vehicles",Lua_Set_Can_Generate_Vehicles); 
	lua_register(L, "Destroy_Base",Lua_Destroy_Base);
	lua_register(L, "Beacon_Destroyed_Base",Lua_Beacon_Destroyed_Base);
	lua_register(L, "Enable_Base_Radar",Lua_Enable_Base_Radar);
	lua_register(L, "Is_Harvester",Lua_Is_Harvester);
	lua_register(L, "Is_Radar_Enabled",Lua_Is_Radar_Enabled); 
	lua_register(L, "Building_Type",Lua_Building_Type); 
	lua_register(L, "Is_Building_Dead",Lua_Is_Building_Dead); 
	lua_register(L, "Find_Building",Lua_Find_Building);
	lua_register(L, "Find_Base_Defense",Lua_Find_Base_Defense);
	lua_register(L, "Is_Map_Flying",Lua_Is_Map_Flying); 
	lua_register(L, "Find_Harvester",Lua_Find_Harvester);
	lua_register(L, "Is_Base_Powered",Lua_Is_Base_Powered);
	lua_register(L, "Can_Generate_Vehicles",Lua_Can_Generate_Vehicles);
	lua_register(L, "Can_Generate_Soliders",Lua_Can_Generate_Soliders);
	lua_register(L, "Is_A_Building",Lua_Is_A_Building);
	lua_register(L, "Get_Building_Count_Team",Lua_Get_Building_Count_Team);
	lua_register(L, "Find_Building_By_Team",Lua_Find_Building_By_Team);
	lua_register(L, "Find_Building_By_Name",Lua_Find_Building_By_Name);
	lua_register(L, "Find_Power_Plant",Lua_Find_Power_Plant);
	lua_register(L, "Find_Refinery",Lua_Find_Refinery);
	lua_register(L, "Find_Repair_Bay",Lua_Find_Repair_Bay);
	lua_register(L, "Find_Soldier_Factory",Lua_Find_Soldier_Factory);
	lua_register(L, "Find_Airstrip",Lua_Find_Airstrip);
	lua_register(L, "Find_War_Factory",Lua_Find_War_Factory);
	lua_register(L, "Find_Vehicle_Factory",Lua_Find_Vehicle_Factory);
	lua_register(L, "Find_Com_Center",Lua_Find_Com_Center);
	lua_register(L, "Is_Gameplay_Permitted",Lua_Is_Gameplay_Permitted);
	lua_register(L, "Is_Dedicated",Lua_Is_Dedicated); 
	lua_register(L, "Get_Current_Game_Mode",Lua_Get_Current_Game_Mode);
	lua_register(L, "Get_Harvester_Preset_ID",Lua_Get_Harvester_Preset_ID);
	lua_register(L, "Is_Harvester_Preset",Lua_Is_Harvester_Preset);
	lua_register(L, "Destroy_Connection",Lua_Destroy_Connection);
	lua_register(L, "Get_IP_Address",Lua_Get_IP_Address);
	lua_register(L, "Get_IP_Port",Lua_Get_IP_Port);
	lua_register(L, "Get_Bandwidth",Lua_Get_Bandwidth);
	lua_register(L, "Get_Ping",Lua_Get_Ping);
	lua_register(L, "Get_Kbits",Lua_Get_Kbits);
	lua_register(L, "Get_Object_Type",Lua_Get_Object_Type);
	lua_register(L, "Set_Object_Type",Lua_Set_Object_Type);
	lua_register(L, "Is_Building",Lua_Is_Building);
	lua_register(L, "Is_Soldier",Lua_Is_Soldier);
	lua_register(L, "Is_Vehicle",Lua_Is_Vehicle);
	//lua_register(L, "Is_Cinematic",Lua_Is_Cinematic);
	lua_register(L, "Is_ScriptZone",Lua_Is_ScriptZone);
	lua_register(L, "Is_Powerup",Lua_Is_Powerup);
	lua_register(L, "Is_C4",Lua_Is_C4);
	lua_register(L, "Is_Beacon",Lua_Is_Beacon);
	lua_register(L, "Is_Armed",Lua_Is_Armed);
	//lua_register(L, "Is_Simple",Lua_Is_Simple);
	lua_register(L, "Is_PowerPlant",Lua_Is_PowerPlant);
	lua_register(L, "Is_SoldierFactory",Lua_Is_SoldierFactory);
	lua_register(L, "Is_VehicleFactory",Lua_Is_VehicleFactory);
	lua_register(L, "Is_Airstrip",Lua_Is_Airstrip);
	lua_register(L, "Is_WarFactory",Lua_Is_WarFactory);
	lua_register(L, "Is_Refinery",Lua_Is_Refinery);
	lua_register(L, "Is_ComCenter",Lua_Is_ComCenter);
	lua_register(L, "Is_RepairBay",Lua_Is_RepairBay);
	//lua_register(L, "Is_Scriptable",Lua_Is_Scriptable);
	//lua_register(L, "Get_Building_Type",Lua_Get_Building_Type);
	lua_register(L, "Get_Object_Count",Lua_Get_Object_Count);
	lua_register(L, "Find_Random_Preset",Lua_Find_Random_Preset);
	lua_register(L, "Send_Custom_To_Team_Buildings",Lua_Send_Custom_To_Team_Buildings);
	lua_register(L, "Send_Custom_To_Team_Preset",Lua_Send_Custom_To_Team_Preset);
	lua_register(L, "Send_Custom_All_Objects",Lua_Send_Custom_All_Objects);
	lua_register(L, "Send_Custom_Event_To_Object",Lua_Send_Custom_Event_To_Object);
	lua_register(L, "Get_Is_Powerup_Persistant",Lua_Get_Is_Powerup_Persistant);
	lua_register(L, "Get_Powerup_Always_Allow_Grant",Lua_Get_Powerup_Always_Allow_Grant);
	lua_register(L, "Set_Powerup_Always_Allow_Grant",Lua_Set_Powerup_Always_Allow_Grant);
	lua_register(L, "Get_Powerup_Grant_Sound",Lua_Get_Powerup_Grant_Sound);
	lua_register(L, "Grant_Powerup",Lua_Grant_Powerup);
	lua_register(L, "Get_Vehicle",Lua_Get_Vehicle);
	lua_register(L, "Grant_Refill",Lua_Grant_Refill);
	lua_register(L, "Change_Character",Lua_Change_Character);
	lua_register(L, "Create_Vehicle",Lua_Create_Vehicle);
	lua_register(L, "Toggle_Fly_Mode",Lua_Toggle_Fly_Mode);
	lua_register(L, "Get_Vehicle_Occupant_Count",Lua_Get_Vehicle_Occupant_Count);
	lua_register(L, "Get_Vehicle_Occupant",Lua_Get_Vehicle_Occupant);
	lua_register(L, "Get_Vehicle_Driver",Lua_Get_Vehicle_Driver);
	lua_register(L, "Get_Vehicle_Gunner",Lua_Get_Vehicle_Gunner);
	lua_register(L, "Force_Occupant_Exit",Lua_Force_Occupant_Exit);
	lua_register(L, "Force_Occupants_Exit",Lua_Force_Occupants_Exit);
	lua_register(L, "Get_Vehicle_Return",Lua_Get_Vehicle_Return);
	lua_register(L, "Is_Stealth",Lua_Is_Stealth);
	lua_register(L, "Get_Fly_Mode",Lua_Get_Fly_Mode);
	lua_register(L, "Get_Vehicle_Seat_Count",Lua_Get_Vehicle_Seat_Count);
	lua_register(L, "Soldier_Transition_Vehicle",Lua_Soldier_Transition_Vehicle);
	lua_register(L, "Get_Vehicle_Mode",Lua_Get_Vehicle_Mode);
	lua_register(L, "Get_Team_Vehicle_Count",Lua_Get_Team_Vehicle_Count);
	lua_register(L, "Get_Vehicle_Owner",Lua_Get_Vehicle_Owner);
	lua_register(L, "Force_Occupants_Exit_Team",Lua_Force_Occupants_Exit_Team);
	lua_register(L, "Get_Vehicle_Definition_Mode",Lua_Get_Vehicle_Definition_Mode);
	lua_register(L, "IsInsideZone",Lua_IsInsideZone);
	lua_register(L, "Get_Vehicle_Definition_Mode_By_ID",Lua_Get_Vehicle_Definition_Mode_By_ID);
	lua_register(L, "Get_Zone_Type",Lua_Get_Zone_Type);
	lua_register(L, "IsAvailableForPurchase",Lua_IsAvailableForPurchase);
	lua_register(L, "Get_Vehicle_Gunner_Pos",Lua_Get_Vehicle_Gunner_Pos);
//	lua_register(L, "Set_Vehicle_Is_Visible",Lua_Set_Vehicle_Is_Visible);
	lua_register(L, "Set_Vehicle_Gunner",Lua_Set_Vehicle_Gunner);
	lua_register(L, "Get_Model",Lua_Get_Model);
	lua_register(L, "Get_Animation_Frame",Lua_Get_Animation_Frame);
	lua_register(L, "Is_TrackedVehicle",Lua_Is_TrackedVehicle);
	lua_register(L, "Is_VTOLVehicle",Lua_Is_VTOLVehicle);
	lua_register(L, "Is_WheeledVehicle",Lua_Is_WheeledVehicle);
	lua_register(L, "Is_Motorcycle",Lua_Is_Motorcycle);
	/*lua_register(L, "Is_Door",Lua_Is_Door);
	lua_register(L, "Is_Elevator",Lua_Is_Elevator);
	lua_register(L, "Is_DamageableStaticPhys",Lua_Is_DamageableStaticPhys);
	lua_register(L, "Is_AccessablePhys",Lua_Is_AccessablePhys);
	lua_register(L, "Is_DecorationPhys",Lua_Is_DecorationPhys);
	lua_register(L, "Is_HumanPhys",Lua_Is_HumanPhys);
	lua_register(L, "Is_MotorVehicle",Lua_Is_MotorVehicle);
	lua_register(L, "Is_Phys3",Lua_Is_Phys3);
	lua_register(L, "Is_RigidBody",Lua_Is_RigidBody);
	lua_register(L, "Is_ShakeableStatricPhys",Lua_Is_ShakeableStatricPhys);
	lua_register(L, "Is_StaticAnimPhys",Lua_Is_StaticAnimPhys);
	lua_register(L, "Is_StaticPhys",Lua_Is_StaticPhys);
	lua_register(L, "Is_TimedDecorationPhys",Lua_Is_TimedDecorationPhys);
	lua_register(L, "Is_VehiclePhys",Lua_Is_VehiclePhys);
	lua_register(L, "Is_DynamicAnimPhys",Lua_Is_DynamicAnimPhys);
	lua_register(L, "Is_BuildingAggregate",Lua_Is_BuildingAggregate);
	lua_register(L, "Is_Projectile",Lua_Is_Projectile);
	lua_register(L, "Get_Physics",Lua_Get_Physics);*/
	lua_register(L, "Copy_Transform",Lua_Copy_Transform);
	lua_register(L, "Get_Mass",Lua_Get_Mass);
	lua_register(L, "Get_Htree_Name",Lua_Get_Htree_Name);
	lua_register(L, "Get_Sex",Lua_Get_Sex);
	lua_register(L, "Create_Effect_All_Of_Preset",Lua_Create_Effect_All_Of_Preset);
	lua_register(L, "Get_GameObj",Lua_Get_GameObj);
	lua_register(L, "Get_Player_ID",Lua_Get_Player_ID);
	lua_register(L, "Get_Player_Name",Lua_Get_Player_Name);
	lua_register(L, "Get_Player_Name_By_ID",Lua_Get_Player_Name_By_ID);
	lua_register(L, "Change_Team",Lua_Change_Team);
	lua_register(L, "Change_Team_By_ID",Lua_Change_Team_By_ID);
	lua_register(L, "Get_Player_Count",Lua_Get_Player_Count);
	lua_register(L, "Get_Team_Player_Count",Lua_Get_Team_Player_Count);
	lua_register(L, "Get_Team",Lua_Get_Team);
	lua_register(L, "Get_Rank",Lua_Get_Rank);
	lua_register(L, "Get_Kills",Lua_Get_Kills);
	lua_register(L, "Get_Deaths",Lua_Get_Deaths);
	lua_register(L, "Get_Score",Lua_Get_Score);
	lua_register(L, "Get_Money",Lua_Get_Money);
	lua_register(L, "Get_Kill_To_Death_Ratio",Lua_Get_Kill_To_Death_Ratio);
	lua_register(L, "Get_Part_Name",Lua_Get_Part_Name);
	lua_register(L, "Get_Part_Names",Lua_Get_Part_Names);
	lua_register(L, "Get_GameObj_By_Player_Name",Lua_Get_GameObj_By_Player_Name);
	lua_register(L, "Purchase_Item",Lua_Purchase_Item);
	lua_register(L, "Set_Ladder_Points",Lua_Set_Ladder_Points);
	lua_register(L, "Set_Rung",Lua_Set_Rung);
	lua_register(L, "Set_Money",Lua_Set_Money);
	lua_register(L, "Set_Score",Lua_Set_Score);
	lua_register(L, "Find_First_Player",Lua_Find_First_Player);
	lua_register(L, "Change_Player_Team",Lua_Change_Player_Team);
	lua_register(L, "Tally_Team_Size",Lua_Tally_Team_Size);
	lua_register(L, "Get_Team_Score",Lua_Get_Team_Score);
	lua_register(L, "Send_Custom_All_Players",Lua_Send_Custom_All_Players);
	lua_register(L, "Steal_Team_Credits",Lua_Steal_Team_Credits);
	lua_register(L, "Get_Team_Credits",Lua_Get_Team_Credits);
	lua_register(L, "Change_Team_2",Lua_Change_Team_2);
	lua_register(L, "Get_Player_Type",Lua_Get_Player_Type);
	lua_register(L, "Get_Team_Cost",Lua_Get_Team_Cost);
	lua_register(L, "Get_Cost",Lua_Get_Cost);
	lua_register(L, "Get_Team_Icon",Lua_Get_Team_Icon);
	lua_register(L, "Get_Icon",Lua_Get_Icon);
	lua_register(L, "Remove_Script",Lua_Remove_Script);
	lua_register(L, "Remove_All_Scripts",Lua_Remove_All_Scripts);
	lua_register(L, "Attach_Script_Preset",Lua_Attach_Script_Preset);
	lua_register(L, "Attach_Script_Type",Lua_Attach_Script_Type);
	lua_register(L, "Remove_Script_Preset",Lua_Remove_Script_Preset);
	lua_register(L, "Remove_Script_Type",Lua_Remove_Script_Type);
	lua_register(L, "Is_Script_Attached",Lua_Is_Script_Attached);
	lua_register(L, "Attach_Script_Once",Lua_Attach_Script_Once);
	lua_register(L, "Attach_Script_Preset_Once",Lua_Attach_Script_Preset_Once);
	lua_register(L, "Attach_Script_Type_Once",Lua_Attach_Script_Type_Once);
	lua_register(L, "Attach_Script_Building",Lua_Attach_Script_Building);
	lua_register(L, "Attach_Script_Is_Preset",Lua_Attach_Script_Is_Preset);
	lua_register(L, "Attach_Script_Is_Type",Lua_Attach_Script_Is_Type);
	lua_register(L, "Attach_Script_Player_Once",Lua_Attach_Script_Player_Once);
	lua_register(L, "Remove_Duplicate_Script",Lua_Remove_Duplicate_Script);
	lua_register(L, "Attach_Script_All_Buildings_Team",Lua_Attach_Script_All_Buildings_Team);
	lua_register(L, "Attach_Script_All_Turrets_Team",Lua_Attach_Script_All_Turrets_Team);
	lua_register(L, "Find_Building_With_Script",Lua_Find_Building_With_Script);
	lua_register(L, "Find_Object_With_Script",Lua_Find_Object_With_Script);
	lua_register(L, "Get_Translated_String",Lua_Get_Translated_String);
	lua_register(L, "Get_Translated_Preset_Name",Lua_Get_Translated_Preset_Name);
	lua_register(L, "Get_Translated_Weapon",Lua_Get_Translated_Weapon);
	lua_register(L, "Get_Current_Translated_Weapon",Lua_Get_Current_Translated_Weapon);
	lua_register(L, "Get_Team_Name",Lua_Get_Team_Name);
	lua_register(L, "Get_Vehicle_Name",Lua_Get_Vehicle_Name);
	lua_register(L, "Get_Translated_Definition_Name",Lua_Get_Translated_Definition_Name);
	lua_register(L, "Get_Current_Bullets",Lua_Get_Current_Bullets);
	lua_register(L, "Get_Current_Clip_Bullets",Lua_Get_Current_Clip_Bullets);
	lua_register(L, "Get_Current_Total_Bullets",Lua_Get_Current_Total_Bullets);
	lua_register(L, "Get_Total_Bullets",Lua_Get_Total_Bullets);
	lua_register(L, "Get_Clip_Bullets",Lua_Get_Clip_Bullets);
	lua_register(L, "Get_Bullets",Lua_Get_Bullets);
	lua_register(L, "Get_Current_Max_Bullets",Lua_Get_Current_Max_Bullets);
	lua_register(L, "Get_Current_Clip_Max_Bullets",Lua_Get_Current_Clip_Max_Bullets);
	lua_register(L, "Get_Current_Total_Max_Bullets",Lua_Get_Current_Total_Max_Bullets);
	lua_register(L, "Get_Max_Total_Bullets",Lua_Get_Max_Total_Bullets);
	lua_register(L, "Get_Max_Clip_Bullets",Lua_Get_Max_Clip_Bullets);
	lua_register(L, "Get_Max_Bullets",Lua_Get_Max_Bullets);
	lua_register(L, "Get_Position_Total_Bullets",Lua_Get_Position_Total_Bullets);
	lua_register(L, "Get_Position_Bullets",Lua_Get_Position_Bullets);
	lua_register(L, "Get_Position_Clip_Bullets",Lua_Get_Position_Clip_Bullets);
	lua_register(L, "Get_Position_Total_Max_Bullets",Lua_Get_Position_Total_Max_Bullets);
	lua_register(L, "Get_Position_Max_Bullets",Lua_Get_Position_Max_Bullets);
	lua_register(L, "Get_Position_Clip_Max_Bullets",Lua_Get_Position_Clip_Max_Bullets);
	lua_register(L, "Set_Current_Bullets",Lua_Set_Current_Bullets);
	lua_register(L, "Set_Current_Clip_Bullets",Lua_Set_Current_Clip_Bullets);
	lua_register(L, "Set_Position_Bullets",Lua_Set_Position_Bullets);
	lua_register(L, "Set_Position_Clip_Bullets",Lua_Set_Position_Clip_Bullets);
	lua_register(L, "Set_Bullets",Lua_Set_Bullets);
	lua_register(L, "Set_Clip_Bullets",Lua_Set_Clip_Bullets);
	lua_register(L, "Get_Powerup_Weapon",Lua_Get_Powerup_Weapon);
	lua_register(L, "Get_Powerup_Weapon_By_Obj",Lua_Get_Powerup_Weapon_By_Obj);
	lua_register(L, "Get_Current_Weapon_Style",Lua_Get_Current_Weapon_Style);
	lua_register(L, "Disarm_Beacon",Lua_Disarm_Beacon);
	lua_register(L, "Disarm_Beacons",Lua_Disarm_Beacons);
	lua_register(L, "Disarm_C4",Lua_Disarm_C4);
	lua_register(L, "Get_Current_Weapon",Lua_Get_Current_Weapon);
	lua_register(L, "Get_Weapon_Count",Lua_Get_Weapon_Count);
	lua_register(L, "Get_Weapon",Lua_Get_Weapon);
	lua_register(L, "Has_Weapon",Lua_Has_Weapon);
	lua_register(L, "Find_Beacon",Lua_Find_Beacon);
	lua_register(L, "Get_C4_Count",Lua_Get_C4_Count);
	lua_register(L, "Get_Beacon_Count",Lua_Get_Beacon_Count);
	lua_register(L, "Get_Mine_Limit",Lua_Get_Mine_Limit);
	lua_register(L, "Get_C4_Mode",Lua_Get_C4_Mode);
	lua_register(L, "Get_C4_Planter",Lua_Get_C4_Planter);
	lua_register(L, "Get_C4_Attached",Lua_Get_C4_Attached);
	lua_register(L, "Get_Beacon_Planter",Lua_Get_Beacon_Planter);
	lua_register(L, "Create_Object",Lua_Create_Object);
	lua_register(L, "Destroy_Object",Lua_Destroy_Object);
	lua_register(L, "Get_Preset_Name",Lua_Get_Preset_Name);
	lua_register(L, "Get_Position",Lua_Get_Position);
	lua_register(L, "Enable_Vehicle_Transitions", Lua_Enable_Vehicle_Transitions);
	lua_register(L, "Set_Model",Lua_Set_Model);
	lua_register(L, "Goto_Location",Lua_Goto_Location);
	lua_register(L, "Goto_Object",Lua_Goto_Object);
	lua_register(L, "Disable_Physical_Collisions", Lua_Disable_Physical_Collisions);
	lua_register(L, "Enable_Collisions", Lua_Enable_Collisions);
	lua_register(L, "Random_Building", Lua_Random_Building);
	lua_register(L, "Get_Current_Map", Lua_Get_Current_Map);
	lua_register(L, "Get_Next_Map", Lua_Get_Next_Map);
	lua_register(L, "Set_Position", Lua_Set_Position);
	lua_register(L, "Apply_Damage", Lua_Apply_Damage);
	lua_register(L, "Get_Facing", Lua_Get_Facing);
	lua_register(L, "Set_Facing", Lua_Set_Facing);
	lua_register(L, "Set_Clouds", Lua_Set_Clouds);
	lua_register(L, "Set_Ash", Lua_Set_Ash);
	lua_register(L, "Set_Rain", Lua_Set_Rain);
	lua_register(L, "Set_Snow", Lua_Set_Snow);
	lua_register(L, "Set_Wind", Lua_Set_Wind);
	lua_register(L, "Attach_Script", Lua_Attach_Script);
	lua_register(L, "Create_Object_At_Bone", Lua_Create_Object_At_Bone);
	lua_register(L, "Attach_To_Object_Bone", Lua_Attach_To_Object_Bone);
	lua_register(L, "Display_Health_Bar", Lua_Display_Health_Bar);
	lua_register(L, "Create_Script_Zone", Lua_Create_Script_Zone);
	//lua_register(L, "Get_ID", Lua_Get_ID); // I'm sure this is replaced by last.
	lua_register(L, "tClock", Lua_tClock);
	lua_register(L, "Invoke", Lua_Invoke);
	lua_register(L, "Get_ID", Lua_Get_IDobj);

	LuaVector3::Register(L);
	LuaBox::Register(L);

  LuaScriptManager::RegisterScriptFunctions(L);

}