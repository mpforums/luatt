#include "General.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

#include <stdarg.h>
#include "plugin.h"

extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}

#include "Misc.h"
#include "LuaManager.h"




void LuaUnload() 
{
	LuaScriptManager::Cleanup();
	LuaSocketManager::Cleanup();
	LuaManager::Cleanup();
	//DestroyEngineMath();
}



void LuaLoad()
{
	
	LuaManager::Load();
	LuaScriptManager::Load();
	LuaSocketManager::Load();

	LuaManager::ShowBanner();
	char basepath[64];
	GetCurrentDirectory(64, basepath);
	//char scriptsdir[256], pluginsdir[256];
	char pluginsdir[256];
    //sprintf(scriptsdir, "%s\\LuaScripts\\*", basepath);
	sprintf(pluginsdir, "%s\\LuaPlugins\\*", basepath);

	char *scripts[256];
	char *plugins[256];
	memset((void *)scripts, 0, 256*4);
	memset((void *)plugins, 0, 256*4);
	//GetFiles(scriptsdir, scripts, 256);
	GetFiles(pluginsdir, plugins, 256);

/*	for(int i = 0; i < 256; i++)
	{
		char *x = scripts[i];
		if(!x)
		{
			continue;
		}
		char *path = new char[strlen(x)+strlen(basepath)+14];
	//	sprintf(path, "%s\\LuaScripts\\%s", basepath, x);
//		LuaScriptManager::AddScript(path);
		delete []path;
		delete []x;
	}
	*/

	for(int i = 0; i < 256; i++)
	{
		char *x = plugins[i];
		if(!x)
		{
			continue;
		}
		char *path = new char[strlen(x)+strlen(basepath)+14];
		sprintf(path, "%s\\LuaPlugins\\%s", basepath, x);
		LuaManager::LoadLua(path);
		delete []path;
		delete []x;
	}
	
	AddCharacterPurchaseMonHook(LuaManager::Purchase_Hook, "OnCharacterPurchase");
	AddVehiclePurchaseMonHook(LuaManager::Purchase_Hook, "OnVehiclePurchase");
	AddPowerupPurchaseMonHook(LuaManager::Purchase_Hook, "OnPowerupPurchase");
	
	
	ObjectCreateHookStruct *_och = new ObjectCreateHookStruct;
	_och->hook = LuaManager::Call_Object_Hook;
	_och->data = 0;
	AddObjectCreateHook(_och);
}

