#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}


#include "Misc.h"
#include "LuaManager.h"
#include "Lua_Class.h"
#include "time.h"

#define method(class, name) {#name, class::name}

const luaL_reg LuaVector3::methods[] = {
  method(LuaVector3, AssignX),
  method(LuaVector3,AssignY),
  method(LuaVector3, AssignZ),
  method(LuaVector3, GetX),
  method(LuaVector3, GetY),
  method(LuaVector3, GetZ),
   
  method(LuaVector3, X),
  method(LuaVector3, Y),
  method(LuaVector3, Z),
  {0,0}
};

const luaL_reg LuaBox::methods[] = {
  method(LuaBox, GetCenter),
  method(LuaBox, SetCenter),
  method(LuaBox, GetExtent),
  method(LuaBox, SetExtent),
  {0,0}
};

/*
const luaL_reg LuaSLNodeWrapper::methods[] = {
  method(LuaSLNodeWrapper, Next),
  method(LuaSLNodeWrapper, End),
  {0,0}
};
*/
