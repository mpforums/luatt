#pragma once

#define KB_Friendly_Base_Zone_Exited 113102015
#define KB_Enemy_Base_Zone_Exited 113102016
#define KB_Friendly_Base_Zone_Entered 113102017
#define KB_Enemy_Base_Zone_Entered 113102018
#define RT_Free_Aim 122124001
#define RT_Target_Owner 122124002
#define RT_Target_Veh 122124003
#define RT_Target_Players 122124004
#define PI 3.14159265358979323846

class KB_Base_Defence : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	int CurrentTarget;
	int Count;
	bool Firing;
	bool player;

	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj, GameObject *enemy);
	bool IsValidEnemy(GameObject* WeaponObj, GameObject* EnemyObj);
	void Action_Complete(GameObject *obj, int action_id, ActionCompleteReason complete_reason);
	void Timer_Expired(GameObject *obj, int number);
	void Attack(GameObject* Obj, GameObject* Enemy);
	void Custom(GameObject *obj, int type, int param, GameObject *sender);
};


class KB_Enemy_Zone_Generated : public ScriptImpClass
{
	int zonecount;
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int type, int param, GameObject *sender);
};

class KB_Friendly_Zone_Generated : public ScriptImpClass
{
	int zonecount;
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int type, int param, GameObject *sender);
};


class KB_Obelisk_Weapon : public ScriptImpClass {
	int EnemyID;
	int EffectID;
	bool Firing;
	bool Charged;

	void Created(GameObject* WeaponObj);
	void Destroyed(GameObject* WeaponObj);
	bool IsValidEnemy(GameObject* WeaponObj, GameObject* EnemyObj);

	void StartFiring(GameObject* WeaponObj);
	void StopFiring(GameObject* WeaponObj);
	void StartEffect(GameObject* WeaponObj);
	void StopEffect(GameObject* WeaponObj);
	void FireAt(GameObject* WeaponObj, GameObject* EnemyObj);
	void StopFireAt(GameObject* WeaponObj);
	void Timer_Expired(GameObject* WeaponObj, int Number);
	void Enemy_Seen(GameObject* WeaponObj, GameObject* EnemyObj);
public: void Register_Auto_Save_Variables();
};

class KB_AI_Rep_Turret : public ScriptImpClass {

	Vector3 PosTar;
	int TargetID;
	int CurrentCommand;
	bool Attacking;
	bool Moving;
	void Created(GameObject *obj);
	void Action_Complete(GameObject *obj, int action_id, ActionCompleteReason complete_reason);
	void Killed(GameObject *obj, GameObject *shooter);
	void Custom(GameObject *obj, int type, int param, GameObject *sender);
	void Timer_Expired(GameObject* obj, int Number);
	void TargetOwner(GameObject *obj, GameObject *target);
	void TargetVeh(GameObject *obj);
	void TargetPlayer(GameObject *obj);
	void FreeAim(GameObject *obj);
	GameObject *Find_Target(GameObject *obj, bool Veh);
};