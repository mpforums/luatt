#pragma warning(disable: 4127 4291)

void GetFiles(const char *Path, char **Data, int DataLength);

class TCPSocket
{
	SOCKET Socket;
	char Host[256];
	int Port;
	bool Connected;
	int Mode;

	TCPSocket(SOCKET &_Socket, int _Mode);

public:
	
	TCPSocket();
	TCPSocket(SOCKET &_Socket);
	TCPSocket(TCPSocket &_Socket);
	TCPSocket(const char *Host, int Port);
	TCPSocket(const char *IP, int Port, int Backlog);
	~TCPSocket();
	
	bool Client(const char *Host, int Port);
	bool Server(const char *IP, int Port, int Backlog);
	bool Accept(TCPSocket *NewConnection);

	bool Is_Connected();
	bool Is_DataAvaliable();
	bool Is_ConnectionWaiting();

	bool RecviveData(char *Data, int Length);
	bool RecviveData(char *Data, int *Length);
	bool SendData(const char *Data, int Length);

	bool Destroy();
};

class UDPSocket
{
	SOCKET Socket;
	char Host[256];
	int Port;
	bool Connected;
	int Mode;

	UDPSocket(SOCKET &_Socket, int _Mode);

public:
	
	bool Client(const char *Host, int Port);
	bool Server(const char *IP, int Port, int Backlog);
	bool Accept(UDPSocket *NewConnection);

	bool Is_Connected();
	bool Is_DataAvaliable();
	bool Is_ConnectionWaiting();

	bool RecviveData(char *Data, int Length);
	bool RecviveData(char *Data, int *Length);
	bool SendData(const char *Data, int Length);

	bool Destroy();
};

