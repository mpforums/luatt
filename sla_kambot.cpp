#include "General.h"

#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "AABoxClass.h"
#include "plugin.h"

#include "GameObjManager.h"
#include "HarvesterClass.h"
#include "PhysClass.h"
#include "MoveablePhysClass.h"
#include "PhysicsSceneClass.h"
#include "gmvehicle.h"

#include "sla_kambot.h"

void KB_Base_Defence::Created(GameObject *obj)
{
	player = false;
	objtype = Get_Object_Type(obj);
	Commands->Enable_Hibernation(obj, false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj, true);
	CurrentTarget = NULL;
	Commands->Action_Reset(obj, 1);

	GameObject *object;
	Vector3 pos, pos1, pos2, pos3;
	pos = Commands->Get_Position(obj);
	pos1.X = pos.X - 10;
	pos1.Y = pos.Y - 10;
	pos1.Z = pos.Z + 2;
	pos2.X = pos.X + 10;
	pos2.Y = pos.Y;
	pos2.Z = pos.Z + 2;
	pos3.X = pos.X + 10;
	pos3.Y = pos.Y - 10;
	pos3.Z = pos.Z + 2;
	Firing = false;
	object = Commands->Create_Object("Invisible_Object", pos1);
	if (object)
	{
		id1 = Commands->Get_ID(object);
	}
	object = Commands->Create_Object("Invisible_Object", pos2);
	if (object)
	{
		id2 = Commands->Get_ID(object);
	}
	object = Commands->Create_Object("Invisible_Object", pos3);
	if (object)
	{
		id3 = Commands->Get_ID(object);
	}
	Commands->Start_Timer(obj, this, 10, 1);
}

void KB_Base_Defence::Enemy_Seen(GameObject *obj, GameObject *enemy)
{
	GameObject *o = Get_Vehicle(enemy);
	if (o)
	{
		enemy = o;
	}
	ActionParamsStruct params;
	Vector3 pos, pos2;
	float maxattack, minattack;
	float attacktimer;
	pos = Commands->Get_Position(obj);
	pos2 = Commands->Get_Position(enemy);
	minattack = Get_Float_Parameter("MinAttackDistance");
	maxattack = Get_Float_Parameter("MaxAttackDistance");

	if (!player && !Firing && IsValidEnemy(obj, enemy))
	{
		params.Set_Basic(this, 100, 2);
		if (Get_Int_Parameter("AdjustAim") != 0 && enemy->As_SoldierGameObj())
		{
			params.Set_Attack(pos2, maxattack, 0.0, true);
		}
		else
		{
			params.Set_Attack(enemy, maxattack, 0.0, true);
		}
		params.AttackCheckBlocked = true;
		params.AttackForceFire = false;
		Commands->Action_Attack(obj, params);
		CurrentTarget = Commands->Get_ID(enemy);
		attacktimer = Get_Float_Parameter("AttackTimer");
		Commands->Start_Timer(obj, this, attacktimer, 2);
		Firing = true;
	}
}

bool KB_Base_Defence::IsValidEnemy(GameObject* WeaponObj, GameObject* EnemyObj) {
	if (!EnemyObj) return false;
	if (Commands->Get_Player_Type(EnemyObj) == Commands->Get_Player_Type(WeaponObj)) return false;
	if (Commands->Get_Health(EnemyObj) <= 0) return false;
	if (Is_Script_Attached(EnemyObj, "KB_Friendly_Zone_Generated")) return false;
	if (EnemyObj->As_VehicleGameObj() && Get_Vehicle_Driver(EnemyObj))
	{
		if (Is_Script_Attached(Get_Vehicle_Driver(EnemyObj), "KB_Friendly_Zone_Generated")) return false;
	}
	if (!WeaponObj->As_SmartGameObj()->Is_Obj_Visible(EnemyObj->As_PhysicalGameObj())) return false;
	if (EnemyObj->As_SmartGameObj() && !EnemyObj->As_SmartGameObj()->Is_Visible()) return false;

	float minattack = Get_Float_Parameter("MinAttackDistance");
	float maxattack = Get_Float_Parameter("MaxAttackDistance");

	Vector3 WeaponObjPos = Commands->Get_Position(WeaponObj);
	Vector3 WeaponObjPosXY = WeaponObjPos;
	WeaponObjPosXY.Z = 0;

	Vector3 EnemyObjPos = Commands->Get_Position(EnemyObj);
	Vector3 EnemyObjPosXY = EnemyObjPos;
	EnemyObjPosXY.Z = 0;

	float DistanceXY = Commands->Get_Distance(WeaponObjPosXY, EnemyObjPosXY);

	return DistanceXY > minattack && DistanceXY < maxattack;
}

void KB_Base_Defence::Custom(GameObject *obj, int type, int param, GameObject *sender)
{
	ActionParamsStruct params;
	if (type == CUSTOM_EVENT_VEHICLE_EXITED)
	{
		player = false;
		Commands->Start_Timer(obj, this, 0.5f, 2);
	}
	if (type == CUSTOM_EVENT_VEHICLE_ENTERED)
	{
		if (Commands->Get_Player_Type(sender) == (int)objtype)
		{
			player = true;
			params.Set_Basic(this, 100, 3);
			Commands->Action_Follow_Input(obj, params);
		}
		else
		{
			Commands->Start_Timer(obj, this, 0.5f, 3);
		}
	}
}

void KB_Base_Defence::Action_Complete(GameObject *obj, int action_id, ActionCompleteReason complete_reason)
{
	if (action_id == 2)
	{
		Firing = false;
		Commands->Action_Reset(obj, 100);
	}
	if (action_id == 3)
	{
		Set_Object_Type(obj, objtype);
		Commands->Action_Reset(obj, 100);
	}
}

void KB_Base_Defence::Timer_Expired(GameObject *obj, int number)
{
	ActionParamsStruct var;
	GameObject *object;
	int f;
	if (number == 1 && !Firing)
	{
		f = Commands->Get_Random_Int(0, 4);
		switch (f)
		{
		case 0:
			object = Commands->Find_Object(id1);
			if (object)
			{
				var.Set_Basic(this, 70, 1);
				var.Set_Attack(object, 0.0, 0.0, true);
				Commands->Action_Attack(obj, var);
			}
			break;
		case 1:
			object = Commands->Find_Object(id2);
			if (object)
			{
				var.Set_Basic(this, 70, 1);
				var.Set_Attack(object, 0.0, 0.0, true);
				Commands->Action_Attack(obj, var);
			}
			break;
		default:
			object = Commands->Find_Object(id3);
			if (object)
			{
				var.Set_Basic(this, 70, 1);
				var.Set_Attack(object, 0.0, 0.0, true);
				Commands->Action_Attack(obj, var);
			}
		}
		Commands->Start_Timer(obj, this, 10, 1);
	}
	if (number == 2)
	{
		Commands->Action_Reset(obj, 100);
		Firing = false;
		Count = 0;
		Set_Object_Type(obj, objtype);
	}
	if (number == 3)
	{
		Force_Occupant_Exit(obj, 0);
	}
}
ScriptRegistrant<KB_Base_Defence> KB_Base_Defence_Registrant("KB_Base_Defence", "MinAttackDistance=0.0:float,MaxAttackDistance=300.0:float,AttackTimer=1.0:float,AdjustAim=0:int");


void KB_Friendly_Zone_Generated::Created(GameObject *obj)
{
	zonecount = 1;
	StringClass Msg;
	Msg.Format("CMSGP %d 0,255,0 You've moved in range of your base, enemy defences will ignore you from here!", Get_Player_ID(obj));
	Console_Input(Msg);
}

void KB_Friendly_Zone_Generated::Custom(GameObject *obj, int type, int param, GameObject *sender)
{
	if (type == KB_Friendly_Base_Zone_Exited)
	{
		zonecount--;
		if (zonecount <= 0)
		{
			StringClass Msg;
			Msg.Format("CMSGP %d 255,0,0 You've moved out of your base", Get_Player_ID(obj));
			Console_Input(Msg);
			Remove_Script(obj, "KB_Friendly_Zone_Generated");
		}
	}
	if (type == KB_Friendly_Base_Zone_Entered)
	{
		zonecount++;
	}
}

ScriptRegistrant<KB_Friendly_Zone_Generated> KB_Friendly_Zone_Generated_Reg("KB_Friendly_Zone_Generated", "");


void KB_Enemy_Zone_Generated::Created(GameObject *obj)
{
	zonecount = 1;
	StringClass Msg;
	Msg.Format("CMSGP %d 255,0,0 You've moved in range of enemy base you can't build active defenses here!", Get_Player_ID(obj));
	Console_Input(Msg);
}

void KB_Enemy_Zone_Generated::Custom(GameObject *obj, int type, int param, GameObject *sender)
{
	if (type == KB_Enemy_Base_Zone_Exited)
	{
		zonecount--;
		if (zonecount <= 0)
		{
			StringClass Msg;
			Msg.Format("CMSGP %d 0,255,0 You've moved out of the enemy base", Get_Player_ID(obj));
			Console_Input(Msg);
			Remove_Script(obj, "KB_Enemy_Zone_Generated");
		}
	}
	if (type == KB_Enemy_Base_Zone_Entered)
	{
		zonecount++;
	}
}

ScriptRegistrant<KB_Enemy_Zone_Generated> KB_Enemy_Zone_Generated_Reg("KB_Enemy_Zone_Generated", "");



//
//
//KB obelisk fix to not fire in enemy base
//
//
//

void KB_Obelisk_Weapon::Created(GameObject* WeaponObj) {
	// Some settings
	Commands->Set_Is_Rendered(WeaponObj, false); // It's not visible
	Commands->Set_Player_Type(WeaponObj, 0); // We're a Nod Obelisk, GDI will not own an Obelisk
	Commands->Enable_Enemy_Seen(WeaponObj, true); // We want to get notified when we see an enemy
	Commands->Enable_Hibernation(WeaponObj, false); // Not controlled, or whatever?
	Commands->Innate_Enable(WeaponObj); // Dunno :)

	Firing = false;
	Charged = false;
	EnemyID = NULL;
	EffectID = NULL;
}

void KB_Obelisk_Weapon::Destroyed(GameObject* WeaponObj) {
	StopFiring(WeaponObj);
}

bool KB_Obelisk_Weapon::IsValidEnemy(GameObject* WeaponObj, GameObject* EnemyObj) {
	if (!EnemyObj) return false;
	// TODO: Make switch for obby kills neutral?
	// NEUTRAL:  if (Commands->Get_Player_Type(EnemyObj) == Commands->Get_Player_Type(WeaponObj)) return false;
	// GDI-ONLY: if (Commands->Get_Player_Type(EnemyObj) == 1) return false;
	if (Commands->Get_Player_Type(EnemyObj) != 1) return false;
	if (Commands->Get_Health(EnemyObj) <= 0) return false;
	if (!Commands->Is_Object_Visible(WeaponObj, EnemyObj)) return false;
	if (Is_Script_Attached(EnemyObj, "KB_Friendly_Zone_Generated")) return false;
	if (EnemyObj->As_VehicleGameObj())
	{
		if (Get_Vehicle_Driver(EnemyObj))
		{
			if (Is_Script_Attached(Get_Vehicle_Driver(EnemyObj), "KB_Friendly_Zone_Generated")) return false;
		}
		else if (!Is_Harvester(EnemyObj))
		{
			return false;
		}
	}

	Vector3 WeaponObjPos = Commands->Get_Position(WeaponObj);
	Vector3 WeaponObjPosXY = WeaponObjPos;
	WeaponObjPosXY.Z = 0;

	Vector3 EnemyObjPos = Commands->Get_Position(EnemyObj);
	Vector3 EnemyObjPosXY = EnemyObjPos;
	EnemyObjPosXY.Z = 0;

	float Distance = Commands->Get_Distance(WeaponObjPos, EnemyObjPos);
	float DistanceXY = Commands->Get_Distance(WeaponObjPosXY, EnemyObjPosXY);

	return DistanceXY > 15 && Distance < 150;
}

void KB_Obelisk_Weapon::StartFiring(GameObject* WeaponObj) {
	// Start effect
	StartEffect(WeaponObj);

	// Start charging
	Commands->Start_Timer(WeaponObj, this, Get_Float_Parameter("Delay") / 2, 1);
	Firing = true;
}

void KB_Obelisk_Weapon::StopFiring(GameObject* WeaponObj) {
	// Stop effect
	StopEffect(WeaponObj);

	// Stop firing
	Firing = false;
	Charged = false;
	EnemyID = NULL;
	EffectID = NULL;

	StopFireAt(WeaponObj);
}

void KB_Obelisk_Weapon::StartEffect(GameObject* WeaponObj) {
	Commands->Create_Sound("Obelisk_Warm_Up", Commands->Get_Position(WeaponObj), WeaponObj);

	GameObject* EffectObj = Commands->Create_Object("Obelisk Effect", Commands->Get_Position(WeaponObj));
	if (EffectObj) {
		EffectID = Commands->Get_ID(EffectObj);
	}
}
void KB_Obelisk_Weapon::StopEffect(GameObject* WeaponObj) {
	GameObject* EffectObj = Commands->Find_Object(EffectID);
	if (EffectObj) {
		Commands->Destroy_Object(EffectObj);
	}
}

void KB_Obelisk_Weapon::FireAt(GameObject* WeaponObj, GameObject* EnemyObj)
{
	ActionParamsStruct AttackParams;
	AttackParams.Set_Basic(this, 100, 0);
	AttackParams.Set_Attack(EnemyObj, 150.f, 0, true);
	Commands->Action_Attack(WeaponObj, AttackParams);

	Commands->Start_Timer(WeaponObj, this, 1, 3);
}

void KB_Obelisk_Weapon::StopFireAt(GameObject* WeaponObj)
{
	Commands->Action_Reset(WeaponObj, 100);
}

void KB_Obelisk_Weapon::Timer_Expired(GameObject* WeaponObj, int Number) {
	if (Number == 1) {
		// Charged; Set variable and try to fire
		Charged = true;

		GameObject* EnemyObj = Commands->Find_Object(EnemyID);
		if (IsValidEnemy(WeaponObj, EnemyObj)) {
			// Fire at the enemy
			FireAt(WeaponObj, EnemyObj);

			// Check effect in 4 seconds
			Commands->Start_Timer(WeaponObj, this, Get_Float_Parameter("Delay"), 1);
		}
		else {
			// Forget it
			StopFiring(WeaponObj);
		}
	}
	else if (Number == 2) {
		// Restart the effect
		StopEffect(WeaponObj);

		if (IsValidEnemy(WeaponObj, Commands->Find_Object(EnemyID))) {
			// Restart the effect
			StartEffect(WeaponObj);

			// Check again in 3.5 seconds
			Commands->Start_Timer(WeaponObj, this, Get_Float_Parameter("Delay"), 1);
		}
		else {
			// Forget it
			StopFiring(WeaponObj);
		}
	}
	else if (Number == 3)
	{
		StopFireAt(WeaponObj);
	}
}

void KB_Obelisk_Weapon::Enemy_Seen(GameObject* WeaponObj, GameObject* EnemyObj) {
	// Check for an living target which is in range
	if (!IsValidEnemy(WeaponObj, EnemyObj)) {
		return;
	}

	// If the previous enemy has gone, set the enemy to the currently detected enemy
	if (!IsValidEnemy(WeaponObj, Commands->Find_Object(EnemyID))) {
		EnemyID = Commands->Get_ID(EnemyObj);
	}

	if (Firing) {
		if (Charged) {
			// Only change the target if the previous target has gone
			if (!IsValidEnemy(WeaponObj, Commands->Find_Object(EnemyID))) {
				FireAt(WeaponObj, EnemyObj);
			}
		}
	}
	else {
		StartFiring(WeaponObj);
	}
}
void KB_Obelisk_Weapon::Register_Auto_Save_Variables() {
	Auto_Save_Variable(&EnemyID, 4, 1);
	Auto_Save_Variable(&EffectID, 4, 1);
	Auto_Save_Variable(&Firing, 1, 3);
	Auto_Save_Variable(&Charged, 1, 4);
}
ScriptRegistrant<KB_Obelisk_Weapon> KB_Obelisk_Weapon_Registrant("KB_Obelisk_Weapon", "delay=4.0f:float");

//
//
//KB_AI_Rep_Turret
//
//

void KB_AI_Rep_Turret::Created(GameObject *obj)
{
	Commands->Innate_Enable(obj);
	CurrentCommand = RT_Free_Aim;
	FreeAim(obj);
	Attacking = false;
	Moving = false;
	Commands->Start_Timer(obj, this, 1, 22);
	((SoldierGameObj*)obj)->Set_Max_Speed(((SoldierGameObj*)obj)->Get_Max_Speed()*2.0f);
}

void KB_AI_Rep_Turret::Action_Complete(GameObject *obj, int action_id, ActionCompleteReason complete_reason)
{
	if (Commands->Get_Health(obj) > 0)
	{
		Commands->Action_Reset(obj, 100);
		if (action_id == RT_Free_Aim)
		{
			if (CurrentCommand == RT_Free_Aim)
			{
				FreeAim(obj);
			}
			else
			{
				Commands->Action_Reset(obj, 100);
			}
		}
		if (action_id == RT_Target_Owner)
		{
			if (CurrentCommand == RT_Target_Owner)
			{
				if (Moving)
				{
					Moving = false;
				}
			}
		}
		if (action_id == RT_Target_Veh)
		{
			if (CurrentCommand == RT_Target_Veh)
			{
				TargetVeh(obj);
			}
		}
		if (action_id == RT_Target_Players)
		{
			if (CurrentCommand == RT_Target_Players)
			{
				TargetPlayer(obj);
			}
		}
		Attacking = false;
	}
}

void KB_AI_Rep_Turret::Killed(GameObject *obj, GameObject *shooter)
{
	int ID = Get_Int_Parameter("Owner");
	StringClass Msg;
	Msg.Format("CMSGP %d 0,255,0 your turret has been killed", ID);
	Console_Input(Msg);
}

void KB_AI_Rep_Turret::Custom(GameObject *obj, int type, int param, GameObject *sender)
{
	if (type == CurrentCommand)
	{
		return;
	}
	/*if(type == RT_Free_Aim)
	{
	CurrentCommand = RT_Free_Aim;
	FreeAim(obj);
	}*/
	if (type == RT_Target_Owner)
	{
		CurrentCommand = RT_Target_Owner;
		TargetOwner(obj, Get_GameObj(Get_Int_Parameter("Owner")));
	}
	if (type == RT_Target_Veh)
	{
		CurrentCommand = RT_Target_Veh;
		TargetVeh(obj);
	}
	if (type == RT_Target_Players)
	{
		CurrentCommand = RT_Target_Players;
		TargetPlayer(obj);
	}
}

void KB_AI_Rep_Turret::Timer_Expired(GameObject* obj, int Number)
{
	if (Number == 22)
	{
		if (CurrentCommand == RT_Target_Owner)
		{
			TargetOwner(obj, Get_GameObj(Get_Int_Parameter("Owner")));
		}
		if (CurrentCommand == RT_Target_Veh)
		{
			//is current target fully healed?
			GameObject *Target = Commands->Find_Object(TargetID);
			if (Target)
			{
				float maxhp = Commands->Get_Max_Health(Target);
				maxhp += Commands->Get_Max_Shield_Strength(Target);
				float HP = Commands->Get_Health(Target);
				HP += Commands->Get_Shield_Strength(Target);
				if (HP >= 0.8*maxhp || Commands->Get_Distance(Commands->Get_Position(Target), (Commands->Get_Position(obj))) > 13)
				{
					Attacking = false;
					TargetID = NULL;
					TargetVeh(obj);
				}
			}
			else
			{
				TargetID = NULL;
				TargetVeh(obj);
			}
		}
		if (CurrentCommand == RT_Target_Players)
		{
			//is current target fully healed?
			GameObject *Target = Commands->Find_Object(TargetID);
			if (Target)
			{
				float maxhp = Commands->Get_Max_Health(Target);
				maxhp += Commands->Get_Max_Shield_Strength(Target);
				float HP = Commands->Get_Health(Target);
				HP += Commands->Get_Shield_Strength(Target);
				if (HP >= 0.8*maxhp || Commands->Get_Distance(Commands->Get_Position(Target), (Commands->Get_Position(obj))) > 13)
				{
					Attacking = false;
					TargetID = NULL;
					TargetPlayer(obj);
				}
			}
			else
			{
				TargetID = NULL;
				TargetPlayer(obj);
			}
		}
		Commands->Start_Timer(obj, this, 1, 22);
	}
}

void KB_AI_Rep_Turret::TargetOwner(GameObject *obj, GameObject *target)
{
	if (target) //Can't do anything without a target
	{
		float distance = Commands->Get_Distance(Commands->Get_Position(target), (Commands->Get_Position(obj)));

		if (distance > 10 && !Moving)
		{
			Commands->Action_Reset(obj, 100);
			ActionParamsStruct params;
			params.Set_Basic(this, 100, RT_Target_Owner);
			params.Set_Movement(Get_GameObj(Get_Int_Parameter("Owner")), 1, 5, false);
			params.Set_Attack(target, 40, 0, true);
			params.AttackCheckBlocked = false;
			params.AttackForceFire = true;
			Commands->Action_Attack(obj, params);
			Moving = true;
		}
		else
		{
			if (!Moving)
			{
				Commands->Action_Reset(obj, 100);
				ActionParamsStruct params;
				params.Set_Basic(this, 100, RT_Target_Owner);
				params.Set_Attack(target, 40, 0, true);
				params.AttackCheckBlocked = false;
				params.AttackForceFire = true;
				Commands->Action_Attack(obj, params);
				Moving = false;
			}
		}
	}
}

void KB_AI_Rep_Turret::TargetVeh(GameObject *obj)
{
	GameObject *Old = Commands->Find_Object(TargetID);
	GameObject *Lowest = Find_Target(obj, true);

	if (Lowest)
	{
		if (Lowest != Old && !Attacking)
		{
			Commands->Action_Reset(obj, 100);
			TargetID = Commands->Get_ID(Lowest);
			ActionParamsStruct params;
			params.Set_Basic(this, 100, RT_Target_Veh);
			params.Set_Attack(Lowest, 40, 0, true);
			params.AttackCheckBlocked = false;
			params.AttackWanderAllowed = false;
			params.AttackForceFire = true;
			Commands->Action_Attack(obj, params);
			Attacking = true;
		}
		else
		{
			FreeAim(obj);
		}
	}
	else
	{
		FreeAim(obj);
	}
}

void KB_AI_Rep_Turret::TargetPlayer(GameObject *obj)
{
	GameObject *Old = Commands->Find_Object(TargetID);
	GameObject *Lowest = Find_Target(obj, false);

	if (Lowest)
	{
		if (Lowest != Old && !Attacking)
		{
			Commands->Action_Reset(obj, 100);
			TargetID = Commands->Get_ID(Lowest);
			ActionParamsStruct params;
			params.Set_Basic(this, 100, RT_Target_Players);
			params.Set_Attack(Lowest, 40, 0, true);
			params.AttackCheckBlocked = false;
			params.AttackWanderAllowed = false;
			Commands->Action_Attack(obj, params);
			params.AttackForceFire = true;
			Attacking = true;
		}
		else
		{
			FreeAim(obj);
		}
	}
	else
	{
		FreeAim(obj);
	}
}

void KB_AI_Rep_Turret::FreeAim(GameObject *obj)
{
	Commands->Action_Reset(obj, 100);
	float Facing = Commands->Get_Facing(obj);
	PosTar = Commands->Get_Position(obj);
	PosTar.Z += 1.50f;
	PosTar.X += static_cast<float>(30 * cos(Facing*(PI / 180)));
	PosTar.Y += static_cast<float>(30 * sin(Facing*(PI / 180)));
	ActionParamsStruct params;
	params.AttackWanderAllowed = false;
	params.Set_Basic(this, 100, RT_Free_Aim);
	params.Set_Attack(PosTar, 40, 0, true);
	params.AttackCheckBlocked = false;
	Commands->Action_Attack(obj, params);
	Attacking = false;
}

GameObject *KB_AI_Rep_Turret::Find_Target(GameObject *obj, bool veh)
{
	GameObject *Lowest = NULL;
	if (veh)
	{
		SLNode<VehicleGameObj> *x = GameObjManager::VehicleGameObjList.Head();
		while (x)
		{
			GameObject *o = (GameObject *)x->Data();
			if (o)
			{
				float maxhp = Commands->Get_Max_Health(o) + Commands->Get_Max_Shield_Strength(o);
				float HP = Commands->Get_Health(o) + Commands->Get_Shield_Strength(o);
				if (HP < maxhp && Commands->Get_Distance(Commands->Get_Position(o), Commands->Get_Position(obj)) < 13 && Get_Object_Type(o) == Get_Object_Type(obj))
				{
					if (Lowest)
					{
						float LowestHP = Commands->Get_Health(Lowest) + Commands->Get_Shield_Strength(Lowest);
						if (LowestHP > HP)
						{
							Lowest = o;
						}
					}
					else
					{
						Lowest = o;
					}
				}
			}
			x = x->Next();
		}
	}
	else
	{
		SLNode<SoldierGameObj> *x = GameObjManager::SoldierGameObjList.Head();
		while (x)
		{
			GameObject *o = (GameObject *)x->Data();
			if (o)
			{
				if (Commands->Is_A_Star(o) && o->As_SoldierGameObj()
					&& Get_Object_Type(o) == Get_Object_Type(obj))
				{
					float maxhp = Commands->Get_Max_Health(o) + Commands->Get_Max_Shield_Strength(o);
					float HP = Commands->Get_Health(o) + Commands->Get_Shield_Strength(o);
					if (HP < maxhp && Commands->Get_Distance(Commands->Get_Position(o), Commands->Get_Position(obj)) < 13)
					{
						if (Lowest)
						{
							float LowestHP = Commands->Get_Health(Lowest) + Commands->Get_Shield_Strength(Lowest);
							if (LowestHP > HP)
							{
								Lowest = o;
							}
						}
						else
						{
							Lowest = o;
						}
					}
				}
			}
			x = x->Next();
		}
	}
	return Lowest;
}
ScriptRegistrant<KB_AI_Rep_Turret> KB_AI_Rep_Turret_Registrant("KB_AI_Rep_Turret", "Owner:int");


