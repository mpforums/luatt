
#include "General.h"
#include "luatt.h"

#include "engine.h"
#include "Definition.h"
#include "PurchaseSettingsDefClass.h"
#include "SoldierGameObj.h"

#include "engine_tt.h"
#include "engine_ttdef.h"
#include "engine_io.h"
#include "gmgame.h"

#include "ScriptableGameObj.h"
#include "ConnectionRequest.h"
#include "Iterator.h"

#include "weaponmgr.h"

#include "CommandLineParser.h"


#include "Misc.h"

//#include <fstream>
#include <stdarg.h>

extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
	#include "lsqlite3.h"
}

#include "LuaManager.h"
#include "LuaFunctions.h"
#include "LuaHooks.h"
#include "Lua_Class.h"
//#include "..\hooks.h"




#pragma warning(disable: 4291)


LuaScriptManager::Script *LuaScriptManager::Scripts[256];

void LuaScriptManager::Load()
{
	memset((void *)Scripts, 0, 256*4);
}

ScriptImpClass *LuaScriptManager::Get_Script(GameObject *obj, int scriptID)
{
	const SimpleDynVecClass<GameObjObserverClass *> *observers = &((ScriptableGameObj *)obj)->Get_Observers();
	int x = observers->Count();
	for (int i = 0;i < x;i++)
	{
		if((*observers)[i]->Get_ID() == scriptID)
		{
			return ((ScriptImpClass*)(*observers)[i]);
		}
	}
	return 0;
}

void LuaScriptManager::Lua_DestroyScript(GameObject *obj, int scriptID)
{
	if(!obj)
	{
		return;
	}
	ScriptImpClass *script = Get_Script(obj, scriptID);
	if(!script)
	{
		return;
	}
	script->Destroy_Script();
}

float LuaScriptManager::Lua_Get_Float_Parameter(GameObject *obj, int scriptID, const char *Name)
{
	if(!obj)
	{
		return 0;
	}
	ScriptImpClass *script = Get_Script(obj, scriptID);
	if(!script)
	{
		return 0;
	}
	return script->Get_Float_Parameter(Name);
}

int LuaScriptManager::Lua_Get_Int_Parameter(GameObject *obj, int scriptID, const char *Name)
{
	if(!obj)
	{
		return 0;
	}
	ScriptImpClass *script = Get_Script(obj, scriptID);
	if(!script)
	{
		return 0;
	}
	return script->Get_Int_Parameter(Name);
}


class LuaManager::CommandLua :
	public ConsoleFunctionClass
{
public:
	const char* Get_Name() 
	{ 
		return "Lua"; 
	} 
	const char* Get_Help() 
	{ 
		return "LUA - Shows version and plugins of LuaTT.";
	}
	void Activate(const char* argumentsString)
	{
		int versionlua = LUATT_BUILD;
		char msgz[600];
		char pluginlist[400];

		for(int lua_it = 0; lua_it < 256; lua_it++)
	    {
		  if(!LuaManager::Lua[lua_it])
		  {
			//sprintf(pluginlist, "%s\n", LuaManager::LuaNames[lua_it]);
		  }
		}

	    sprintf(msgz,"\n---\nLua 1.2 %d\n---\nPlugins:\n%s\n---\n",versionlua,pluginlist);
	    Console_Output(msgz);
	}
};

const char *LuaScriptManager::Lua_Get_String_Parameter(GameObject *obj, int scriptID, const char *Name)
{
	if(!obj)
	{
		return "";
	}
	ScriptImpClass *script = Get_Script(obj, scriptID);
	if(!script)
	{
		return "";
	}
	return script->Get_Parameter(Name);
}

int LuaScriptManager::Register_Script(lua_State *L)
{
	if(!lua_istable(L, -1))
	{
		luaL_error(L, "Bad argument #1 to Register_Script. Expected table.");
		return 0;
	}

	const char *Name = luaL_checkstring(L, -3);
	if(!Name)
	{
		return 0;
	}

	const char *Parameters = luaL_checkstring(L, -2);
	if(!Parameters)
	{
		return 0;
	}

	for(int script_it = 0; script_it < 256; script_it++)
	{
		if(Scripts[script_it])
		{
			if(_stricmp(Scripts[script_it]->Name, Name) == 0)
			{
			//	lua_pop(L, 1);
		    //	luaL_error(L, "You cannot register more than one script with the same name.");
				Console_Output("[Lua-Error] You cannot register more then one script with the same name.\n");
				return 0;
			}
		}
	}

	int uRef = luaL_ref(L, LUA_REGISTRYINDEX);
	
	Script *s = new Script;
	s->uRef = uRef;
	strcpy(s->Name, Name);
	strcpy(s->Param, Parameters);
	s->scriptreg = new ScriptRegistrant<LuaScript>(Name, Parameters); // removed (char *)
	s->lua = L;

	for(int script_it = 0; script_it < 256; script_it++)
	{
		if(Scripts[script_it] == 0)
		{
			Scripts[script_it] = s;
			return 0;
		}
	}
	return 0;	
}

void LuaScriptManager::Lua_Start_Timer(GameObject *obj, int scriptID, float Time, int Number)
{
	if(!obj)
	{
		return;
	}
	ScriptImpClass *script = Get_Script(obj, scriptID);
	if(!script)
	{
		return;
	}
	Commands->Start_Timer(obj, script, Time, Number);
}

void LuaScriptManager::Cleanup()
{

	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		delete s->scriptreg;
		delete s;
		
	}
	memset((void *)Scripts, 0, 256*4);

}

void LuaScriptManager::RegisterScriptFunctions(lua_State *L)
{
	lua_register(L, "DestroyScript", LuaScriptManager::Lua_DestroyScript_Wrap);
	lua_register(L, "Get_Float_Parameter", LuaScriptManager::Lua_Get_Float_Parameter_Wrap);
	lua_register(L, "Get_Int_Parameter", LuaScriptManager::Lua_Get_Int_Parameter_Wrap);
	lua_register(L, "Get_String_Parameter", LuaScriptManager::Lua_Get_String_Parameter_Wrap);
	lua_register(L, "Start_Timer", LuaScriptManager::Lua_Start_Timer_Wrap);
	lua_register(L, "Register_Script", LuaScriptManager::Register_Script);
	lua_register(L, "Install_Hook", LuaScriptManager::Lua_Install_Hook_Warp);
}

int LuaScriptManager::Lua_DestroyScript_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 2)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int objID = (int)lua_tonumber(L, 2);
	LuaScriptManager::Lua_DestroyScript(Commands->Find_Object(objID), scriptID);
	return 0;
}
int LuaScriptManager::Lua_Get_Float_Parameter_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 3)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int objID = (int)lua_tonumber(L, 2);
	const char *name = lua_tostring(L, 3);
	lua_pushnumber(L, LuaScriptManager::Lua_Get_Float_Parameter(Commands->Find_Object(objID), scriptID, name));
	return 1;
}
int LuaScriptManager::Lua_Get_Int_Parameter_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 3)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int objID = (int)lua_tonumber(L, 2);
	const char *name = lua_tostring(L, 3);
	lua_pushnumber(L, LuaScriptManager::Lua_Get_Int_Parameter(Commands->Find_Object(objID), scriptID, name));
	return 1;
}
int LuaScriptManager::Lua_Get_String_Parameter_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 3)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int objID = (int)lua_tonumber(L, 2);
	const char *name = lua_tostring(L, 3);
	lua_pushstring(L, LuaScriptManager::Lua_Get_String_Parameter(Commands->Find_Object(objID), scriptID, name));
	return 1;
}
int LuaScriptManager::Lua_Start_Timer_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 4)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int obj = (int)lua_tonumber(L, 2);
	float Time = (float)lua_tonumber(L, 3);
	int Number = (int)lua_tonumber(L, 4);
	LuaScriptManager::Lua_Start_Timer(Commands->Find_Object(obj), scriptID, Time, Number);
	return 0;
}

int LuaScriptManager::Lua_Install_Hook_Warp(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 3)
	{
		return 0;
	}
	int scriptID = (int)lua_tonumber(L, 1);
	int objID = (int)lua_tonumber(L, 2);
	const char *TheKey = lua_tostring(L, 3);
	//LuaScriptManager::Lua_Install_Hook(Commands->Find_Object(obj), scriptID, TheKey);
	
	GameObject* obj = Commands->Find_Object(objID);
	if (!obj) return 0;

	LuaScript* script = (LuaScript*)Get_Script(obj, scriptID);
	if (!script) return 0;
	script->InstallHook(TheKey, obj);
	return 0;
}

//The dynamic script itself
void LuaScriptManager::LuaScript::Created(GameObject *obj)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Created");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Destroyed(GameObject *obj)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Destroyed");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Detach(GameObject *obj)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Detach");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::KeyHook()
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "KeyHook");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(Owner()));
				LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Killed(GameObject *obj,GameObject *shooter)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Killed");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(shooter));
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Damaged(GameObject *obj,GameObject *damager,float damage)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Damaged");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(damager));
				lua_pushnumber(L, damage);
				LuaManager::Report_Errors(L, lua_pcall(L, 5, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Custom");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, message);
				lua_pushnumber(L, param);
				lua_pushnumber(L, Commands->Get_ID(sender));
				LuaManager::Report_Errors(L, lua_pcall(L, 6, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Enemy_Seen");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(seen));
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Action_Complete(GameObject *obj,int action, ActionCompleteReason reason)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Action_Complete");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, action);

				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Timer_Expired(GameObject *obj,int number)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Timer_Expired");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, number);
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Animation_Complete(GameObject *obj,const char *anim)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Animation_Complete");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushstring(L, anim);
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Poked(GameObject *obj,GameObject *poker)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Poked");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(poker));
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Entered(GameObject *obj,GameObject *enter)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Entered");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(enter));
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}
void LuaScriptManager::LuaScript::Exited(GameObject *obj,GameObject *exit)
{
	for(int script_it = 0; script_it < 256; script_it++)
	{
		Script *s = Scripts[script_it];
		if(!s)
		{
			continue;
		}
		if(strcmp(s->Name, this->Get_Name()) == 0)
		{
			lua_State *L = s->lua;
			if(!L)
			{
				continue;
			}
			lua_rawgeti(L, LUA_REGISTRYINDEX, s->uRef);
			if(!lua_istable(L, -1))
			{
				return;
			}

			lua_getfield(L, -1, "Exited");
			if(lua_isfunction(L, -1))
			{
				lua_pushvalue(L, -2);
				lua_pushnumber(L, Get_ID());
				lua_pushnumber(L, Commands->Get_ID(obj));
				lua_pushnumber(L, Commands->Get_ID(exit));
				LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
			}
		}
	}
}



//LuaManager

lua_State *LuaManager::Lua[256];

void LuaManager::Load()
{
	memset((void *)Lua, 0, 256*4);
	
	int Loc = (int)main_loop_glue - 0x0043BAC8 - 5;
	char h[5];
	*h = '\xE9';
	memcpy((void *)(h+1), (void *)&Loc, 4);

	unsigned long op;
	void *p = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	VirtualProtectEx(p, (void *)0x0043BAC8, 5, PAGE_EXECUTE_READWRITE, &op);
	WriteProcessMemory(p, (void *)0x0043BAC8, h, 5, NULL);
	VirtualProtectEx(p, (void *)0x0043BAC8, 5, op, NULL);
}

void LuaManager::ShowBanner()
{
	Console_Output("|-------------------------------------------------------|\n");
	Console_Output("| LuaJIT 2.0.3 Copyright � 2005-2014 Mike Pall          |\n");
	Console_Output("| Originaly created by jnz                              |\n");
	Console_Output("| LuaTT 2.1 by sla.ro. thanks jonwil for help           |\n");
	Console_Output("|-------------------------------------------------------|\n");
	char revisionluatt[255];
	int lttver = LUATT_BUILD;
	sprintf(revisionluatt, "LuaTT 2.1 %d revision\n", lttver);
	Console_Output(revisionluatt);
	LoadConsoleLua();
}



void LoadConsoleLua()
{
	ConsoleFunctionList.Add(new LuaManager::CommandLua);
	Sort_Function_List();
	Verbose_Help_File();
}


void LuaManager::LoadLua(const char *LuaFile)
{
	lua_State *L = lua_open();
	luaL_openlibs(L);
	LuaSocketManager::Register(L);
	lsqlite3(L);
//	LuaSLNodeWrapper::Register(L);
	AddFunctions(L);
	int s = luaL_loadfile(L, LuaFile);
	if (s == 0) 
	{
		s = lua_pcall(L, 0, LUA_MULTRET, 0);
	}
	if(Report_Errors(L, s))
	{
		lua_close(L);
		Console_Output("[Lua] Failed to load plugin: %s\n", LuaFile);
		return;
	}
	Console_Output("[Lua] Loaded plugin: %s\n", LuaFile);
	
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		if(!Lua[lua_it])
		{
			Lua[lua_it] = L;
			//LuaNames[lua_it] = LuaFile;
			break;
		}
	}
}

void LuaManager::Cleanup()
{
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		if(Lua[lua_it])
		{
			lua_close(Lua[lua_it]);
			Lua[lua_it] = 0;
			//LuaNames[lua_it]= 0;
		}
	}

	Delete_Console_Function("LuaManager::CommandLua");
}

void LuaManager::Reload_All()
{
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "UnloadingLua");
		if(lua_isfunction(L, -1))
		{
			LuaManager::Report_Errors(L, lua_pcall(L, 0, 0, 0));
		}
		else
		{
			lua_setglobal(L, "UnloadingLua");
		}
	}

    LuaManager::Cleanup();
	LuaManager::Load();

	char basepath[64];
	GetCurrentDirectory(64, basepath);
	char pluginsdir[256];
	sprintf(pluginsdir, "%s\\LuaPlugins\\*", basepath);

	char *plugins[256];
	memset((void *)plugins, 0, 256*4);
	GetFiles(pluginsdir, plugins, 256);

	for(int i = 0; i < 256; i++)
	{
		char *x = plugins[i];
		if(!x)
		{
			continue;
		}
		char *path = new char[strlen(x)+strlen(basepath)+14];
		sprintf(path, "%s\\LuaPlugins\\%s", basepath, x);
		LuaManager::LoadLua(path);
		delete []path;
		delete []x;
	}
}


//hooks

bool LuaManager::Call_Chat_Hook(int ID, int Type, const wchar_t *Msg, int Target)
{
	int ret = 1;
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnChat");
		if(lua_isfunction(L, -1))
		{
			const char *Msg2 = WideCharToChar(Msg);
			lua_pushnumber(L, ID);
			lua_pushnumber(L, Type);
			lua_pushstring(L, Msg2);
			lua_pushnumber(L, Target);
			LuaManager::Report_Errors(L, lua_pcall(L, 4, 1, 0));
			ret = !ret ? ret : (int)lua_tonumber(L, -1);
			delete []Msg2;
		}
		else
		{
			lua_setglobal(L, "OnChat");
		}
	}
	return ret != 0 ? 1 : 0;
}

const char *LuaManager::ConAc(const char *ip, const char *nick, const char *serial, const char *version)
{
	const char *ret = "CONTINUE";

	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "ConnectionAcceptance");
		if(lua_isfunction(L, -1))
		{
			lua_pushstring(L, nick);
			lua_pushstring(L, ip);
			lua_pushstring(L, serial);
			lua_pushstring(L, version);
			LuaManager::Report_Errors(L, lua_pcall(L, 4, 1, 0));
			
			ret = (const char *)lua_tostring(L, -1);
			if (!ret) { ret="CONTINUE"; }
		}
		else
		{
			lua_setglobal(L, "ConnectionAcceptance");
		}
	}
	
	return ret;
}


bool LuaManager::RadioHook(int PlayerType, int PlayerID, int AnnouncementID, int IconID, AnnouncementEnum AnnouncementType)
{
	int ret = 1;
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "RadioHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, PlayerType);
			lua_pushnumber(L, PlayerID);
			lua_pushnumber(L, AnnouncementID);
			lua_pushnumber(L, IconID);
			lua_pushnumber(L, AnnouncementType);
			LuaManager::Report_Errors(L, lua_pcall(L, 5, 1, 0));
			ret = !ret ? ret : (int)lua_tonumber(L, -1);
		}
		else
		{
			lua_setglobal(L, "RadioHook");
		}
	}
	return ret != 0 ? 1 : 0;
}

bool LuaManager::Refill_Hook(GameObject *purchaser)
{
	if (!purchaser) { return true; }
	int ret = 1;
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnRefill");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, Commands->Get_ID(purchaser));
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 1, 0));
			ret = !ret ? ret : (int)lua_tonumber(L, -1);
		}
		else
		{
			lua_setglobal(L, "OnRefill");
		}
	}
	return ret != 0 ? 1 : 0;
}

bool LuaManager::Call_Host_Hook(int PlayerID,TextMessageEnum Type,const char *Message)
{
	//if (!PlayerID) { return true; }
	//if (!Message) { return true; }
	//if (!Type) { return true; }
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnHostMessage");
		if(lua_isfunction(L, -1))
		{
		//	if (!PlayerID) { PlayerID = 0; }
			//if (!Type) { return true; }
			lua_pushnumber(L, PlayerID);
			lua_pushnumber(L, Type);
			lua_pushstring(L, Message);
			LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnHostMessage");
		}
	}
	return true;
}
void LuaManager::Call_Player_Join_Hook(int ID, const char *Nick)
{
	if (!ID) { return; }
	if (!Nick) { return; }
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnPlayerJoin");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, ID);
			lua_pushstring(L, Nick);
			LuaManager::Report_Errors(L, lua_pcall(L, 2, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnPlayerJoin");
		}
	}
}
void LuaManager::Call_Player_Leave_Hook(int ID)
{
	if (!ID) { return; }
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, "OnPlayerLeave");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, ID);
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnPlayerLeave");
		}
	}
}
void LuaManager::Call_Level_Loaded_Hook()
{
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, "OnLevelLoaded");
		if(lua_isfunction(L, -1))
		{
			LuaManager::Report_Errors(L, lua_pcall(L, 0, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnLevelLoaded");
		}
	}
}
void LuaManager::Call_GameOver_Hook()
{
	// TO TEST
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, "OnGameOver");
		if(lua_isfunction(L, -1))
		{
			LuaManager::Report_Errors(L, lua_pcall(L, 0, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnGameOver");
		}
	}
}
void LuaManager::Call_Console_Output_Hook(const char *Output)
{

	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, "OnConsoleOutput");
		if(lua_isfunction(L, -1))
		{
			lua_pushstring(L, Output);
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnConsoleOutput");
		}
	}
}

void LuaManager::Purchase_Hook(BaseControllerClass *base, GameObject *purchaser, unsigned int cost, unsigned int preset,unsigned int purchaseret,const char *data)
{
	//if (!purchaser) { return; }
	//if (!cost) { return; }
	//if (!preset) { return; }
	//if (!purchaseret) { return; }

	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, data);
		if(lua_isfunction(L, -1))
		{
			if (!Commands->Get_ID(purchaser)) { return; }
			lua_pushnumber(L, Commands->Get_ID(purchaser));
			lua_pushnumber(L, cost);
			lua_pushnumber(L, preset);
			lua_pushnumber(L, purchaseret);
			LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
		}
		else
		{
			lua_setglobal(L, data);
		}
	}
}

int LuaManager::Purchase_Hook2(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data)
{
	int ret = -1;

	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		
		lua_getglobal(L, "VehicleTTHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, Commands->Get_ID(purchaser));
			lua_pushnumber(L, cost);
			lua_pushnumber(L, preset);
			LuaManager::Report_Errors(L, lua_pcall(L, 3, 1, 0));
			int rety = (int)lua_tonumber(L, -1);
			if (rety) ret=rety;
		}
		else
		{
			lua_setglobal(L, "VehicleTTHook");
		}
	}

	return ret;
}


bool LuaManager::TT_Damage_Hook(PhysicalGameObj* damager, PhysicalGameObj* target, const AmmoDefinitionClass* ammo, const char* bone)
{
	int ret = 1;
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "TTDamageHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, Commands->Get_ID(damager));
			lua_pushnumber(L, Commands->Get_ID(target));
			lua_pushstring(L, ammo->Get_Name());
			lua_pushnumber(L, ammo->Warhead());
			lua_pushstring(L, bone);
			LuaManager::Report_Errors(L, lua_pcall(L, 5, 1, 0));
			ret = !ret ? ret : (int)lua_tonumber(L, -1);
		}
		else
		{
			lua_setglobal(L, "TTDamageHook");
		}
	}
	return ret != 0 ? 1 : 0;

}


bool LuaManager::Report_Errors(lua_State *L, int status)
{
	if(status != 0) 
	{
		const char *err = lua_tostring(L, -1);
		lua_pop(L, 1);
		Console_Output("[Lua_Error] %s\n", err);
		lua_getglobal(L, "OnError");
		if(lua_isfunction(L, -1))
		{
			lua_pushstring(L, err);
			if(lua_pcall(L, 1, 0, 0) != 0)
			{
				//printf("[Error] Could not call OnError\n%s\n", lua_tostring(L, -1)); // disabled because sometimes causes crash
				lua_pop(L, 1);
			}
		}
		else
		{
			//printf("[Error] %s\n", err); // disabled because sometimes causes crash
		}
		return 1;
	}

	return 0;
}


void LuaManager::UnloadingLua()
{
 for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "UnloadingLua");
		if(lua_isfunction(L, -1))
		{
			LuaManager::Report_Errors(L, lua_pcall(L, 0, 0, 0));
		}
		else
		{
			lua_setglobal(L, "UnloadingLua");
		}
	}


 Delete_Console_Function("Lua");
}


void LuaManager::Call_Object_Hook(void *data, GameObject *obj)
{
	if (!obj) { return; }
	Commands->Attach_Script(obj,"Lua_Script_Hook","");

	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnObjectCreate");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, Commands->Get_ID(obj));
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnObjectCreate");
		}
	}
}

void LuaManager::Adress_IP(const char *addr)
{
	if (!addr) { return; }
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "Adress_IP");
		if(lua_isfunction(L, -1))
		{
			lua_pushstring(L, addr);
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
		else
		{
			lua_setglobal(L, "Adress_IP");
		}
	}
}

bool LuaManager::Reload_Flag=false;

void LuaManager::Call_Think_Hook()
{
	if(Reload_Flag)
	{
		Reload_Flag = false;
		Reload_All();
	}
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnThink");
		if(lua_isfunction(L, -1))
		{
			LuaManager::Report_Errors(L, lua_pcall(L, 0, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnThink");
		}
	}
}

void LuaManager::CallInvoke(const char *Function, const char *Arg)
{
	for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, Function);
		if(lua_isfunction(L, -1))
		{
			lua_pushstring(L, Arg);
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
	}
}

//socket manager

TCPSocket *LuaSocketManager::Sockets[64];

void LuaSocketManager::Load()
{
	memset((void *)Sockets, 0, 4*64);
	WSADATA wsaData;
	if(WSAStartup(0x202, &wsaData))
	{
		Console_Output("Error starting winsock, LuaSocket may not work.\n");
	}

}

int LuaSocketManager::Connect(const char *IP, int port)
{
	TCPSocket *s = new TCPSocket;

	if(!s->Client(IP, port))
	{
		delete s;
		return -1;
	}

	for(int i = 0; i < 64; i++)
	{
		if(!Sockets[i])
		{
			Sockets[i] = s;
			return i;
		}
	}
	delete s;
	return -1;
}

int LuaSocketManager::Server(const char *IP, int port)
{
	TCPSocket *s = new TCPSocket;

	if(!s->Server(IP, port, 10))
	{
		delete s;
		return -1;
	}

	for(int i = 0; i < 64; i++)
	{
		if(!Sockets[i])
		{
			Sockets[i] = s;
			return i;
		}
	}
	delete s;
	return -1;
}

int LuaSocketManager::Accept(int Socket)
{
	if(Socket < 0)
	{
		return -1;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return -1;
	}

	TCPSocket *c = new TCPSocket;
	if(!s->Accept(c))
	{
		delete c;
		return -1;
	}
	
	for(int i = 0; i < 64; i++)
	{
		if(!Sockets[i])
		{
			Sockets[i] = c;
			return i;
		}
	}
	delete c;
	return -1;
}

int LuaSocketManager::Recv(int Socket, char *Data, int Length)
{
	if(Socket < 0)
	{
		return -1;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return -1;
	}

	if(!s->RecviveData(Data, &Length))
	{
		return -1;
	}
	Data[Length] = 0;
	return 0;
}

int LuaSocketManager::Send(int Socket, const char *Data)
{
	if(Socket < 0)
	{
		return -1;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return -1;
	}

	if(!s->SendData(Data, strlen(Data)))
	{
		return -1;
	}
	return 0;
}

int LuaSocketManager::Is_DataAvaliable(int Socket)
{
	if(Socket < 0)
	{
		return -1;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return -1;
	}

	if(!s->Is_DataAvaliable())
	{
		return -1;
	}
	return 0;
}

int LuaSocketManager::Is_ClientWaiting(int Socket)
{
	if(Socket < 0)
	{
		return -1;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return -1;
	}

	if(!s->Is_ConnectionWaiting())
	{
		return -1;
	}
	return 0;
}

void LuaSocketManager::Disconnect(int Socket)
{
	if(Socket < 0)
	{
		return;
	}
	TCPSocket *s = Sockets[Socket];
	if(!s)
	{
		return;
	}

	s->Destroy();
	delete s;
	Sockets[Socket] = 0;
}


int LuaSocketManager::Lua_Connect_Wrap(lua_State *L)
{

	int argc = lua_gettop(L);
	if(argc < 2)
	{
		return 0;
	}
	const char *Host = lua_tostring(L, 1);
	int Port = (int)lua_tonumber(L, 2);
	lua_pushnumber(L, Connect(Host, Port));
	return 1;
}

int LuaSocketManager::Lua_Server_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 2)
	{
		return 0;
	}
	const char *Host = lua_tostring(L, 1);
	int Port = (int)lua_tonumber(L, 2);
	lua_pushnumber(L, Server(Host, Port));
	return 1;
}

int LuaSocketManager::Lua_Accept_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	int Sock = (int)lua_tonumber(L, 1);
	lua_pushnumber(L, Accept(Sock));
	return 1;
}

int LuaSocketManager::Lua_Recv_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	char data[1024];
	*data = 0;
	int Sock = (int)lua_tonumber(L, 1);
	Recv(Sock, data, 1023);
	lua_pushstring(L, data);
	return 1;
}

int LuaSocketManager::Lua_Send_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 2)
	{
		return 0;
	}
	int Sock = (int)lua_tonumber(L, 1);
	const char *str = lua_tostring(L, 2);
	lua_pushnumber(L, Send(Sock, str));
	return 1;
}

int LuaSocketManager::Lua_DataAvaliable_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	int Sock = (int)lua_tonumber(L, 1);
	lua_pushnumber(L, Is_DataAvaliable(Sock));
	return 1;
}

int LuaSocketManager::Lua_ClientWaiting_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	int Sock = (int)lua_tonumber(L, 1);
	lua_pushnumber(L, Is_ClientWaiting(Sock));
	return 1;
}

int LuaSocketManager::Lua_Disconnect_Wrap(lua_State *L)
{
	int argc = lua_gettop(L);
	if(argc < 1)
	{
		return 0;
	}
	
	int Sock = (int)lua_tonumber(L, 1);
	Disconnect(Sock);
	return 0;
}


void LuaSocketManager::Register(lua_State *L)
{
	lua_register(L, "Client", Lua_Connect_Wrap);
	lua_register(L, "Server", Lua_Server_Wrap);
	lua_register(L, "Accept", Lua_Accept_Wrap);
	lua_register(L, "Recv", Lua_Recv_Wrap);
	lua_register(L, "Send", Lua_Send_Wrap);
	lua_register(L, "DataAvaliable", Lua_DataAvaliable_Wrap);
	lua_register(L, "ClientWaiting", Lua_ClientWaiting_Wrap);
	lua_register(L, "Disconnect", Lua_Disconnect_Wrap);
}

void LuaSocketManager::Cleanup()
{
	for(int i = 0; i < 64; i++)
	{
		if(Sockets[i])
		{
			delete Sockets[i];
		}
	}
	memset((void *)Sockets, 0, 4*64);

	WSACleanup();
}

void GetFiles(const char *Path, char **Data, int DataLength)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	hFind = FindFirstFile(Path, &FindFileData);
	if(FindFileData.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY)
	{
		char *file = new char[strlen(FindFileData.cFileName)+1];
		strcpy(file, FindFileData.cFileName);
		for(int i = 0; i < DataLength; i++)
		{
			if(!Data[i])
			{
				Data[i] = file;
				break;
			}
		}
	}
	while (FindNextFile(hFind, &FindFileData) != 0) 
	{
		if(FindFileData.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY)
		{
			char *file = new char[strlen(FindFileData.cFileName)+1];
			strcpy(file, FindFileData.cFileName);
			for(int i = 0; i < DataLength; i++)
			{
				if(!Data[i])
				{
					Data[i] = file;
					break;
				}
			}
		}
	}
	FindClose(hFind);
}

TCPSocket::TCPSocket()
{
	memset((void *)this, 0, sizeof(TCPSocket));
}

TCPSocket::TCPSocket(TCPSocket &_Socket)
{
	memcpy((void *)this, &_Socket, sizeof(TCPSocket));
}

TCPSocket::TCPSocket(const char *Host, int Port)
{
	memset((void *)this, 0, sizeof(TCPSocket));
	this->Client(Host, Port);
}

TCPSocket::TCPSocket(const char *IP, int Port, int Backlog)
{
	memset((void *)this, 0, sizeof(TCPSocket));
	this->Server(IP, Port, Backlog);
}

TCPSocket::TCPSocket(SOCKET &_Socket, int _Mode)
{
	memset((void *)this, 0, sizeof(TCPSocket));
	this->Socket = _Socket;
	this->Mode = _Mode;
	this->Connected = 1;
}

TCPSocket::~TCPSocket()
{
	shutdown(this->Socket, SD_BOTH);
	closesocket(this->Socket);
	memset((void *)this, 0, sizeof(TCPSocket));
}

bool TCPSocket::Client(const char *Host, int Port)
{
	hostent *he;
	if ((he = gethostbyname(Host)) == 0) 
	{
		return 0;
	}
	SOCKET s_ = socket(AF_INET,SOCK_STREAM,0);
	if (s_ == INVALID_SOCKET) 
	{
		return 0;
	}
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons((u_short)Port);
	addr.sin_addr = *((in_addr *)he->h_addr);
	memset(&(addr.sin_zero), 0, 8); 
	if (connect(s_, (sockaddr *) &addr, sizeof(sockaddr))) 
	{
		return 0;
	}

	this->Socket = s_;
	this->Connected = 1;
	this->Mode = 0;
	this->Port = Port;
	strcpy_s(this->Host, 256, Host);
	return 1;
}

bool TCPSocket::Server(const char *IP, int Port, int Backlog)
{
	SOCKET server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server == INVALID_SOCKET) 
	{
		return 0;
	}

	sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(IP == 0 ? "127.0.0.1" : IP);
	service.sin_port = htons((u_short)Port);

	if(bind(server, (SOCKADDR*) &service, sizeof(service)) == SOCKET_ERROR) 
	{
		return 0;
	}

	if (listen(server, Backlog) == SOCKET_ERROR) 
	{
		return 0;
	}
	
	this->Socket = server;
	this->Mode = 1;
	this->Connected = 1;
	return 1;
}

bool TCPSocket::Is_Connected()
{
	if(this->Mode == 1)
	{
		return 1;
	}

	fd_set fd;
	TIMEVAL tv;

	tv.tv_sec = 0;
	tv.tv_usec = 1000;

	FD_ZERO(&fd);
	FD_SET(this->Socket, &fd);

	if(select((int)this->Socket, 0, &fd, 0, &tv) == -1)
	{
		return 0;
	}

	if(FD_ISSET(this->Socket, &fd))
	{
		FD_CLR(this->Socket, &fd);
		return 1;
	}
	FD_CLR(this->Socket, &fd);

	return 0;
}

bool TCPSocket::Is_DataAvaliable()
{
	if(!this->Is_Connected())
	{
		return 0;
	}
	if(this->Mode != 0)
	{
		return 0;
	}
	
	fd_set fd;
	TIMEVAL tv;

	tv.tv_sec = 0;
	tv.tv_usec = 1000;

	FD_ZERO(&fd);
	FD_SET(this->Socket, &fd);

	if(select((int)this->Socket, &fd, 0, 0, &tv) == -1)
	{
		return 0;
	}

	if(FD_ISSET(this->Socket, &fd))
	{
		FD_CLR(this->Socket, &fd);
		return 1;
	}
	FD_CLR(this->Socket, &fd);
	return 0;
}

bool TCPSocket::Is_ConnectionWaiting()
{
	if(this->Mode != 1)
	{
		return 0;
	}
	
	fd_set fd;
	TIMEVAL tv;

	tv.tv_sec = 0;
	tv.tv_usec = 1000;

	FD_ZERO(&fd);
	FD_SET(this->Socket, &fd);

	if(select((int)this->Socket, &fd, 0, 0, &tv) == -1)
	{
		return 0;
	}

	if(FD_ISSET(this->Socket, &fd))
	{
		FD_CLR(this->Socket, &fd);
		return 1;
	}
	FD_CLR(this->Socket, &fd);
	return 0;
}

bool TCPSocket::RecviveData(char *Data, int Length)
{
	if(this->Is_DataAvaliable())
	{
		int ret = recv(this->Socket, Data, Length, 0);
		if(ret <= 0)
		{
			this->Destroy();
			return 0;
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
bool TCPSocket::RecviveData(char *Data, int *Length)
{
	int ret;
	if(this->Is_DataAvaliable())
	{
		ret = recv(this->Socket, Data, *Length, 0);
		if(ret <= 0)
		{
			*Length = 0;
			this->Destroy();
			return 0;
		}
	}
	else
	{
		*Length = 0;
		return 0;
	}
	*Length = ret;
	return 1;
}

bool TCPSocket::SendData(const char *Data, int Length)
{
	if(!this->Is_Connected())
	{
		return 0;
	}

	int Send_ = send(this->Socket, Data, Length, 0);
	if(Send_ != Length)
	{
		if(Send_ == -1)
		{
			return 0;
		}
		else
		{
			this->SendData(Data+Send_, Length-Send_);
		}
	}

	return 1;
}

bool TCPSocket::Destroy()
{
	shutdown(this->Socket, SD_BOTH);
	closesocket(this->Socket);

	memset((void *)this, 0, sizeof(TCPSocket));
	return 0;
}

bool TCPSocket::Accept(TCPSocket *NewConnection)
{
	if(!this->Is_ConnectionWaiting())
	{
		return 0;
	}

	sockaddr_in addr;
	int s_sa = sizeof(sockaddr);
	SOCKET client = accept(this->Socket, (sockaddr *)&addr, &s_sa);
	if(client == INVALID_SOCKET) 
	{
		return 0;
	}


	NewConnection->TCPSocket::TCPSocket(client, 0);

	// get ip
	char ip[40];
        int ip1,ip2,ip3,ip4;
                ip1 = addr.sin_addr.s_addr&0x000000FF;
                ip2 = (addr.sin_addr.s_addr&0x0000FF00)>>8;
                ip3 = (addr.sin_addr.s_addr&0x00FF0000)>>16;
                ip4 = (addr.sin_addr.s_addr&0xFF000000)>>24;

        sprintf(ip,"%d.%d.%d.%d",ip1,ip2,ip3,ip4);
	 LuaManager::Adress_IP(ip);


	return 1;
}










// sla's hooks for Lua SSGM

void LuaManager::KilledHook(int obj,int shooter)
{
for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnKilled");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			lua_pushnumber(L, shooter);
			LuaManager::Report_Errors(L, lua_pcall(L, 2, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnKilled");
		}
	}
}


void LuaManager::DestroyedHook(int obj)
{
for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "OnDestroyed");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			LuaManager::Report_Errors(L, lua_pcall(L, 1, 0, 0));
		}
		else
		{
			lua_setglobal(L, "OnDestroyed");
		}
	}
}

void LuaManager::CustomHook(int obj,int message,int param,int sender)
{
for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "CustomHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			lua_pushnumber(L, message);
			lua_pushnumber(L, param);
			lua_pushnumber(L, sender);
			LuaManager::Report_Errors(L, lua_pcall(L, 4, 0, 0));
		}
		else
		{
			lua_setglobal(L, "CustomHook");
		}
	}
}

void LuaManager::DamageHook(int obj,int damager,float damage)
{
for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "DamageHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			lua_pushnumber(L, damager);
			lua_pushnumber(L, damage);
			LuaManager::Report_Errors(L, lua_pcall(L, 3, 0, 0));
		}
		else
		{
			lua_setglobal(L, "DamageHook");
		}
	}
}

void LuaManager::PokedHook(int obj,int damager)
{
for(int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if(!L)
		{
			continue;
		}
		lua_getglobal(L, "PokedHook");
		if(lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			lua_pushnumber(L, damager);
			LuaManager::Report_Errors(L, lua_pcall(L, 2, 0, 0));
		}
		else
		{
			lua_setglobal(L, "PokedHook");
		}
	}
}

void LuaManager::KeyHook(int obj, const char *key, const char *callback)
{
	for (int lua_it = 0; lua_it < 256; lua_it++)
	{
		lua_State *L = Lua[lua_it];
		if (!L)
		{
			continue;
		}
		lua_getglobal(L, callback);
		if (lua_isfunction(L, -1))
		{
			lua_pushnumber(L, obj);
			lua_pushstring(L, key);
			LuaManager::Report_Errors(L, lua_pcall(L, 2, 0, 0));
		}
		else
		{
			lua_setglobal(L, callback);
		}
	}
}

void Lua_Script_Hook::Killed(GameObject *obj, GameObject *shooter)
{
   LuaManager::KilledHook(Commands->Get_ID(obj), Commands->Get_ID(shooter));
}

void Lua_Script_Hook::Destroyed(GameObject *obj)
{
   LuaManager::DestroyedHook(Commands->Get_ID(obj));
}

void Lua_Script_Hook::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
   LuaManager::CustomHook(Commands->Get_ID(obj),message,param,Commands->Get_ID(sender));
}

void Lua_Script_Hook::Damaged(GameObject *obj,GameObject *damager,float damage)
{
   LuaManager::DamageHook(Commands->Get_ID(obj),Commands->Get_ID(damager),damage);
   
}

void Lua_Script_Hook::Poked(GameObject *obj,GameObject *poker)
{
   LuaManager::PokedHook(Commands->Get_ID(obj),Commands->Get_ID(poker));
}

// Lua
ScriptRegistrant<Lua_Script_Hook> Lua_Script_Hook_Registrant("Lua_Script_Hook","");

void Lua_Key_Hook::Created(GameObject *obj)
{
	myobj = Commands->Get_ID(obj);
	this->InstallHook(Get_Parameter("key"), obj);
}

void Lua_Key_Hook::KeyHook()
{
	LuaManager::KeyHook(myobj, Get_Parameter("key"), Get_Parameter("callback"));
}

// Lua Key Hook Script
ScriptRegistrant<Lua_Key_Hook> Lua_Key_Hookk_Registrant("Lua_Key_Hook", "key=none:string,callback=FunctionName:string");

/*
void CleanUp_ConsoleCommands()
{
	for (int i=0; i++; i<=255) {
		if (LuaCS[i]) {
			const char *p;
			p=LuaCS[i];
			Delete_Console_Function(p->Get_Name());
		}
	}

}


class CommandNotYet :
	public ConsoleFunctionClass
{
public:
	const char* Get_Name() 
	{ 
		return "NOTHING";  // to be
	} 
	const char* Get_Help() 
	{ 
		return "NOTHING - NOTHING"; // to be 
	}
	void Activate(const char* argumentsString)
	{
	 // to be
	}
};
*/


