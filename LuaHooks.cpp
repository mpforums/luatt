#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "Misc.h"
extern "C" 
{
	#include "LuaLib\lua.h"
	#include "LuaLib\lualib.h"
	#include "LuaLib\lauxlib.h"
}
#include "LuaManager.h"
#include "LuaFunctions.h"

void __declspec(naked) main_loop_glue()
{
	__asm
	{
		call LuaManager::Call_Think_Hook;

		pop edi;
		pop esi;
		add esp, 0Ch;
		ret;
	}

}


