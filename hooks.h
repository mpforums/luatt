#include "General.h"
#include "luatt.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

//using std::string;


typedef void (*_SerialHook)(int, const char *);
typedef void (*_LoadingEHook)(int, bool);
typedef bool (*_DamageHook)(int, int, int, float, unsigned int);
typedef bool (*_ChatEHook)(int, int, char &, int);
typedef void (*_PingHook)(int, int);
typedef bool (*_SuicideHook)(int);
typedef bool (*_RadioHook)(int, int, int, int, int);

typedef void (*_AddSerialHook)(_SerialHook);
typedef void (*_AddLoadingEHook)(_LoadingEHook);
typedef void (*_AddDamageHook)(_DamageHook);
typedef void (*_AddChatEHook)(_ChatEHook);
typedef void (*_AddPingHook)(_PingHook);
typedef void (*_AddSuicideHook)(_SuicideHook);
typedef void (*_AddRadioHook)(_RadioHook);

typedef void (*_RequestSerial)(int, char &);

extern _RequestSerial RequestSerial;

extern _AddSerialHook AddSerialHook;
extern _AddLoadingEHook AddLoadingEHook;
extern _AddDamageHook AddDamageHook;
extern _AddChatEHook AddChatEHook;
extern _AddPingHook AddPingHook;
extern _AddSuicideHook AddSuicideHook;
//extern _AddRadioHook AddRadioHook;

void Load_Hooks();