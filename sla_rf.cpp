/*	Rainbow Factory Scripts
	Scripts by Stan "sla.ro" Laurentiu Alexandru
	Copyright 2010-2011 Sla Company (http://sla-co.webs.com)
	
	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#include "General.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "AABoxClass.h"
#include "GameObjManager.h"
#include "HarvesterClass.h"
#include "PhysClass.h"
#include "MoveablePhysClass.h"
#include "PhysicsSceneClass.h"
#include "gmvehicle.h"
#include "sla_rf.h"

// ------------------------------------------------
// My Little Mutant - sla_mlm_selectpony
// ------------------------------------------------

void sla_mlm_selectpony::Entered(GameObject *obj, GameObject *p)
{
	if (strcmp(Get_Parameter("ability"), "END") == 0) {
		Console_Input("gameover");
	    Console_Input("amsg Map by sla.ro");
	    Console_Input("amsg Mission Accomplished");
	    Console_Input("amsg MLP (C) Hasbro. Portal (C) Valve. Duke Nukem (C) 3D Realms. Quake (C) ID Software.");
		return;
	}

	if (strcmp(Get_Parameter("ability"), "HAX") != 0) {
		char msg[200];
		sprintf(msg, "You're now in role of %s, with element of %s", Get_Parameter("name"), Get_Parameter("ability"));
		Change_Character(p, Get_Parameter("char"));
		Send_Message_Player(p, 201, 165, 223, msg);
	}
	Commands->Send_Custom_Event(p, Commands->Find_Object(Get_Int_Parameter("controller")), 99, 0, 0.1f);
}

ScriptRegistrant<sla_mlm_selectpony> sla_mlm_selectpony_Registrant("sla_mlm_selectpony", "name=Pony:string,ability=HAX:string,controller=1:int,char=CNC:string");


// ------------------------------------------------
// My Little Mutant - sla_mlm_teleportportal
// ------------------------------------------------

void sla_mlm_teleportportal::Entered(GameObject *obj, GameObject *p)
{
	Commands->Send_Custom_Event(p, Commands->Find_Object(Get_Int_Parameter("controller")), 99, 0, 0.1f);
	char msg[128];
	sprintf(msg, "msg sla_mlm_teleportnext ENTERED %d by %d ", Commands->Get_ID(obj), Commands->Get_ID(p));
	Console_Input(msg);
}

ScriptRegistrant<sla_mlm_teleportportal> sla_mlm_teleportportal_Registrant("sla_mlm_teleportportal", "controller=0:int");

// ------------------------------------------------
// My Little Mutant - sla_mlm_teleport_controller
// ------------------------------------------------


void sla_mlm_teleport_controller::Created(GameObject *obj)
{
	objective=0; // initialize objective (first)
	teleports[0]=Get_Int_Parameter("t0");
	teleports[1]=Get_Int_Parameter("t1");
	teleports[2]=Get_Int_Parameter("t2");
	teleports[3]=Get_Int_Parameter("t3");
	teleports[4]=Get_Int_Parameter("t4");
}

void sla_mlm_teleport_controller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	// messages
	// 99 = TELEPORT
	// 98 = Change teleport type

	if (message == 99) {
		Commands->Send_Custom_Event(sender, Commands->Find_Object(teleports[objective]), 99, 0, 0.1f);
	}
	else if (message == 98) {
		objective=param;
	}
}

ScriptRegistrant<sla_mlm_portal_controller> sla_mlm_portal_controler_Registrant("sla_mlm_portal_controler", "");

// ------------------------------------------------
// My Little Mutant - sla_mlm_teleport
// ------------------------------------------------

void sla_mlm_teleport::Custom(GameObject *obj, int message, int param, GameObject *sender)
{

	if (message == 99) {
		char c[3];
		sprintf(c, "t%d", rand() % 5);
		Commands->Set_Position(sender, Commands->Get_Position(Commands->Find_Object(Get_Int_Parameter(c))));
	}
}

ScriptRegistrant<sla_mlm_teleport> sla_mlm_teleport_Registrant("sla_mlm_teleport", "t0=0:int,t1=0:int,t2=0:int,t3=0:int,t4=0:int");

// ------------------------------------------------
// My Little Mutant - sla_mlm_portal_controler
// ------------------------------------------------

void sla_mlm_portal_controller::Created(GameObject *obj)
{
	for (int i=1; i<6; i++) {
		c[i]=true;
	}
}

void sla_mlm_portal_controller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 100) {
		c[param]=true;
		Rise(obj);
	}
	else if (message == 200) {
		c[param]=false;
	}
}

void sla_mlm_portal_controller::Rise(GameObject *obj)
{
	for (int i=0; i<5; i++) {
		if (c[i] == false) { return; }
	}

	//Commands->Set_Animation(obj2,Get_Model(obj2),false,0,0,-1,0);
	Send_Message(255,255,255, "Laser fence is now disabled.");
	Destroy_Script();
	Commands->Destroy_Object(obj);
}

ScriptRegistrant<sla_mlm_teleport_controller> sla_mlm_teleport_controller_Registrant("sla_mlm_teleport_controller", "t0=0:int,t1=0:int,t2=0:int,t3=0:int,t4=0:int");

// ------------------------------------------------
// My Little Mutant - sla_mlm_portal_button
// ------------------------------------------------
void sla_mlm_portal_button::Created(GameObject *obj)
{
	Commands->Start_Timer(obj, this, 0.1f, 10);
	Commands->Enable_HUD_Pokable_Indicator(obj, true);
}

void sla_mlm_portal_button::Poked(GameObject *obj, GameObject *p)
{
	if (active == true) { return; }
	active=true;
	char msg[50];
	sprintf(msg, "Button %d has been activated.", Get_Int_Parameter("id"));
	Send_Message(255, 255, 255, msg);
	GameObject *obj2 = Commands->Find_Object(Get_Int_Parameter("controller"));
	if (!obj2) return;
	Commands->Send_Custom_Event(obj, obj2, 100, Get_Int_Parameter("id"), 0.1f);
	Commands->Enable_HUD_Pokable_Indicator(obj, false);
	Destroy_Script();
}

void sla_mlm_portal_button::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) {
		active=false;
		Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("controller")), 200, Get_Int_Parameter("id"), 0.1f);
	}
}

ScriptRegistrant<sla_mlm_portal_button> sla_mlm_portal_button_controler_Registrant("sla_mlm_portal_button", "controller=0:int,id=0:int");


// ------------------------------------------------
// My Little Mutant - sla_mlm_boss_heal
// ------------------------------------------------
void sla_mlm_boss_heal::Created(GameObject *obj)
{
	healing=true;
	Commands->Start_Timer(obj, this, 3.0f, 550);
	Commands->Start_Timer(obj, this, 0.5f, 555);
}

void sla_mlm_boss_heal::Damaged(GameObject *obj,GameObject *damager,float damage)
{
	if (Commands->Get_Health(obj) < Get_Int_Parameter("health")) {
	   hit=true;
	}
}

void sla_mlm_boss_heal::Killed(GameObject *obj,GameObject *killer)
{
	Destroy_Script();
}

void sla_mlm_boss_heal::Timer_Expired(GameObject *obj, int number)
{
	if (number == 550) {
		if (Commands->Get_Health(obj) < Get_Int_Parameter("health") && !hit) {
			healing=true;
		}
	  hit=false;
	  Commands->Start_Timer(obj, this, 3.0f, 550);
	}
	else if (number == 555) {
	  if (healing) {
		 float CH = Commands->Get_Health(obj);
		  if (CH < Get_Int_Parameter("health")) {
			  Commands->Set_Health(obj, CH+0.25f);
		  }
		  else if (CH >= Get_Int_Parameter("health"))
		  {
			  healing=false;
		  }
	  }
	}

	Commands->Start_Timer(obj, this, 1.0f, 555);

}
ScriptRegistrant<sla_mlm_boss_heal> sla_mlm_boss_heal_Registrant("sla_mlm_boss_heal", "health=500:int");

// ------------------------------------------------
// Rainbow Factory Express - sla_rfe_controller
// ------------------------------------------------


void sla_rfe_controller::Created(GameObject *obj)
{
	critical_message = false;
	game_end = false;
	bots = 0;
	Commands->Start_Timer(obj, this, 10.0f, 21);
	Commands->Start_Timer(obj, this, 11.0f, 123);
	wave = 6;
}


void sla_rfe_controller::Timer_Expired(GameObject *obj, int number)
{
	if (number == 999) // END
	{
		game_end = true;
		int totalofplayersfound=0;
		Console_Input("snda train_crash.wav");
		Commands->Start_Timer(obj, this, 8.0f, 998);
		Commands->Start_Timer(obj, this, 3.0f, 997);
		Commands->Start_Timer(obj, this, 5.0f, 996);
		if (Get_Player_Count() > 0) 
		{
			for (int i = 0;; i++)
			{

				if (Get_GameObj(i))
				{
					Set_Info_Texture(Get_GameObj(i), "black.tga");
					Enable_HUD_Player(Get_GameObj(i), false);
					Commands->Control_Enable(Get_GameObj(i), false);
					totalofplayersfound++;
				}

				if (totalofplayersfound >= Get_Player_Count()) { break; } // this little thing will save the day :)
				if (i > 999) break;
			}
		}
	}
	else if (number == 998) // End 2
	{
		// final!
		Console_Input("amsg Mission Accomplished! Map by sla.ro");
		Console_Input("snda misnwon1.wav");
		Console_Input("musica intermission.mp3");
		Console_Input("win 1");
	}
	else if (number == 997) // End 2
	{
		// final!
		Console_Input("amsg EVA: Our train crashed! Nod sabotaged it.");
	}
	else if (number == 996) // End 2
	{
		// final!
		Console_Input("amsg Havoc: The only thing I hate is being sabotaged!");
	}
	else if (number == 21) // First start
	{
		Send_Message(255, 204, 0, "Rainbow Factory - Express (by sla.ro)");
		Send_Message(255, 204, 0, "Objective: Destroy all six enemy trains and reach Rainbow Factory!");
		Commands->Start_Timer(obj, this, 5.0f, 20);
		Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("AITrainID")), 1001, 0, 20.0f);
	}
	else if (number == 123 && game_end == false) // train running sound
	{
		Console_Input("snda train_running.wav");
		Commands->Start_Timer(obj, this, 11.0f, 123);
	}
}

void sla_rfe_controller::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 1) // spawn
	{
		bots++;
	}
	else if (message == 2) // killed
	{
		bots--;
		if (bots == 0)
		{
			// send custom to go back and then come back :P
			Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("AITrainID")), 1002, 0, 3.0f);
			wave--;
			if (wave == 0)
			{
				Send_Message(255, 255, 0, "EVA: All incoming enemy trains destroyed!");
				Commands->Start_Timer(obj, this, 6.0f, 999);
				return;
			}
			Commands->Send_Custom_Event(obj, Commands->Find_Object(Get_Int_Parameter("AITrainID")), 1001, 0, 25.0f);
			char text[255];
			sprintf(text, "%d enemy trains left!", wave);
			Send_Message(255, 204, 0, text);
		}
	}
}

ScriptRegistrant<sla_rfe_controller> sla_rfe_controller_Registrant("sla_rfe_controller", "AITrainID=0:int");

// ------------------------------------------------
// Rainbow Factory Express - sla_rfe_aitrain
// ------------------------------------------------

void sla_rfe_aitrain::CreateBots(GameObject *obj)
{
	Commands->Trigger_Spawner(Get_Int_Parameter("ID1"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID2"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID3"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID4"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID5"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID6"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID7"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID8"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID9"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID10"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID11"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID12"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID13"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID14"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID15"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID16"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID17"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID18"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID19"));
	Commands->Trigger_Spawner(Get_Int_Parameter("ID20"));
}

void sla_rfe_aitrain::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 1001) // Move to shooting position
	{
		Commands->Start_Timer(obj, this, 1.0f, 10);
	}
	else if (message == 1002) // Move away
	{
		Commands->Start_Timer(obj, this, 1.0f, 20);
	}
}


void sla_rfe_aitrain::Timer_Expired(GameObject *obj, int number)
{
	if (number == 10) // move train to shooting position
	{
		Commands->Set_Animation(obj, Get_Model(obj), false, 0, 0, 0, false);
		Commands->Set_Animation(obj, Get_Model(obj), false, 0, 0, 200, false);
		Console_Input("snda enmyapp1.wav");
		Commands->Start_Timer(obj, this, 6.6f, 90);
	}
	else if (number == 20) // move train away
		Commands->Set_Animation(obj, Get_Model(obj), false, 0, 200, 300, false);
	else if (number == 90) // spawn bots
		CreateBots(obj);
}

ScriptRegistrant<sla_rfe_aitrain> sla_rfe_aitrain_Registrant("sla_rfe_aitrain", "ID1=0:int,ID2=0:int,ID3=0:int,ID4=0:int,ID5=0:int,ID6=0:int,ID7=0:int,ID8=0:int,ID9=0:int,ID10=0:int,ID11=0:int,ID12=0:int,ID13=0:int,ID14=0:int,ID15=0:int,ID16=0:int,ID17=0:int,ID18=0:int,ID19=0:int,ID20=0:int");


// ------------------------------------------------
// Rainbow Factory Intel - rf_intel_start
// ------------------------------------------------

void rf_intel_start::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_intel_start::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: We have informations of a facility that holds top secret informations.");
		Commands->Start_Timer(obj, this, 5.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: Good, I'm going to take these top secret stuff!");
		Commands->Start_Timer(obj, this, 3.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: Well, first you must find a way to the facility, it's not going to be easy!");
		Commands->Start_Timer(obj, this, 5.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(200, 150, 200, "Havoc: Hard is my middle name! Nick 'Hard' Parker");
		Commands->Start_Timer(obj, this, 3.0f, 5);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 5)
	{
		Send_Message(255, 255, 0, "EVA: Is Seymour your middle name!");
		Commands->Start_Timer(obj, this, 3.0f, 6);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 6)
	{
		Send_Message(200, 150, 200, "Havoc: ... Your artificial mind doesn't get my joke ...");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_intel_start> rf_intel_start_Registrant("rf_intel_start", "");

// ------------------------------------------------
// Rainbow Factory Intel - rf_intel_firestorm
// ------------------------------------------------

void rf_intel_firestorm::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_intel_firestorm::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: I'm detecting the presence of a firestorm near by, you have to disable it!");
		Commands->Start_Timer(obj, this, 4.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: I will just unplug the power cable, how hard can it be?");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_intel_firestorm> rf_intel_firestorm_Registrant("rf_intel_firestorm", "");

// ------------------------------------------------
// Rainbow Factory Intel - rf_intel_pp
// ------------------------------------------------

void rf_intel_pp::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_intel_pp::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: There's a power plant nearby, disabling it to shut down any defences that will occur.");
		Commands->Start_Timer(obj, this, 5.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: Defences like?");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: Probably it powers a Nod Obelisk Of Light.");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_intel_pp> rf_intel_pp_Registrant("rf_intel_pp", "");

// ------------------------------------------------
// Rainbow Factory Intel - rf_intel_facility
// ------------------------------------------------

void rf_intel_facility::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_intel_facility::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: Find the top secret documents. Appears to be protected by laser fences.");
		Commands->Start_Timer(obj, this, 2.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: So, I'm looking for a room with lasers..");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: To disable the laser fences, you must destroy the 4 control terminals powering it!");
		Commands->Start_Timer(obj, this, 4.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(200, 150, 200, "Havoc: Going to take them down!");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_intel_facility> rf_intel_facility_Registrant("rf_intel_facility", "");


// ------------------------------------------------
// Rainbow Factory Intel - rf_intel_documents
// ------------------------------------------------

void rf_intel_documents::Poked(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.1f, 1);
}

void rf_intel_documents::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(200, 150, 200, "Havoc: this the informations we need, can you read it?");
		Commands->Start_Timer(obj, this, 2.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(255, 255, 0, "EVA: Reading..");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: It indicates the existence of a factory called Rainbow Factory..");
		Commands->Start_Timer(obj, this, 2.5f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(200, 150, 200, "Havoc: Rainbow Factory? It creates rainbows?");
		Commands->Start_Timer(obj, this, 2.0f, 5);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 5)
	{
		Send_Message(255, 255, 0, "EVA: Yes, but also at this factory they're experimenting on.. ponies");
		Commands->Start_Timer(obj, this, 3.0f, 6);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 6)
	{
		Send_Message(200, 150, 200, "Havoc: Ponies? Why would Kane experiment in small horses? This time he goes small!");
		Commands->Start_Timer(obj, this, 3.0f, 7);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 7)
	{
		Send_Message(255, 255, 0, "EVA: They use the ponies to power some kind of device.");
		Commands->Start_Timer(obj, this, 3.0f, 8);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 8)
	{
		Send_Message(200, 150, 200, "Havoc: More mutants, I knew it!");
		Commands->Start_Timer(obj, this, 2.0f, 9);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 9)
	{
		Send_Message(255, 255, 0, "EVA: You must obtain more informations, I recommend going to this factory and take it down!");
		Commands->Start_Timer(obj, this, 4.0f, 10);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 10)
	{
		Send_Message(200, 150, 200, "Havoc: Finally, C4 time, this is why I love you!");
		Commands->Start_Timer(obj, this, 5.0f, 11);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 11)
	{
		Console_Input("amsg Mission Accomplished!\nMap by sla.ro\nWe're heading to Rainbow Factory!");
		Console_Input("snda misnwon1.wav");
		Console_Input("musica intermission.mp3");
		Console_Input("win 1");
	}
}

ScriptRegistrant<rf_intel_documents> rf_intel_documents_Registrant("rf_intel_documents", "");


// ------------------------------------------------
// Rainbow Factory Station - rf_station_wait
// ------------------------------------------------

void rf_station_wait::Created(GameObject *obj)
{
	arrival = 300;
	onplatform = 0;
}

void rf_station_wait::Entered(GameObject *obj, GameObject *p)
{
	if (trainstarted != true) {
		trainstarted = true;
		Commands->Start_Timer(obj, this, 0.1f, 1);
	}

	onplatform++;
}

void rf_station_wait::Exited(GameObject *obj, GameObject *p)
{
	onplatform--;
}

void rf_station_wait::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(200, 150, 200, "Havoc: I don't see any train, or is a stealth train?");
		Commands->Start_Timer(obj, this, 2.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(255, 255, 0, "EVA: You must wait for the train to come.");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Commands->Start_Timer(obj, this, 1.0f, 999);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(200, 150, 200, "Havoc: I hope it holds some rainbow horses! I can't wait much time!");
		Commands->Start_Timer(obj, this, 2.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(255, 255, 0, "EVA: Estimated time arrival 5 minutes.");
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 100) // one minute
	{
		Send_Message(255, 255, 0, "EVA: Estimated time arrival one minute.");
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 200)
	{
		Send_Message(200, 150, 200, "EVA: Train stopped. You must stay on platform.");
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 201)
	{
		Send_Message(255, 255, 0, "EVA: Train started. Please hold position.");
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 101)
	{
		Send_Message(255, 255, 0, "EVA: Train arrived in station. Please take it.");
		Commands->Start_Timer(obj, this, 3.0f, 9);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 102)
	{
		Send_Message(200, 150, 200, "Havoc: Can't I just blow it up?");
		Commands->Start_Timer(obj, this, 3.0f, 10);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 103)
	{
		Send_Message(255, 255, 0, "EVA: Destroying the train will cause mission failure.");
		Commands->Start_Timer(obj, this, 3.0f, 11);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 104)
	{
		Send_Message(200, 150, 200, "Havoc: I'm kidding.. or not.");
		Commands->Start_Timer(obj, this, 3.0f, 11);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 998)
	{
		Console_Input("amsg Mission Accomplished!\nMap by sla.ro\nNow we're in the train to the factory!");
		Console_Input("snda misnwon1.wav");
		Console_Input("musica intermission.mp3");
		Console_Input("win 1");
	}
	else if (number == 999)
	{
		if (onplatform > 0)
		{
			if (announced == false)
			{
				announced = true;
				char message[255];
				sprintf(message, "EVA: Train started. ETA %d seconds.", arrival);
				Send_Message(255, 255, 0, message);
			}
			arrival--;
			if (arrival < 5)
			{
				GameObject *train = Commands->Find_Object(Get_Int_Parameter("TRAINID"));
				Commands->Set_Animation(train, Get_Model(train), false, 0, 0, 200, false);
				Send_Message(255, 255, 0, "EVA: Train arrived in station!");
				Commands->Start_Timer(obj, this, 5.0f, 998);
				return;
			}
		}
		else
		{
			if (announced == true)
			{
				Send_Message(255, 255, 0, "EVA: You're outside the platform. Train stopped. You must stay on platform.");
				announced = false;
			}
		}

		Commands->Start_Timer(obj, this, 1.0f, 999);
	}
}

ScriptRegistrant<rf_station_wait> rf_station_wait_Registrant("rf_station_wait", "TRAINID=0:int");

// ------------------------------------------------
// Rainbow Factory Station - rf_station_near
// ------------------------------------------------

void rf_station_near::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_station_near::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: You're near the station.");
		Commands->Start_Timer(obj, this, 2.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: Thank you for telling me, is not like it says..");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: Take the train to the Rainbow Factory.");
		Commands->Start_Timer(obj, this, 2.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(200, 150, 200, "Havoc: I know my mission very well, thank you very much.");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_station_near> rf_station_near_Registrant("rf_station_near", "");

// ------------------------------------------------
// Rainbow Factory Station - rf_station_start
// ------------------------------------------------

void rf_station_start::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_station_start::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(200, 150, 200, "Havoc: Why I have to wear nod clothes?");
		Commands->Start_Timer(obj, this, 2.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(255, 255, 0, "EVA: You're wearing them to take the train without being discovered.");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(200, 150, 200, "Havoc: Is better to be naked than covered in these Nod clothes.");
		Commands->Start_Timer(obj, this, 2.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(255, 255, 0, "EVA: The train you'll be riding is called Rainbow Factory Express");
		Console_Input("snda com_ion_beep.wav");
		Commands->Start_Timer(obj, this, 2.0f, 5);
	}
	else if (number == 5)
	{
		Send_Message(200, 150, 200, "Havoc: Everything is rainbow in this?");
		Console_Input("snda com_ion_beep.wav");
		Commands->Start_Timer(obj, this, 2.0f, 6);
	}
	else if (number == 6)
	{
		Send_Message(255, 255, 0, "EVA: I'm detecting mutants around the station!");
		Console_Input("snda com_ion_beep.wav");
		Commands->Start_Timer(obj, this, 2.0f, 7);
	}
	else if (number == 7)
	{
		Send_Message(200, 150, 200, "Havoc: and the fun begins..");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_station_start> rf_station_start_Registrant("rf_station_start", "");


// ------------------------------------------------
// Rainbow Factory Station - rf_express_start
// ------------------------------------------------

void rf_express_start::Entered(GameObject *obj, GameObject *p)
{
	if (blockme == true) return;
	blockme = true;
	Commands->Start_Timer(obj, this, 0.5f, 1);
}

void rf_express_start::Timer_Expired(GameObject *obj, int number)
{
	if (number == 1)
	{
		Send_Message(255, 255, 0, "EVA: We've been discovered, Nod is sending reinforcements.");
		Commands->Start_Timer(obj, this, 3.0f, 2);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 2)
	{
		Send_Message(200, 150, 200, "Havoc: Finally I can get rid of these clothes!");
		Commands->Start_Timer(obj, this, 2.0f, 3);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 3)
	{
		Send_Message(255, 255, 0, "EVA: We're expecting at least six enemy trains.");
		Commands->Start_Timer(obj, this, 2.0f, 4);
		Console_Input("snda com_ion_beep.wav");
	}
	else if (number == 4)
	{
		Send_Message(200, 150, 200, "Havoc: Keep'em coming! I'm bored of riding the train without explosions!");
		Console_Input("snda com_ion_beep.wav");
	}
}

ScriptRegistrant<rf_express_start> rf_express_start_Registrant("rf_express_start", "");
